#mvn checkstyle:checkstyle-aggregate
#mvn checkstyle:checkstyle
#mvn checkstyle:check #required, violation leads to build failure
#mvn spotless:check
#https://github.com/Cosium/git-code-format-maven-plugin

#the argument is for arrow flight: https://arrow.apache.org/docs/java/install.html
#mvn clean package
#mvn clean package -DskipTests
#mvn -DargLine="--add-opens=java.base/java.nio=ALL-UNNAMED" test

#mvn -Dtest=RapidMatchBFSTest -DfailIfNoTests=false test
#mvn spotless:apply
mvn clean install -T1C -DfailIfNoTests=false test
#mvn com.github.ferstl:depgraph-maven-plugin:3.0.1:aggregate -DcreateImage=true -DreduceEdges=false -Dscope=compile "-Dincludes=org.example*:*"
#mvn com.github.ferstl:depgraph-maven-plugin:4.0.2:aggregate -DcreateImage=true  "-Dincludes=org.example*:*"