WITH "MATCH (a:Continent) RETURN a.name ORDER BY a.name;" AS query
CALL apoc.export.csv.query(query, "Continent/continent.csv", {})
YIELD file, source, format, nodes, relationships, properties, time, rows, batchSize, batches, done, data
RETURN file, source, format, nodes, relationships, properties, time, rows, batchSize, batches, done, data;

