/* (C)2023 */
package leetcode.graph;

import static leetcode.graph.ligra.graphgen.GraphGen.bellmanFordExample;
import static leetcode.graph.ligra.graphgen.GraphGen.dagssspExample;
import static leetcode.graph.ligra.graphgen.GraphGen.dijkstraExample;
import static leetcode.graph.ligra.graphgen.GraphGen.mstExample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import leetcode.graph.connectivity.ConnectedComponents;
import leetcode.graph.mst.MST;
import leetcode.graph.sssp.SSSP;

public class Main {

  // Driver Program
  public static void main(String[] args) {
    Graph graph;
    List<Edge> res;
    Map<Integer, Character> int2char;

    int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 'a');
            put(1, 'b');
            put(2, 'c');
            put(3, 'd');
            put(4, 'e');
            put(5, 'f');
            put(6, 'g');
            put(7, 'h');
            put(8, 'i');
          }
        };
    graph = mstExample();

    ConnectedComponents.connectedComponents(graph);

    res = MST.kruskal(graph);
    MST.printResult(res, int2char);

    graph = mstExample();
    MST.prim(graph, graph.vertices[0]);
    SSSP.printPath(graph, int2char);

    int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 's');
            put(1, 't');
            put(2, 'x');
            put(3, 'y');
            put(4, 'z');
          }
        };
    graph = bellmanFordExample();
    SSSP.bellmanFord(graph, graph.vertices[0]);
    SSSP.printPath(graph, int2char);

    graph = dagssspExample();
    SSSP.dagSSSP(graph, graph.vertices[0]);
    SSSP.printPath(graph, int2char);

    graph = dijkstraExample();
    SSSP.dijkstra(graph, graph.vertices[0]);
    SSSP.printPath(graph, int2char);
  }
}
