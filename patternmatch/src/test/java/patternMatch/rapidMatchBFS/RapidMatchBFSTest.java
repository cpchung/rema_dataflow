package patternMatch.rapidMatchBFS;

// import org.junit.BeforeClass;
// import org.junit.Test;

import static patternMatch.DataGen.dataGraphGenRapidMatch;
import static patternMatch.DataGen.queryGraphGenRapidMatch;
import static patternMatch.DataGen.queryGraphGenRapidMatchStar;
import static patternMatch.DataGen.queryGraphGenRapidMatchTriangle;
import static patternMatch.rapidMatchBFS.RapidMatchBFS.bfs;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RapidMatchBFSTest {
  RapidMatchBFS rapidMatchBFS;

  @BeforeClass
  public void setUp() {

    //        pangolin = new Pangolin();
  }

  @Test
  public void testOriginalGraph() {

    Set<KVertexEmbedding> actual = bfs(queryGraphGenRapidMatch(), dataGraphGenRapidMatch());
    LinkedHashMap<Integer, Integer> e1 = new LinkedHashMap<>();
    e1.put(1, 2);
    e1.put(2, 3);
    e1.put(3, 4);
    e1.put(4, 1);

    LinkedHashMap<Integer, Integer> e2 = new LinkedHashMap<>();
    e2.put(1, 6);
    e2.put(2, 3);
    e2.put(3, 4);
    e2.put(4, 8);
    Set<KVertexEmbedding> expected = new HashSet<>();
    expected.add(new KVertexEmbedding(e1));
    expected.add(new KVertexEmbedding(e2));
    assert actual.equals(expected);
  }

  @Test
  public void testTriangle() {

    Set<KVertexEmbedding> actual = bfs(queryGraphGenRapidMatchTriangle(), dataGraphGenRapidMatch());
    LinkedHashMap<Integer, Integer> e1 = new LinkedHashMap<>();
    e1.put(1, 2);
    e1.put(2, 3);
    e1.put(3, 4);

    LinkedHashMap<Integer, Integer> e2 = new LinkedHashMap<>();
    e2.put(1, 6);
    e2.put(2, 3);
    e2.put(3, 4);

    Set<KVertexEmbedding> expected = new HashSet<>();
    expected.add(new KVertexEmbedding(e1));
    expected.add(new KVertexEmbedding(e2));

    assert actual.equals(expected);
  }

  @Test
  public void testStar() {

    Set<KVertexEmbedding> actual = bfs(queryGraphGenRapidMatchStar(), dataGraphGenRapidMatch());

    LinkedHashMap<Integer, Integer> e1 = new LinkedHashMap<>();
    e1.put(1, 2);
    e1.put(2, 3);
    e1.put(3, 4);
    e1.put(4, 1);

    LinkedHashMap<Integer, Integer> e2 = new LinkedHashMap<>();
    e2.put(1, 6);
    e2.put(2, 3);
    e2.put(3, 7);
    e2.put(4, 8);

    LinkedHashMap<Integer, Integer> e3 = new LinkedHashMap<>();
    e3.put(1, 6);
    e3.put(2, 3);
    e3.put(3, 4);
    e3.put(4, 8);
    Set<KVertexEmbedding> expected = new HashSet<>();
    expected.add(new KVertexEmbedding(e1));
    expected.add(new KVertexEmbedding(e2));
    expected.add(new KVertexEmbedding(e3));

    assert actual.equals(expected);
  }

  @Test
  public void testWedge() {

    Set<KVertexEmbedding> actual = bfs(queryGraphGenRapidMatchTriangle(), dataGraphGenRapidMatch());
    LinkedHashMap<Integer, Integer> e1 = new LinkedHashMap<>();
    e1.put(1, 2);
    e1.put(2, 3);
    e1.put(3, 4);

    LinkedHashMap<Integer, Integer> e2 = new LinkedHashMap<>();
    e2.put(1, 6);
    e2.put(2, 3);
    e2.put(3, 4);

    Set<KVertexEmbedding> expected = new HashSet<>();
    expected.add(new KVertexEmbedding(e1));
    expected.add(new KVertexEmbedding(e2));

    assert actual.equals(expected);
  }
}
