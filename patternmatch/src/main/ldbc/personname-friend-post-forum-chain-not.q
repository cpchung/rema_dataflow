MATCH (person:Person {firstName:'John'})-[:KNOWS*1..2]->(friend:Person)
MATCH (person)<-[membership:HAS_MEMBER]-(forum:Forum)
MATCH (friend)<-[:HAS_CREATOR]-(post:Post)
WHERE NOT (post)<-[:CONTAINER_OF]-(forum)
RETURN forum.title ORDER BY forum.title ASC LIMIT 100;
