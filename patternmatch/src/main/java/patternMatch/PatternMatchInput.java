package patternMatch;

import leetcode.graph.Graph;
import lombok.Data;

@Data
public class PatternMatchInput {

  Graph Q;
  Graph G;
}
