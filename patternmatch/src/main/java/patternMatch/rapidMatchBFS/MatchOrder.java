package patternMatch.rapidMatchBFS;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import leetcode.graph.GraphV2;

// @Data
// @Builder
class MatchOrder {
  List<Integer> matchOrder;
  int current;

  MatchOrder(GraphV2 Q) {
    //    matchOrder = new LinkedList<>(Arrays.asList(2, 3, 1, 4));
    matchOrder = new LinkedList<>(Q.id2Vertex.keySet());
    Collections.shuffle(matchOrder);
    current = 0;
  }

  int current() {
    return matchOrder.get(current);
  }

  void increment() {
    current++;
  }

  void decrement() {
    current--;
  }
}
