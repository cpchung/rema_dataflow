package patternMatch.rapidMatchBFS;

import static patternMatch.DataGen.dataGraphGenRapidMatch;
import static patternMatch.DataGen.queryGraphGenRapidMatch;
import static patternMatch.DataGen.queryGraphGenRapidMatchStar;
import static patternMatch.DataGen.queryGraphGenRapidMatchTriangle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import leetcode.graph.GraphV2;

public class RapidMatchBFS {

  static Logger logger = Logger.getLogger("RapidMatch");

  public static Set<KVertexEmbedding> bfs(GraphV2 Q, GraphV2 G) {

    RelationGeneration relationGeneration = new RelationGeneration(Q, G);

    MatchOrder matchOrder = new MatchOrder(Q);

    // initialize the frontier
    Set<KVertexEmbedding> frontier = new HashSet<>();
    Set<KVertexEmbedding> res = new HashSet<>();

    int firstUId = matchOrder.current();
    matchOrder.increment();
    for (int vID : relationGeneration.getUvMapping().get(firstUId)) {

      KVertexEmbedding kVertexEmbedding =
          KVertexEmbedding.builder().embedding(new HashMap<>()).build();

      kVertexEmbedding.put(firstUId, vID);
      frontier.add(kVertexEmbedding);
    }

    // worklist algorithm for BFS
    while (!frontier.isEmpty()) {
      //            Set<KVertexEmbedding> nextFrontier = new TreeSet<>();// TreeSet use compare() to
      // check duplicates
      //
      // https://stackoverflow.com/questions/15062322/how-sets-avoid-duplicates-internally
      Set<KVertexEmbedding> nextFrontier = new HashSet<>();

      for (KVertexEmbedding kVertexEmbedding : frontier) {
        if (kVertexEmbedding.size() == Q.getId2Vertex().size()) {
          // deduplicate result
          if (!res.contains(kVertexEmbedding)) {
            res.add(kVertexEmbedding);
          }
          continue;
        }

        int uPrime = matchOrder.current();

        Set<Integer> outNeighbourIDs = Q.getSrc2dst().get(uPrime);
        // For uPrime, find its neighbors which are already matched by getting
        // the intersection of neighbours from partial embedding and neighbours from query graph.
        // Note that it is possible there are neighbours of uPrime not in the partial embedding yet.
        Set<Integer> uCandidateDataVertex = new HashSet<>(outNeighbourIDs);
        Set<Integer> kVertexEmbeddingKeySet = kVertexEmbedding.getEmbedding().keySet();
        uCandidateDataVertex.retainAll(kVertexEmbeddingKeySet);

        // TODO: optimize the join order
        // Copy is needed for vCandidateDataVertex
        Set<Integer> vCandidateDataVertex =
            new HashSet<>(relationGeneration.getUvMapping().get(uPrime));
        for (int uId : uCandidateDataVertex) {
          int vID = kVertexEmbedding.getEmbedding().get(uId);
          vCandidateDataVertex.retainAll(relationGeneration.getR(uId, vID, uPrime, G));
        }

        // TODO: optimize the encoding for partial embedding
        for (int vCandidate : vCandidateDataVertex) {
          KVertexEmbedding kVertexEmbeddingCopy = KVertexEmbedding.deepCopy(kVertexEmbedding);
          kVertexEmbeddingCopy.put(uPrime, vCandidate);

          if (!nextFrontier.contains(kVertexEmbeddingCopy)) {
            nextFrontier.add(kVertexEmbeddingCopy);
          }
        }
      }
      matchOrder.increment();
      frontier = nextFrontier;
    }
    return res;
  }

  public static void main(String[] args) {

    bfs(queryGraphGenRapidMatch(), dataGraphGenRapidMatch());
    bfs(queryGraphGenRapidMatchTriangle(), dataGraphGenRapidMatch());
    bfs(queryGraphGenRapidMatchStar(), dataGraphGenRapidMatch());
  }
}
