package patternMatch.rapidMatchBFS;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.util.Pair;
import leetcode.graph.Edge;
import leetcode.graph.GraphV2;
import leetcode.graph.Vertex;
import lombok.Data;

@Data
class RelationGeneration {

  //  store the `Relation` so it can be accessed like R(u:v,u_prime),
  //  where u_prime is the current node in the match order from Q,
  //        u is one of the neighbours of u_prime in Q
  //        v is the node in G matched with u in the current partial embedding
  // Note that (u,v) is an exiting match from the partial embedding

  // from edgeID in Q to a set of edgeID in G if their labels on (src,dst) match
  Map<Integer, Set<Integer>> qEdge2gEdge;
  Map<Pair<Integer, Integer>, Integer> directedEdge2edgeId;
  private Map<Integer, Set<Integer>> uvMapping;

  private void initializeUVMapping(GraphV2 q, GraphV2 g) {
    // change list to set
    uvMapping = new HashMap<>();
    //    using two-pointer after sorting here might be faster
    for (Vertex u : q.getId2Vertex().values()) {
      for (Vertex v : g.getId2Vertex().values()) {
        if (u.label == v.label) {
          if (!uvMapping.containsKey(u.id)) {
            uvMapping.put(u.id, new HashSet<>());
          }
          uvMapping.get(u.id).add(v.id);
        }
      }
    }
  }

  private void initializeNodes2EdgeID(GraphV2 Q, GraphV2 G) {
    directedEdge2edgeId = new HashMap<>();
    for (Edge edge : Q.id2Edge.values()) {
      int src_id = edge.getSrcID();
      int dst_id = edge.getDstID();

      Pair<Integer, Integer> pair = new Pair<>(src_id, dst_id);
      directedEdge2edgeId.put(pair, edge.getId());
    }
  }

  private String GetLabelByVertexID(int id, GraphV2 g) {
    return g.getId2Vertex().get(id).getLabel();
  }

  private void initializeEdgeRelation(GraphV2 Q, GraphV2 G) {
    qEdge2gEdge = new HashMap<>();
    for (Map.Entry<Integer, Edge> eQ : Q.getId2Edge().entrySet()) {
      int qEdgeID = eQ.getKey();
      Edge edgeQ = eQ.getValue();
      qEdge2gEdge.put(qEdgeID, new HashSet<>());

      for (Map.Entry<Integer, Edge> eG : G.getId2Edge().entrySet()) {
        int gEdgeID = eG.getKey();
        Edge edgeG = eG.getValue();

        boolean srcEqual =
            GetLabelByVertexID(edgeQ.getSrcID(), Q).equals(GetLabelByVertexID(edgeG.getSrcID(), G));
        boolean dstEqual =
            GetLabelByVertexID(edgeQ.getDstID(), Q).equals(GetLabelByVertexID(edgeG.getDstID(), G));
        if (srcEqual && dstEqual) {
          if (!qEdge2gEdge.containsKey(qEdgeID)) {
            qEdge2gEdge.put(qEdgeID, new HashSet<>());
          }
          qEdge2gEdge.get(qEdgeID).add(gEdgeID);
        }
      }
    }
  }

  RelationGeneration(GraphV2 Q, GraphV2 G) {
    initializeUVMapping(Q, G);
    initializeNodes2EdgeID(Q, G);
    initializeEdgeRelation(Q, G);
  }

  Set<Integer> getR(int u, int v, int u_prime, GraphV2 dataGraph) {

    //    u_prime -> u ==> edge ID, find the srcID whose dstID is v
    //    u -> u_prime ==> edge ID, find the dstID whose srcID is v

    Set<Integer> res = new HashSet<>();
    if (directedEdge2edgeId.containsKey(new Pair<>(u_prime, u))) {
      Pair<Integer, Integer> pair = new Pair<>(u_prime, u);

      int qEdgeID = directedEdge2edgeId.get(pair);
      for (int gEdgeID : qEdge2gEdge.get(qEdgeID)) {

        Edge dataGraphEdge = dataGraph.getId2Edge().get(gEdgeID);
        if (dataGraphEdge.getDstID() == v) {
          res.add(dataGraphEdge.getSrcID());
        }
      }
    } else {
      assert directedEdge2edgeId.containsKey(new Pair<>(u, u_prime));

      Pair<Integer, Integer> pair = new Pair<>(u, u_prime);
      int qEdgeID = directedEdge2edgeId.get(pair);
      for (int gEdgeID : qEdge2gEdge.get(qEdgeID)) {
        Edge dataGraphEdge = dataGraph.getId2Edge().get(gEdgeID);
        if (dataGraphEdge.getSrcID() == v) {
          res.add(dataGraphEdge.getDstID());
        }
      }
    }
    return res;
  }
}
