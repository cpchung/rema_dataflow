package patternMatch.rapidMatchBFS;

import java.util.List;

public class Table {

  class Column {

    int header;

    List<Integer> data;
  }

  List<Column> columns;
}
