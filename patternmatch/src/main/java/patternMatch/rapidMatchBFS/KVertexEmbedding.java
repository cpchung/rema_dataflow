package patternMatch.rapidMatchBFS;

import java.io.Serializable;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.SerializationUtils;

@Builder
@Data
@EqualsAndHashCode
public class KVertexEmbedding implements Comparable<KVertexEmbedding>, Serializable {
  //    https://stackoverflow.com/questions/28288546/how-to-copy-hashmap-not-shallow-copy-in-java
  Map<Integer, Integer> embedding;

  public static KVertexEmbedding deepCopy(KVertexEmbedding other) {
    return SerializationUtils.clone(other);
  }

  void put(Integer u, Integer v) {
    if (!embedding.containsKey(u)) {
      embedding.put(u, v);
    } else {
      throw new RuntimeException();
    }
  }

  int size() {
    return embedding.size();
  }

  @Override
  public int compareTo(KVertexEmbedding kVertexEmbedding) {
    //
    // https://stackoverflow.com/questions/26927781/lexicographical-comparison-of-two-arrays-in-java
    Integer[] self = embedding.keySet().toArray(new Integer[embedding.size()]);
    Integer[] other =
        kVertexEmbedding.embedding.keySet().toArray(new Integer[kVertexEmbedding.embedding.size()]);

    for (int i = 0; i < self.length && i < other.length; ++i) {
      int valueA, valueB, keyA, keyB;
      keyA = self[i];
      valueA = embedding.get(keyA);
      keyB = other[i];
      valueB = kVertexEmbedding.embedding.get(keyB);
      int diff = keyA - keyB;
      if (diff != 0) {
        return diff;
      } else {
        return valueA - valueB;
      }
    }
    return self.length - other.length;
  }

  boolean contain(int vID) {
    return embedding.containsKey(vID);
  }

  Map.Entry<Integer, Integer> getLastMatch() {
    //        https://stackoverflow.com/questions/1936462/java-linkedhashmap-get-first-or-last-entry
    Map.Entry<Integer, Integer> lastUV =
        embedding.entrySet().stream().skip(embedding.size() - 1).findFirst().get();
    return lastUV;
  }
}
