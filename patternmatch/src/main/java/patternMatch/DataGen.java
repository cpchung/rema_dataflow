package patternMatch;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import leetcode.graph.Edge;
import leetcode.graph.GraphV2;
import leetcode.graph.Vertex;

public class DataGen {

  public static GraphV2 queryGraphGenRapidMatch() {
    GraphV2 Q = GraphV2.builder().build();

    // https://www.bilibili.com/video/BV1uQ4y127kQ/?spm_id_from=333.788.recommend_more_video.3
    // the query graph from the paper
    Vertex u1 = Vertex.builder().id(1).label("C").build();
    Vertex u2 = Vertex.builder().id(2).label("A").build();
    Vertex u3 = Vertex.builder().id(3).label("B").build();
    Vertex u4 = Vertex.builder().id(4).label("D").build();

    Map<Integer, Vertex> id2vertex = new HashMap<>();
    id2vertex.put(1, u1);
    id2vertex.put(2, u2);
    id2vertex.put(3, u3);
    id2vertex.put(4, u4);

    Q.setId2Vertex(id2vertex);

    Map<Integer, Set<Integer>> src2dst = new HashMap<>();

    src2dst.put(1, Set.of(2, 3, 4));
    src2dst.put(2, Set.of(1, 3));
    src2dst.put(3, Set.of(1, 2));
    src2dst.put(4, Set.of(1));

    Q.setSrc2dst(src2dst);
    Q.construct();

    return Q;
  }

  public static GraphV2 queryGraphGenRapidMatchStar() {
    // star pattern with C as the center
    Vertex u1 = Vertex.builder().id(1).label("C").build();
    Vertex u2 = Vertex.builder().id(2).label("A").build();
    Vertex u3 = Vertex.builder().id(3).label("B").build();
    Vertex u4 = Vertex.builder().id(4).label("D").build();

    Map<Integer, Vertex> id2vertex = new HashMap<>();

    id2vertex.put(1, u1);
    id2vertex.put(2, u2);
    id2vertex.put(3, u3);
    id2vertex.put(4, u4);

    GraphV2 Q = GraphV2.builder().build();

    Q.setId2Vertex(id2vertex);

    Map<Integer, Set<Integer>> src2dst = new HashMap<>();

    src2dst.put(1, Set.of(2, 3, 4));
    src2dst.put(2, Set.of(1));
    src2dst.put(3, Set.of(1));
    src2dst.put(4, Set.of(1));

    Q.setSrc2dst(src2dst);

    Q.construct();

    return Q;
  }

  public static GraphV2 queryGraphGenRapidMatchTriangle() {

    GraphV2 Q = GraphV2.builder().build();

    Vertex u1 = Vertex.builder().id(1).label("C").build();
    Vertex u2 = Vertex.builder().id(2).label("A").build();
    Vertex u3 = Vertex.builder().id(3).label("B").build();

    Map<Integer, Vertex> id2Vertex = new HashMap<>();

    id2Vertex.put(1, u1);
    id2Vertex.put(2, u2);
    id2Vertex.put(3, u3);

    Q.setId2Vertex(id2Vertex);

    Map<Integer, Edge> id2Edge = new HashMap<>();

    id2Edge.put(0, Edge.builder().id(0).srcID(1).dstID(3).build());
    id2Edge.put(1, Edge.builder().id(1).srcID(2).dstID(3).build());
    id2Edge.put(2, Edge.builder().id(2).srcID(1).dstID(2).build());
    Q.setId2Edge(id2Edge);

    Q.construct();

    return Q;
  }

  public static GraphV2 dataGraphGenRapidMatch() {

    GraphV2 G = GraphV2.builder().build();

    Vertex v1 = Vertex.builder().id(1).label("D").build();
    Vertex v2 = Vertex.builder().id(2).label("C").build();
    Vertex v3 = Vertex.builder().id(3).label("A").build();
    Vertex v4 = Vertex.builder().id(4).label("B").build();
    Vertex v5 = Vertex.builder().id(5).label("C").build();
    Vertex v6 = Vertex.builder().id(6).label("C").build();
    Vertex v7 = Vertex.builder().id(7).label("B").build();
    Vertex v8 = Vertex.builder().id(8).label("D").build();
    Vertex v9 = Vertex.builder().id(9).label("A").build();
    Vertex v10 = Vertex.builder().id(10).label("C").build();

    Map<Integer, Vertex> id2Vertex = new HashMap();

    id2Vertex.put(1, v1);
    id2Vertex.put(2, v2);
    id2Vertex.put(3, v3);
    id2Vertex.put(4, v4);
    id2Vertex.put(5, v5);
    id2Vertex.put(6, v6);
    id2Vertex.put(7, v7);
    id2Vertex.put(8, v8);
    id2Vertex.put(9, v9);
    id2Vertex.put(10, v10);
    G.setId2Vertex(id2Vertex);

    Map<Integer, Set<Integer>> src2dst = new HashMap<>();
    src2dst.put(1, new HashSet<>(Arrays.asList(2)));
    src2dst.put(2, new HashSet<>(Arrays.asList(1, 3, 4)));
    src2dst.put(3, new HashSet<>(Arrays.asList(2, 4, 5, 6)));
    src2dst.put(4, new HashSet<>(Arrays.asList(2, 3, 6)));
    src2dst.put(5, new HashSet<>(Arrays.asList(3, 6, 7)));
    src2dst.put(6, new HashSet<>(Arrays.asList(3, 4, 5, 7, 8)));
    src2dst.put(7, new HashSet<>(Arrays.asList(5, 6, 8, 9)));
    src2dst.put(8, new HashSet<>(Arrays.asList(6, 7, 10)));
    src2dst.put(9, new HashSet<>(Arrays.asList(7, 10)));
    src2dst.put(10, new HashSet<>(Arrays.asList(8, 9)));
    G.setSrc2dst(src2dst);

    G.construct();

    return G;
  }
}
