package patternMatch.rapidMatchDFS;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import leetcode.graph.GraphV2;
import leetcode.graph.Vertex;
import patternMatch.DataGen;

public class Main {
  static Set<Vertex> findNeighbour(Vertex u, Vertex v, Vertex uPrime) {
    Set<Vertex> res = new HashSet<>();
    for (Vertex neighbour : v.outNeighbors) {
      if (neighbour.label == uPrime.label) {
        res.add(neighbour);
      }
    }
    return res;
  }

  public static void dfs(GraphV2 Q, GraphV2 G) {
    HashMap<Vertex, Vertex> embedding = new HashMap<>();
    Vertex[] phi = Q.getId2Vertex().values().toArray(new Vertex[0]);
    // pair<candidate vertex, at which layer>
    // https://stackoverflow.com/questions/24328679/does-java-se-8-have-pairs-or-tuples
    Stack<Map.Entry<Vertex, Integer>> st = new Stack<>();
    Set<Vertex> candidate_data_vertex = new HashSet<>();
    //        I want v3 popped out from stack before v9
    //    Arrays.sort(G.vertices, (x, y) -> y.id - x.id);
    for (Vertex v : G.getId2Vertex().values()) {
      if (v.label == phi[0].label) {
        candidate_data_vertex.add(v);
        st.push(Map.entry(v, 0));
      }
    }
    List<Map<Vertex, Vertex>> output = new LinkedList<>();
    while (!st.empty()) {
      System.out.println(st);
      Map.Entry<Vertex, Integer> pair = st.pop();
      Vertex v = pair.getKey();
      int i = pair.getValue();
      //            System.out.println(phi.length - i);
      for (int j = i; j < phi.length; j++) { // todo:reverse the order of eviction for readability
        //                if (!embedding.containsKey(phi[j])) {
        //                    System.out.println("redundant removal");
        //                }
        embedding.remove(phi[j]);
      }
      embedding.put(phi[i], v);
      if (embedding.size() == 4) {
        //                todo: review backtracking here, bug
        output.add((Map<Vertex, Vertex>) embedding.clone());
        continue;
      }
      TreeSet<Vertex> res = new TreeSet<>();
      res.addAll(G.getId2Vertex().values());
      for (Vertex u : phi[i + 1].outNeighbors) {

        if (!embedding.containsKey(u)) continue;

        Set<Vertex> neigbours = findNeighbour(u, embedding.get(u), phi[i + 1]);
        // cannot use existing matched vertices in G
        neigbours.removeAll(embedding.values());
        res.retainAll(neigbours);
      }

      for (Vertex vertex : res) {
        st.add(Map.entry(vertex, i + 1));
      }
      //            System.out.println(embedding);
    }
    System.out.println("output:");
    for (Map<Vertex, Vertex> m : output) {
      System.out.println(m);
    }
    return;
  }

  public static void main(String[] args) {
    dfs(DataGen.queryGraphGenRapidMatch(), DataGen.dataGraphGenRapidMatch());
    dfs(DataGen.queryGraphGenRapidMatchStar(), DataGen.dataGraphGenRapidMatch());
  }
}
