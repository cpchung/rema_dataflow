package leetcode.graph.shortestPath;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

// https://leetcode.com/problems/network-delay-time/discuss/248390/Shortest-Path-Faster-Algorithm-(SPFA)-26ms-beats-86.85
public class ShortestPathFasterAlgorithm {
  public int networkDelayTime(int[][] times, int N, int K) {
    // construct the graph
    Map<Integer, Map<Integer, Integer>> costs = new HashMap<>();
    for (int[] e : times) {
      costs.putIfAbsent(e[0], new HashMap<>());
      costs.get(e[0]).put(e[1], e[2]);
    }

    int[] distances = new int[N + 1];
    Arrays.fill(distances, Integer.MAX_VALUE);
    distances[K] = 0;

    Queue<Integer> que = new LinkedList<>();
    boolean[] inQue = new boolean[N + 1]; // indicate whether the element is inside the queue or not
    que.offer(K);
    while (!que.isEmpty()) {
      int node = que.poll();
      inQue[node] = false;
      if (!costs.containsKey(node)) continue;
      for (int next : costs.get(node).keySet()) {
        int d = costs.get(node).get(next);
        if (distances[next] > distances[node] + d) {
          distances[next] = distances[node] + d;
          if (!inQue[next]) {
            que.offer(next);
            inQue[next] = true;
          }
        }
      }
    }

    int result = 0;
    for (int i = 1; i <= N; i++) {
      result = Math.max(result, distances[i]);
    }

    return result == Integer.MAX_VALUE ? -1 : result;
  }
}
