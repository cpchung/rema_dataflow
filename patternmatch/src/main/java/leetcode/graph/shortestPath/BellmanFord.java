package leetcode.graph.shortestPath;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class BellmanFord {
  //
  // https://leetcode.com/problems/network-delay-time/discuss/210698/Java-Djikstrabfs-Concise-and-very-easy-to-understand
  public static void main(String[] args) {
    BellmanFord networkDelayTime = new BellmanFord();

    networkDelayTime.networkDelayTime(new int[][] {{2, 1, 1}, {2, 3, 1}, {3, 4, 1}}, 4, 2);
  }

  public int networkDelayTime(int[][] times, int N, int K) {
    Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
    for (int[] time : times) {
      map.putIfAbsent(time[0], new HashMap<>());
      map.get(time[0]).put(time[1], time[2]);
    }

    //        https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
    //        However, Dijkstra's algorithm uses a priority queue to
    //        greedily select the closest vertex that has not yet been processed,
    //        and performs this relaxation process on all of its outgoing edges;
    //        by contrast, the Bellman–Ford algorithm simply relaxes all the edges,
    //        and does this {\displaystyle |V|-1}|V|-1 times,
    //        where {\displaystyle |V|}|V| is the number of vertices in the graph.
    Queue<int[]> pq = new PriorityQueue<>((a, b) -> (a[0] - b[0]));
    pq.add(new int[] {0, K});

    boolean[] visited = new boolean[N + 1];
    int res = 0;

    while (!pq.isEmpty()) {
      int[] cur = pq.remove();
      int curNode = cur[1];
      int curDist = cur[0];
      if (visited[curNode]) continue;
      visited[curNode] = true;
      res = curDist;
      N--;
      if (map.containsKey(curNode)) {
        for (int next : map.get(curNode).keySet()) {
          pq.add(new int[] {curDist + map.get(curNode).get(next), next});
        }
      }
    }
    return N == 0 ? res : -1;
  }
}
