package leetcode.graph.shortestPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class UberIntersection {
  public static void main(String[] args) {
    UberIntersection uberIntersection = new UberIntersection();

    Map<String, List<Road>> map = new HashMap<>();
    map.put(
        "A",
        Arrays.asList(
            new Road("road1", 3, "B"), new Road("road2", 2, "B"), new Road("road3", 1, "B")));
    map.put("B", Arrays.asList(new Road("road4", 4, "C")));
    map.put("C", new ArrayList<>());
    System.out.println(uberIntersection.computeShortestRoad(map, "A", "C"));
  }

  public String computeShortestRoad(Map<String, List<Road>> roadMap, String start, String end) {
    Map<String, Map<String, Road>> map = new HashMap<>();
    for (String key : roadMap.keySet()) {
      if (!map.containsKey(key)) {
        map.put(key, new HashMap<>());
      }
      List<Road> roads = roadMap.get(key);
      Map<String, Road> wantedMap = map.get(key);
      for (Road road : roads) {
        if (!wantedMap.containsKey(road.end)) {
          wantedMap.put(road.end, road);
        }
        if (road.cost < wantedMap.get(road.end).cost) {
          wantedMap.put(road.end, road);
        }
      }
    }

    return dijkstra(map, start, end);
  }

  private String dijkstra(Map<String, Map<String, Road>> map, String start, String end) {
    String previousNode = "";
    String res = "";
    Map<String, Integer> hash = new HashMap<>();
    PriorityQueue<Node> pq = new PriorityQueue<>(Comparator.comparingInt(Node::getCost));
    hash.put(start, 0);
    pq.offer(new Node("A", 0));
    while (!pq.isEmpty()) {
      Node cur = pq.poll();
      if (!previousNode.isEmpty()) {
        res += map.get(previousNode).get(cur.name).name + ", ";
      }
      if (cur.name.equals(end)) {
        break;
      }
      previousNode = cur.name;

      for (String next : map.get(cur.name).keySet()) {
        if (!hash.containsKey(next)
            || hash.get(next) < hash.get(cur.name) + map.get(cur.name).get(next).cost) {
          hash.put(next, hash.get(cur.name) + map.get(cur.name).get(next).cost);
          pq.add(new Node(next, hash.get(cur.name) + map.get(cur.name).get(next).cost));
        }
      }
    }
    return res;
  }

  public static class Road {
    String name;
    int cost;
    String end;

    public Road(String name, int cost, String end) {
      this.name = name;
      this.cost = cost;
      this.end = end;
    }
  }

  public static class Node {
    String name;
    int cost;

    public Node(String name, int cost) {
      this.name = name;
      this.cost = cost;
    }

    public int getCost() {
      return cost;
    }
  }
}
