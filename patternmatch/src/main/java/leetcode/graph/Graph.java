package leetcode.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Graph {

  private List<Integer> edgeIDs;
  //  List<Integer> vertexIDs;

  // assume  ID is contiguous
  public Edge[] edges;
  public Vertex[] vertices;
  public Map<Integer, Vertex> id2Vertex;
  public Map<Integer, Edge> id2Edge; // edgeID and nodeID can be different
  private Map<Integer, Set<Integer>> src2dst;

  //  TODO: weights info should be stored inside edges
  public Map<Integer, Map<Integer, Integer>> weight;

  public int V;
  public int E;
  // FIXME can I get Node by id?

  private Map<Integer, Integer> outDegree;
  private Map<Integer, Integer> inDegree;

  public void construct() {

    if (this.getEdges() != null) {
      Map<Integer, Set<Integer>> src2dst = new HashMap<>();
      for (Edge edge : this.getEdges()) {
        int src = edge.getSrcID();
        int dst = edge.getDstID();

        if (!src2dst.containsKey(src)) {
          src2dst.put(src, new HashSet<>());
        }
        if (!src2dst.containsKey(dst)) {
          src2dst.put(dst, new HashSet<>());
        }
        src2dst.get(src).add(dst);
        src2dst.get(dst).add(src);
      }
      this.setSrc2dst(src2dst);
    } else {
      assert this.getSrc2dst() != null;

      for (Entry<Integer, Set<Integer>> entry : this.getSrc2dst().entrySet()) {
        Integer src = entry.getKey();
        for (Integer dst : entry.getValue()) {

          //          this.edges

          Edge.builder().srcID(src).dstID(dst).build();
        }
      }
    }

    Map<Integer, Vertex> id2Vertex = new HashMap<>();
    for (Vertex vertex : this.vertices) {
      id2Vertex.put(vertex.id, vertex);
    }
    this.setId2Vertex(id2Vertex);

    List<Integer> edgeIDs = new LinkedList<>();
    for (Edge edge : this.getEdges()) {
      edgeIDs.add(edge.getId());
    }
    this.setEdgeIDs(edgeIDs);

    Map<Integer, Edge> id2edge = new HashMap<>();
    for (int edgeID : this.getEdgeIDs()) {
      id2edge.put(edgeID, this.getEdges()[edgeID]);
    }
    this.setId2Edge(id2edge);
  }
}
