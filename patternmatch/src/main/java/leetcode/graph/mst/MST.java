package leetcode.graph.mst;
// https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/

// Java program for Kruskal's algorithm to find Minimum
// Spanning Tree of a given connected, undirected and
// weighted graph

import static leetcode.graph.sssp.SSSP.initializeSingleSource;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import leetcode.graph.Edge;
import leetcode.graph.Graph;
import leetcode.graph.Vertex;
import leetcode.graph.mst.unionfind.UnionFind;

public class MST {

  // The main function to construct MST using Kruskal's algorithm
  public static List<Edge> kruskal(Graph g) {
    // Step 1: Sort all the edges in non-decreasing order of their weight.
    Arrays.sort(
        g.edges,
        (a, b) -> g.weight.get(a.src.id).get(a.dst.id) - g.weight.get(b.src.id).get(b.dst.id));
    List<Edge> result = new LinkedList<>();

    UnionFind uf = new UnionFind(g.V);

    for (int i = 0; i < g.edges.length; i++) {
      int u = g.edges[i].src.id;
      int v = g.edges[i].dst.id;

      if (uf.find(u) != uf.find(v)) {
        result.add(g.edges[i]);
        uf.union(u, v);
      }
    }

    return result;
  }

  public static void prim(Graph g, Vertex s) {
    //        todo: refactor it s.t.
    //              it can pass relax() as functional argument for both dijkstra and prim
    /***
     * can select b-c or a-h, depending on the order of neighbours of vertex a
     * */

    /***
     * page634: During execution of the algorithm,
     * all vertices that are not in the tree reside in a min-priority queue Q
     * based on a key attribute.
     */
    /***
     * For each vertex v, the attribute v.key is the minimum weight of any edge connecting v
     * to a vertex in the tree; by convention, v.key is +inf if there is no such edge. The
     * attribute v.pi names the parent of v in the tree.
     */

    //        SSSP.dijkstra(g, r);

    initializeSingleSource(g, s);
    //        min-priority queue Q of vertices, keyed by their d values.
    PriorityQueue<Vertex> pq = new PriorityQueue<>((a, b) -> a.distance - b.distance);
    pq.add(s);

    boolean[] visited = new boolean[g.V]; // no decrease key, insert into pq only after visited
    Arrays.fill(visited, false);

    while (!pq.isEmpty()) {
      Vertex u = pq.poll();
      visited[u.id] = true;

      for (Vertex v : u.outNeighbors) {
        if (visited[v.id]) continue;

        int w = g.weight.get(u.id).get(v.id);
        if (v.distance > w) {
          v.distance = w;
          v.predecessor = u;
        }
        //                relax(u, v, g.weight.get(u.id).get(v.id));

        pq.add(v);
      }
    }

    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
    System.out.println("done " + methodName);
  }

  public static void printResult(List<Edge> result, Map<Integer, Character> int2char) {
    System.out.println("Following are the edges in " + "the constructed MST");

    for (Edge e : result) {
      System.out.printf(
          "%c -> %c, weight: %d\n", int2char.get(e.src.id), int2char.get(e.dst.id), e.weight);
    }
    System.out.printf("MST edge count is correct: %b\n", result.size() == 8);
  }
}
