package leetcode.graph.mst.unionfind;

// https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/discuss/479635/Intuitive-Disjoint-Set-Union-Find-in-JavaPython3
public class CountComponents {

  public static void main(String[] args) {

    CountComponents countComponents = new CountComponents();

    //        int  n=5;
    //        int [][] edges=      {{0,1},{1,2},{3,4}};

    //        int n = 3;
    //        int[][] edges = {{1, 0}, {0, 2}, {2, 1}};

    int n = 4;
    int[][] edges = {{0, 1}, {2, 3}, {1, 2}};
    int res = countComponents.countComponents(n, edges);

    System.out.println(res);
  }

  public int countComponents(int n, int[][] edges) {

    int count = n;

    UnionFind dsu = new UnionFind(n);
    for (int i = 0; i < edges.length; ++i) {
      boolean res = dsu.union(edges[i][0], edges[i][1]);
      System.out.println(res);

      if (res) count--;
    }
    return count;
  }
}
