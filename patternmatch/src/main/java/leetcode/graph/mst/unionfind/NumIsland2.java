package leetcode.graph.mst.unionfind;

import java.util.ArrayList;
import java.util.List;

public class NumIsland2 {

  public static void main(String[] args) {

    NumIsland2 numIsland2 = new NumIsland2();

    //        int[][] positions = {{0, 0}, {0, 1}, {1, 2}, {2, 1}};
    //        int m = 3, n = 3;

    //        int[][] positions = {{0, 0}, {0, 1}, {1, 2}, {1, 2}};
    //        int m = 3, n = 3;

    int[][] positions = {{0, 0}, {7, 1}, {6, 1}, {3, 3}, {4, 1}};
    int m = 8, n = 4;

    List<Integer> res = numIsland2.numIslands2(m, n, positions);

    System.out.println(res);
  }

  public List<Integer> numIslands2(int m, int n, int[][] positions) {

    int count = 0;
    List<Integer> res = new ArrayList<>();
    UnionFind unionFind = new UnionFind(m * n);

    int[][] grid = new int[m][n];
    for (int[] pos : positions) {

      int i = pos[0], j = pos[1];
      if (grid[i][j] == 1) {
        res.add(count);
        continue;
      }

      grid[i][j] = 1;
      count++;

      int[] dirs = {0, -1, 0, 1, 0};

      for (int k = 0; k < 4; k++) {
        int ii = i + dirs[k], jj = j + dirs[k + 1];

        if (ii < 0 || ii >= m || jj < 0 || jj >= n) continue;

        if (grid[ii][jj] == 1) {
          if (unionFind.union(ii * n + jj, i * n + j)) count--;
        }
      }

      res.add(count);
    }

    return res;
  }
}
