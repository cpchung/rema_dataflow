package leetcode.graph.mst.unionfind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

class Edge implements Comparable<Edge> {
  int v;
  int c;

  public Edge(int v, int c) {
    this.v = v;
    this.c = c;
  }

  public int compareTo(Edge e) {
    return this.c - e.c;
  }
}

class Node {
  int id;
  List<Edge> edges;

  public Node(int id) {
    this.id = id;
    this.edges = new ArrayList();
  }
}

public class MSTPrimMinCostToSupplyWater {

  public static void main(String[] args) {
    MSTPrimMinCostToSupplyWater sol = new MSTPrimMinCostToSupplyWater();
    int res = sol.minCostToSupplyWater(3, new int[] {1, 2, 2}, new int[][] {{1, 2, 1}, {2, 3, 1}});
    System.out.println(res);
  }

  public int minCostToSupplyWater(int n, int[] wells, int[][] pipes) {
    Map<Integer, Node> map = new HashMap();
    PriorityQueue<Edge> pq = new PriorityQueue();
    Set<Integer> seen = new HashSet();
    int cost = 0;

    for (int i = 0; i <= n; i++) map.put(i, new Node(i));

    for (int i = 0; i < wells.length; i++) {
      Edge e = new Edge(i + 1, wells[i]);
      map.get(0).edges.add(e);
      pq.offer(e);
    }

    for (int[] pipe : pipes) {
      map.get(pipe[0]).edges.add(new Edge(pipe[1], pipe[2]));
      map.get(pipe[1]).edges.add(new Edge(pipe[0], pipe[2]));
    }

    seen.add(0);

    while (pq.size() > 0 && seen.size() < n + 1) {
      Edge minEdge = pq.poll();

      if (seen.contains(minEdge.v)) continue;

      seen.add(minEdge.v);
      cost += minEdge.c;

      for (Edge newEdge : map.get(minEdge.v).edges) {
        if (!seen.contains(newEdge.v)) pq.offer(newEdge);
      }
    }

    return cost;
  }
}
