package leetcode.graph.mst.unionfind;

import java.util.Arrays;

//        Runtime: 0 ms, faster than 100.00% of Java online submissions for Redundant Connection.
//        Memory Usage: 39.3 MB, less than 95.38% of Java online submissions for Redundant
// Connection.
public class FindRedundantConnection {
  //
  // https://leetcode.com/problems/redundant-connection/discuss/123819/Union-Find-with-Explanations-(Java-Python)

  public int[] findRedundantConnection(int[][] edges) {
    UnionFind disjointSet = new UnionFind(edges.length);

    for (int[] edge : edges) {
      if (!disjointSet.union(edge[0] - 1, edge[1] - 1)) return edge;
    }

    throw new IllegalArgumentException();
  }

  public static void main(String[] args) {

    FindRedundantConnection findRedundantConnection = new FindRedundantConnection();
    int[][] edges = {{1, 2}, {1, 3}, {2, 3}};

    int[] res = findRedundantConnection.findRedundantConnection(edges);

    System.out.println(Arrays.toString(res));
  }
}
