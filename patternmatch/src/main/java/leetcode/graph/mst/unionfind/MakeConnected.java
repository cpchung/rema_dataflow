package leetcode.graph.mst.unionfind;

// 1319. Number of Operations to Make Network Connected
// https://leetcode.com/contest/weekly-contest-171/problems/number-of-operations-to-make-network-connected/
public class MakeConnected {

  public int makeConnected(int n, int[][] connections) {

    UnionFind uf = new UnionFind(n);

    int left = 0;

    for (int[] pair : connections) {

      if (uf.find(pair[0]) != uf.find(pair[1])) {
        uf.union(pair[0], pair[1]);
      } else {
        left++;
      }
    }

    int count = 0;

    for (int i = 1; i < n; i++) {

      if (uf.find(0) != uf.find(i)) {
        uf.union(0, i);
        count++;
        left--;
      }
    }

    return left >= 0 ? count : -1;
  }
}
