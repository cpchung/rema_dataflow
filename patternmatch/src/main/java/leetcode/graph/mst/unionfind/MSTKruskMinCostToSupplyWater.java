package leetcode.graph.mst.unionfind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MSTKruskMinCostToSupplyWater {
  public int minCostToSupplyWater(int n, int[] wells, int[][] pipes) {

    List<int[]> augmentedPipes = new ArrayList<>();

    for (int i = 0; i < pipes.length; i++) {
      augmentedPipes.addAll(Arrays.asList(pipes[i]));
    }
    for (int i = 0; i < wells.length; i++) {
      augmentedPipes.add(new int[] {0, i + 1, wells[i]});
    }

    int[][] new_pipes = augmentedPipes.toArray(new int[0][0]);

    Arrays.sort(new_pipes, (a, b) -> (a[2] - b[2]));

    int cost = 0;
    UnionFind uf = new UnionFind(n + 1);
    for (int[] p : new_pipes) {
      if (uf.find(p[0]) != uf.find(p[1])) {
        uf.union(p[0], p[1]);
        cost += p[2];
      }
    }
    return cost;
  }

  public static void main(String[] args) {

    MSTKruskMinCostToSupplyWater minCostToSupplyWater = new MSTKruskMinCostToSupplyWater();

    minCostToSupplyWater.minCostToSupplyWater(
        3, new int[] {1, 2, 2}, new int[][] {{1, 2, 1}, {2, 3, 1}});
  }
}
