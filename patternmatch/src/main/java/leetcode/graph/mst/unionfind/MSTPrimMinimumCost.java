package leetcode.graph.mst.unionfind;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.IntStream;

public class MSTPrimMinimumCost {

  // prim algorithm
  public int minimumCost(int N, int[][] connections) {
    if (N == 0) return 0;
    if (connections == null || connections.length == 0) return -1;

    boolean[] visited = new boolean[N + 1];
    Map<Integer, HashSet<int[]>> g = new HashMap<>();
    for (int[] c : connections) {
      g.computeIfAbsent(c[0], y -> new HashSet<>()).add(c);
      g.computeIfAbsent(c[1], y -> new HashSet<>()).add(new int[] {c[1], c[0], c[2]});
    }
    Queue<int[]> pq = new PriorityQueue<>((int[] a, int[] b) -> a[2] - b[2]);
    int k = g.keySet().iterator().next();
    visited[k] = true;
    for (int[] e : g.get(k)) pq.offer(e);

    int r = 0;
    while (!pq.isEmpty()) {
      int[] e = pq.poll();
      if (visited[e[1]]) continue;
      r += e[2];
      visited[e[1]] = true;
      for (int[] ne : g.get(e[1])) {
        if (!visited[ne[1]]) {
          pq.offer(ne);
        }
      }
    }
    return IntStream.range(1, N + 1).allMatch(i -> visited[i] == true) ? r : -1;
  }

  public static void main(String[] args) {

    MSTPrimMinimumCost sol = new MSTPrimMinimumCost();

    //        Input: N = 3, connections = [[1,2,5],[1,3,6],[2,3,1]]
    //        Output: 6

    int N = 3;
    int[][] connections = {{1, 2, 5}, {1, 3, 6}, {2, 3, 1}};

    //        Input: N = 4, connections = [[1,2,3],[3,4,4]]
    //        Output: -1
    //        int N = 4;
    //        int[][] connections = {{1, 2, 3}, {3, 4, 4}};

    int res = sol.minimumCost(N, connections);
    System.out.println(res);
  }
}
