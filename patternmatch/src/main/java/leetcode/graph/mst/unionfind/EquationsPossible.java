package leetcode.graph.mst.unionfind;

public class EquationsPossible {

  public boolean equationsPossible(String[] equations) {

    UnionFind uf = new UnionFind(26);

    for (String s : equations) {
      if (s.charAt(1) == '=') {
        uf.union(s.charAt(0) - 'a', s.charAt(3) - 'a');
      }
    }

    for (String s : equations) {
      if (s.charAt(1) == '!') {
        if (uf.find(s.charAt(0) - 'a') == uf.find(s.charAt(3) - 'a')) return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {

    String[] eqs = {"a==b", "b!=a"};

    EquationsPossible sol = new EquationsPossible();
    sol.equationsPossible(eqs);
  }
}
