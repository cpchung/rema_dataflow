package leetcode.graph.mst.unionfind;

public class NumIslands {

  public static void main(String[] args) {

    //
    // [["1","1","0","0","0"],["1","1","0","0","0"],["0","0","1","0","0"],["0","0","0","1","1"]]
    // expected 3

    NumIslands numIslands = new NumIslands();

    char[][] grid = {
      {'1', '1', '0', '0', '0'},
      {'1', '1', '0', '0', '0'},
      {'0', '0', '1', '0', '0'},
      {'0', '0', '0', '1', '1'}
    };
    int res = numIslands.numIslands(grid);

    System.out.println(res);
  }

  public int numIslands(char[][] grid) {
    if (grid == null || grid.length == 0) {
      return 0;
    }

    int n = grid.length;
    int m = grid[0].length;
    int count = 0;

    UnionFind uf = new UnionFind(n * m);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (grid[i][j] == '1') {
          count++;
        }
      }
    }

    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {

        if (grid[i][j] == '1') {

          if (i - 1 >= 0 && grid[i - 1][j] == '1') {
            if (uf.union(i * m + j, (i - 1) * m + j)) count--;
          }
          if (i + 1 < n && grid[i + 1][j] == '1') {
            if (uf.union(i * m + j, (i + 1) * m + j)) count--;
          }
          if (j - 1 >= 0 && grid[i][j - 1] == '1') {
            if (uf.union(i * m + j, i * m + j - 1)) count--;
          }
          if (j + 1 < m && grid[i][j + 1] == '1') {
            if (uf.union(i * m + j, i * m + j + 1)) count--;
          }

          grid[i][j] = '0';
        }
      }
    }

    return count;
  }
}
;
