package leetcode.graph.mst.unionfind;

public class UnionFind200 {}

//
//    class UnionFind {
//        int[] parent;
//        int[] rank;
////    private byte[] rank;
//
//        public UnionFind(char[][] grid) { // for problem 200
//            int m = grid.length;
//            int n = grid[0].length;
//            parent = new int[m * n];
//            rank = new int[m * n];
//            for (int i = 0; i < m; ++i) {
//                for (int j = 0; j < n; ++j) {
//                    if (grid[i][j] == '1') {
//                        parent[i * n + j] = i * n + j;
//                    }
//                    rank[i * n + j] = 0;
//                }
//            }
//        }
//
//        public UnionFind(int n) {
//            if (n < 0) throw new IllegalArgumentException();
//            rank = new int[n];
////        rank = new byte[n];
//
//            parent = new int[n];
//
//            for (int i = 0; i < n; i++) parent[i] = i;
//
//        }
//
//        public int find(int x) {
////        if (parent[x] == 0) return x;
//            if (parent[x] == x) return x;
//            return parent[x] = find(parent[x]); // Path compression by halving.
//        }
//
//        // Return false if x, y are connected.
//        public boolean union(int x, int y) {
//            int rootX = find(x);
//            int rootY = find(y);
//            if (rootX == rootY) return false;
//
//            // Make root of smaller rank point to root of larger rank.
//            if (rank[rootX] < rank[rootY]) {
//                parent[rootX] = rootY;
//            } else if (rank[rootX] > rank[rootY]) {
//                parent[rootY] = rootX;
//            } else {
//                parent[rootX] = rootY;
//                rank[rootY]++;
//            }
//            return true;
//        }
//
//        public int getNumConnectedComponents() {
//
//            Set<Integer> s = new HashSet<>();
//            for (int i : parent) {
//                s.add(i);
//            }
//            return s.size();
//        }
//    }
