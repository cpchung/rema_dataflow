package leetcode.graph.mst.unionfind;

import java.util.HashSet;
import java.util.Set;

public class ValidTree {

  public static void main(String[] args) {
    ValidTree sol = new ValidTree();

    //        int n = 4;
    //        int[][] edges = {{0, 1}, {2, 3}};

    int n = 4;
    int[][] edges = {{0, 1}, {2, 3}, {1, 2}};

    boolean res = sol.validTree(n, edges);

    System.out.println(res);
  }

  public boolean validTree(int n, int[][] edges) {

    UnionFind uf = new UnionFind(n);

    for (int[] edge : edges) {

      if (!uf.union(edge[0], edge[1])) return false;
    }

    Set<Integer> s = new HashSet<>();

    for (int i = 0; i < n; i++) {
      s.add(uf.find(i));
    }

    return s.size() == 1;
  }
}
