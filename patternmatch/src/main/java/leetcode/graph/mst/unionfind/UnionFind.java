package leetcode.graph.mst.unionfind;

import java.util.Arrays;

public class UnionFind {

  int[] parent;
  int[] rank;

  public UnionFind(int n) {
    rank = new int[n];
    parent = new int[n];

    for (int i = 0; i < n; i++) parent[i] = i;
  }

  public int find(int x) {

    if (x == parent[x]) {
      return parent[x];
    }

    parent[x] = find(parent[x]);
    return parent[x];
    /*no path-compression:*/
    /* return find(parent[x]);
     */

  }

  // Return false if x, y are connected.
  public boolean union(int x, int y) {
    int rootX = find(x);
    int rootY = find(y);
    if (rootX == rootY) return false;

    // Make root of smaller rank point to root of larger rank.
    if (rank[rootX] < rank[rootY]) {
      parent[rootX] = rootY;
    } else if (rank[rootX] > rank[rootY]) {
      parent[rootY] = rootX;
    } else {
      parent[rootX] = rootY;
      rank[rootY]++;
    }
    return true;
  }

  @Override
  public String toString() {
    return "UnionFind{"
        + "parent="
        + Arrays.toString(parent)
        + ", rank="
        + Arrays.toString(rank)
        + '}';
  }
}
