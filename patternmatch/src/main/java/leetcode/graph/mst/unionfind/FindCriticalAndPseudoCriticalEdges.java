package leetcode.graph.mst.unionfind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;

public class FindCriticalAndPseudoCriticalEdges {

  int[] roots;
  Map<String, Integer> nonCrit = new HashMap<>();
  Map<String, Integer> crit = new HashMap<>();
  int[][] edgs;
  Set<Integer>[] graph;

  @SuppressWarnings("unchecked")
  public List<List<Integer>> findCriticalAndPseudoCriticalEdges(int n, int[][] edges) {
    PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> a[2] - b[2]);
    edgs = new int[edges.length][];
    for (int i = 0; i < edges.length; i++) {
      int[] e = edges[i];
      pq.add(edgs[i] = new int[] {Math.min(e[0], e[1]), Math.max(e[0], e[1]), e[2], i});
    }

    roots = new int[n];
    graph = new HashSet[n];
    for (int i = 0; i < n; i++) {
      roots[i] = i;
      graph[i] = new HashSet<>();
    }

    while (pq.size() > 0) {
      int[] e = pq.poll();
      int u = find(e[0]);
      int v = find(e[1]);

      graph[e[0]].add(e[1]);
      graph[e[1]].add(e[0]);

      if (u != v) {
        union(u, v);
        crit.put(getKey(e), e[3]);
      } else {
        dfs(-1, e[0], e, new Stack<>(), new boolean[n]);
        graph[e[0]].remove(e[1]);
        graph[e[1]].remove(e[0]);
      }
    }

    return Arrays.asList(new ArrayList<>(crit.values()), new ArrayList<>(nonCrit.values()));
  }

  private String getKey(int[] e) {
    return e[0] + "_" + e[1];
  }

  private boolean dfs(int prev, int u, int[] lastEdge, Stack<int[]> stack, boolean[] visited) {
    if (visited[u]) {
      // stack has the last edge and the start edge in the cycle, ignore them
      int ignore = 2;
      while (ignore > 0) {
        int[] e = stack.pop();
        if (e[0] == u || e[1] == u) {
          ignore--;
        }
        String key = getKey(e);
        Integer val = crit.get(key);
        if (val != null && edgs[val][2] == lastEdge[2]) {
          nonCrit.put(key, crit.get(key));
          crit.remove(key);
        }

        Integer ncVal = nonCrit.get(key);
        if (ncVal != null && edgs[ncVal][2] == lastEdge[2]) {
          nonCrit.put(getKey(lastEdge), lastEdge[3]);
        }
      }
      return false;
    }

    visited[u] = true;
    for (int v : graph[u]) {
      if (v != prev && v != u) {
        stack.add(new int[] {Math.min(u, v), Math.max(u, v)});
        if (!dfs(u, v, lastEdge, stack, visited)) {
          return false;
        }
        stack.pop();
      }
    }
    return true;
  }

  private void union(int u, int v) {
    int rU = find(u);
    int rV = find(v);
    roots[rU] = rV;
  }

  private int find(int u) {
    int orig = u;
    while (u != roots[u]) {
      u = roots[u];
    }
    while (orig != roots[orig]) {
      int tmp = roots[orig];
      roots[orig] = u;
      orig = tmp;
    }

    return u;
  }
}
