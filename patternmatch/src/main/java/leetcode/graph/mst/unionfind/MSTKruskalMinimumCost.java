package leetcode.graph.mst.unionfind;

import java.util.Arrays;

public class MSTKruskalMinimumCost {

  public static void main(String[] args) {

    MSTKruskalMinimumCost sol = new MSTKruskalMinimumCost();

    //        Input: N = 3, connections = [[1,2,5],[1,3,6],[2,3,1]]
    //        Output: 6

    int N = 3;
    int[][] connections = {{1, 2, 5}, {1, 3, 6}, {2, 3, 1}};

    //        Input: N = 4, connections = [[1,2,3],[3,4,4]]
    //        Output: -1
    //        int N = 4;
    //        int[][] connections = {{1, 2, 3}, {3, 4, 4}};

    int res = sol.minimumCost(N, connections);
    System.out.println(res);
  }

  //
  // https://leetcode.com/problems/connecting-cities-with-minimum-cost/discuss/344867/Java-Kruskal's-Minimum-Spanning-Tree-Algorithm-with-Union-Find
  public int minimumCost(int N, int[][] connections) {
    UnionFind uf = new UnionFind(N + 1);

    Arrays.sort(connections, (a, b) -> (a[2] - b[2]));

    int res = 0;

    int n = N;
    for (int[] c : connections) {
      int x = c[0], y = c[1];
      if (uf.find(x) != uf.find(y)) {
        res += c[2];
        uf.union(x, y);
        n--;
      }
    }

    return n == 1 ? res : -1;
  }
}
