package leetcode.graph.connectivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConnectedComponentsDPV {
  private int[][] adj;

  private int v; // number of vertices

  // constructor
  public ConnectedComponentsDPV(int[][] in) {
    this.adj = in;
    this.v = in.length;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < this.v; i++) {
      sb.append(Arrays.toString(adj[i]));
      sb.append("\n");
    }
    return sb.toString();
  }

  public List<List<Integer>> connectedCompnents() {
    ArrayList res = new ArrayList<ArrayList<Integer>>();
    boolean[] visited = new boolean[this.adj.length];
    ArrayList cc = new ArrayList<Integer>();
    for (int i = 0; i < this.adj.length; i++) {
      if (visited[i] == false) {
        cc = new ArrayList<Integer>();
        explore(i, visited, cc);
        res.add(cc);
      }
    }
    return res;
  }

  private void explore(int n, boolean[] visited, ArrayList<Integer> cc) {
    visited[n] = true;
    cc.add(n);
    for (int i = 0; i < this.v; i++) {
      if (visited[i] == false && adj[n][i] == 1) {
        explore(i, visited, cc);
      }
    }
  }

  public static void main(String[] args) {
    int[][] graph = {{0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
    ConnectedComponentsDPV g = new ConnectedComponentsDPV(graph);
    System.out.print(g.connectedCompnents());
  }
}
