package leetcode.graph.connectivity;

import leetcode.graph.Graph;
import leetcode.graph.mst.unionfind.UnionFind;

public class ConnectedComponents {

  public static void connectedComponents(Graph g) {

    UnionFind uf = new UnionFind(g.V);

    for (int i = 0; i < g.edges.length; i++) {
      int u = g.edges[i].src.id;
      int v = g.edges[i].dst.id;

      if (uf.find(u) != uf.find(v)) {
        uf.union(u, v);
      }
    }

    System.out.println(uf);
  }
}
