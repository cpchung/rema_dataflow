package leetcode.graph;

import lombok.Builder;
import lombok.Data;

// A class to represent a graph edge
@Data
@Builder
public class Edge /*implements Comparable<Edge>*/ {
  // TODO: store ID only, create a map from ID to edge

  int id;
  int srcID;
  int dstID;

  public Vertex src;
  public Vertex dst;
  public int weight;

  private String label;

  //    @Override
  //    public int compareTo(Edge edge) {
  //        return 0;
  //    }
}
