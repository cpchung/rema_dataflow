package leetcode.graph.sssp;

import java.util.Arrays;
import java.util.Map;
import java.util.PriorityQueue;
import leetcode.graph.Edge;
import leetcode.graph.Graph;
import leetcode.graph.Vertex;
import leetcode.graph.dfs.DFS;

// todo
// page 649 for dijkstra, bellman ford and linear time sssp
public class SSSP {
  //    24.1 BellmanFord for general case allowing negative weight, can detect negative-weight cycle
  //    24.2 linear-time algorithm for DAG, can be used for finding where is the critical-path and
  // its length
  //    24.3 Dijkstra, with non-nagative weight, can have positive cycles, check example in fig 24.6
  //    24.4 use Bellman-Ford to solve special case of linear programming

  public static boolean bellmanFord(Graph g, Vertex s) {

    initializeSingleSource(g, s);

    // todo:       makes |V-1| passes over all edges. why V-1 times? propagation?
    //       good explanation about why |V-1| times :https://youtu.be/NzgFUwOaoIw?t=366
    for (int i = 0; i < g.V - 1; i++) {
      for (Edge edge : g.edges) {
        relax(edge.src, edge.dst, edge.getWeight());
      }
    }

    for (Edge edge : g.edges) {
      if (edge.dst.distance > edge.src.distance + edge.weight) return false;
    }

    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
    System.out.println("done " + methodName);
    return true;
  }

  public static void dagSSSP(Graph g, Vertex s) {
    initializeSingleSource(g, s);

    DFS dfs = new DFS();
    int[] topologicalOrder = dfs.dfs(g);
    for (int vertexID : topologicalOrder) {
      Vertex u = g.vertices[vertexID];
      for (Vertex v : g.vertices[vertexID].outNeighbors) {
        relax(u, v, g.weight.get(u.id).get(v.id));
      }
    }

    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
    System.out.println("done " + methodName);
  }

  public static void dijkstra(Graph g, Vertex s) {

    //        Dijkstra is a greedy algo traversing like BFS,
    //        except using priorityQueue rather than Queue

    initializeSingleSource(g, s);
    //        min-priority queue Q of vertices, keyed by their d values.
    PriorityQueue<Vertex> pq = new PriorityQueue<>((a, b) -> a.distance - b.distance);
    pq.add(s);

    boolean[] visited = new boolean[g.V]; // no decrease key, insert into pq only after visited
    Arrays.fill(visited, false);

    while (!pq.isEmpty()) {
      Vertex u = pq.poll();
      visited[u.id] = true;
      for (Vertex v : u.outNeighbors) {
        if (visited[v.id]) continue;

        relax(u, v, g.weight.get(u.id).get(v.id));
        pq.add(v);
      }
    }

    String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
    System.out.println("done " + methodName);
  }

  public static void initializeSingleSource(Graph g, Vertex s) {

    for (Vertex v : g.vertices) {
      v.distance = Integer.MAX_VALUE;
      v.predecessor = null; // NIL
    }
    s.distance = 0;
  }

  public static void relax(Vertex u, Vertex v, int w) {

    if (v.distance > u.distance + w) {
      v.distance = u.distance + w;
      v.predecessor = u;
    }
  }

  public static void printPath(Graph g, Map<Integer, Character> int2char) {
    //        todo: page 601, better printPath()
    for (Vertex v : g.vertices) {
      if (v.predecessor != null) {
        System.out.printf(
            "%c -> %c, distance: %d\n",
            int2char.get(v.predecessor.id), int2char.get(v.id), v.distance);
      }
    }
  }
}
