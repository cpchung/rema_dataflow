package leetcode.graph.max_flow;
// Java program for implementation of Ford Fulkerson algorithm

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class MaxFlowBak {
  // static final int V = 6; // Number of vertices in graph

  // boolean bfs(List<List<Integer>> rGraph, int s, int t, Map<Integer, Integer>
  // parent) {
  boolean bfs(int[][] rGraph, int s, int t, Map<Integer, Integer> parent) {

    Map<Integer, Integer> dist = new HashMap<Integer, Integer>();
    // for (int i = 0; i < rGraph.size(); ++i)
    for (int i = 0; i < rGraph.length; ++i) dist.put(i, -1);

    LinkedList<Integer> queue = new LinkedList<Integer>();

    queue.add(s);
    dist.put(s, 0);

    parent.put(s, -1);

    // Standard BFS Loop
    while (queue.size() != 0) {
      int u = queue.poll();
      for (int v = 0; v < dist.size(); v++) {
        // if (dist.get(v) == -1 && rGraph.get(u).get(v) > 0) {
        if (dist.get(v) == -1 && rGraph[u][v] > 0) {
          queue.add(v);
          // parent[v] = u;
          parent.put(v, u);
          dist.put(v, dist.get(u) + 1); // // visited[v] = true;
        }
      }
    }
    System.out.println(dist);

    // System.out.println(Arrays.asList(dist)); // method 1
    // System.out.println(Collections.singletonList(dist)); // method 2
    return (dist.get(t) > 0);
  }

  // Returns tne maximum flow from s to t in the given graph
  int fordFulkerson(int graph[][], int s, int t) {

    // List<List<Integer>> rGraph = new ArrayList<>();

    // int rGraph[][] = new int[V][V];
    // for (u = 0; u < V; u++)
    // for (v = 0; v < V; v++)
    // rGraph[u][v] = graph[u][v];

    int[][] rGraph = graph;
    // for (int[] ints : graph) {

    // List<Integer> list = new ArrayList<>();
    // for (int i : ints) {
    // list.add(i);
    // }
    // rGraph.add(list);
    // }

    // This array is filled by BFS and to store path
    // int parent[] = new int[V];
    Map<Integer, Integer> parent = new HashMap<Integer, Integer>();
    int max_flow = 0; // There is no flow initially
    // Augment the flow while tere is path from source to sink
    int u, v;

    while (bfs(rGraph, s, t, parent)) {
      // Find minimum residual capacity of the edhes
      // along the path filled by BFS. Or we can say
      // find the maximum flow through the path found.
      int path_flow = Integer.MAX_VALUE;
      for (v = t; v != s; v = parent.get(v)) {
        // u = parent[v];
        u = parent.get(v);
        // path_flow = Math.min(path_flow, rGraph.get(u).get(v));
        path_flow = Math.min(path_flow, rGraph[u][v]);
      }

      // update residual capacities of the edges and
      // reverse edges along the path
      for (v = t; v != s; v = parent.get(v)) {
        // u = parent[v];
        u = parent.get(v);
        rGraph[u][v] -= path_flow;
        rGraph[v][u] += path_flow;
        // rGraph.get(u).set(v, rGraph.get(u).get(v) - path_flow);
        // rGraph.get(v).set(u, rGraph.get(v).get(u) + path_flow);
      }
      // System.out.println(Arrays.deepToString(rGraph));

      // Add path flow to overall flow
      max_flow += path_flow;
    }
    // Return the overall flow
    return max_flow;
  }

  // Driver program to test above functions
  public static void main(String[] args) throws java.lang.Exception {
    // Let us create a graph shown in the above example

    MaxFlowBak m = new MaxFlowBak();

    // graph = new int[][] { { 0, 3, 2, 0 }, { 0, 0, 5, 2 }, { 0, 0, 0, 3 }, { 0, 0,
    // 0, 0 } };
    // System.out.println("The maximum possible flow is " + m.fordFulkerson(graph,
    // 0, 3));

    // clrs: figure 26.6
    int graph[][] =
        new int[][] {
          {0, 16, 13, 0, 0, 0},
          {0, 0, 0, 12, 0, 0},
          {0, 4, 0, 0, 14, 0},
          {0, 0, 9, 0, 0, 20},
          {0, 0, 0, 7, 0, 4},
          {0, 0, 0, 0, 0, 0}
        };

    // System.out.println(Arrays.toString(graph));
    // System.out.println(Arrays.deepToString(graph));
    // String result =
    // Arrays.stream(graph).map(Arrays::toString).collect(Collectors.joining(System.lineSeparator()));
    // System.out.println(result);

    System.out.println("The maximum possible flow is " + m.fordFulkerson(graph, 0, 5));
  }
}
