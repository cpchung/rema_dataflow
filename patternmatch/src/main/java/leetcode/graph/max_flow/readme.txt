In the maximum-flow problem, we wish to compute the greatest rate at which
we can ship material from the source to the sink without violating any capacity
constraints.

This chapter presents two general methods for solving the maximum-flow prob-
lem.

Section 26.1 formalizes the notions of flow networks and flows, formally
defining the maximum-flow problem.

Section 26.2 describes the classical method
of Ford and Fulkerson for finding maximum flows.

Section 26.3 An application of this method
finding a maximum matching in an undirected bipartite graph.

Section 26.4 presents the push-relabel method, which underlies many of
the fastest algorithms for network-flow problems.

Section 26.5 covers the “relabel-to-front” algorithm, a particular implementation of the push-relabel method that
runs in time O(V^3). Although this algorithm is not the fastest algorithm known,
it illustrates some of the techniques used in the asymptotically fastest algorithms,
and it is reasonably efficient in practice.