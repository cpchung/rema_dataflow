Section 25.1 presents a DP based on matrix multiplication to solve the all-pairs shortest-paths(APSP).
Using the technique of “repeated squaring,” we can achieve a running time of O(V^3 lg V).

Section 25.2 gives another DP, the Floyd-Warshall algorithm, which
runs in time O(V^3).
Section 25.2 also covers  transitive closure of a directed graph,
which is related to the all-pairs shortest-paths problem.

Finally, Section 25.3 presents Johnson’s algorithm, which solves the all-
pairs shortest-paths problem in O(V^2lgV+VE) time and is a good choice for
large, sparse graphs.