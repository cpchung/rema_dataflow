package leetcode.graph.apsp;

import java.util.Arrays;
// https://leetcode.com/problems/network-delay-time/discuss/183873/Java-solutions-using-Dijkstra-FloydWarshall-and-Bellman-Ford-algorithm

public class FloydWarshall {

  public static void main(String[] args) {
    FloydWarshall networkDelayTime = new FloydWarshall();

    networkDelayTime.networkDelayTime(new int[][] {{2, 1, 1}, {2, 3, 1}, {3, 4, 1}}, 4, 2);
  }

  public int networkDelayTime(int[][] times, int N, int K) {
    double[][] disTo = new double[N][N];
    for (int i = 0; i < N; i++) {
      Arrays.fill(disTo[i], Double.POSITIVE_INFINITY);
    }
    for (int i = 0; i < N; i++) {
      disTo[i][i] = 0;
    }
    for (int[] edge : times) {
      disTo[edge[0] - 1][edge[1] - 1] = edge[2];
    }
    for (int k = 0; k < N; k++) {
      for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
          if (disTo[i][j] > disTo[i][k] + disTo[k][j]) disTo[i][j] = disTo[i][k] + disTo[k][j];
        }
      }
    }
    double max = Double.MIN_VALUE;
    for (int i = 0; i < N; i++) {
      if (disTo[K - 1][i] == Double.POSITIVE_INFINITY) return -1;
      max = Math.max(max, disTo[K - 1][i]);
    }
    return (int) max;
  }
}
