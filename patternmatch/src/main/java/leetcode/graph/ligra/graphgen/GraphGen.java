/* (C)2023 */
package leetcode.graph.ligra.graphgen;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import leetcode.graph.Edge;
import leetcode.graph.Graph;
import leetcode.graph.Vertex;

public class GraphGen {

  static Graph adjList2dag(int V, int[][] edgeList) {
    int E = edgeList.length;

    Vertex vertices[] = new Vertex[V];
    for (int i = 0; i < V; i++) {
      vertices[i] = Vertex.builder().id(i).outNeighbors(new LinkedList<>()).build();
    }

    Map<Integer, Map<Integer, Integer>> weights = new HashMap<>();
    Edge edge[] = new Edge[E];
    for (int i = 0; i < edgeList.length; ++i) {
      int u = edgeList[i][0];
      int v = edgeList[i][1];
      int w = edgeList[i][2];

      edge[i] = Edge.builder().src(vertices[u]).dst(vertices[v]).weight(w).build();

      vertices[u].outNeighbors.add(vertices[v]);

      if (!weights.containsKey(u)) {
        weights.put(u, new HashMap<>());
      }
      weights.get(u).put(v, w);
    }

    Graph graph = new Graph();

    graph.V = V;
    graph.E = E;
    graph.edges = edge;
    graph.vertices = vertices;
    graph.weight = weights;

    return graph;
  }

  public static Graph bellmanFordExample() {

    //        BellmanFord page 652 CLRS
    //        s,t,x,y,z
    //        0,1,2,3,4

    Map<Integer, Character> int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 's');
            put(1, 't');
            put(2, 'x');
            put(3, 'y');
            put(4, 'z');
          }
        };
    int V = 5;
    int[][] edgeList = {
      {0, 1, 6}, // s
      {0, 3, 7},
      {1, 2, 5}, // t
      {1, 3, 8},
      {1, 4, -4},
      {2, 1, -2}, // x
      {3, 2, -3}, // y
      {3, 4, 9},
      {4, 0, 2}, // z
      {4, 2, 7}
    };
    return adjList2dag(V, edgeList);
  }

  public static Graph dagssspExample() {

    //        dag-sssp page 6526CLRS
    //        s,t,x,y,z
    //        0,1,2,3,4
    Map<Integer, Character> int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 's');
            put(1, 't');
            put(2, 'x');
            put(3, 'y');
            put(4, 'z');
          }
        };
    int V = 5;
    int[][] edgeList = {
      {0, 1, 2}, // s
      {0, 2, 6},
      {1, 3, 4}, // t
      {1, 4, 2},
      {2, 3, -1}, // x
      {2, 4, 1},
      {3, 4, -2}, // y
    };
    return adjList2dag(V, edgeList);
  }

  public static Graph dijkstraExample() {

    //        dag-sssp page 659 CLRS
    //        s,t,x,y,z
    //        0,1,2,3,4
    Map<Integer, Character> int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 's');
            put(1, 't');
            put(2, 'x');
            put(3, 'y');
            put(4, 'z');
          }
        };
    int V = 5;
    int[][] edgeList = {
      {0, 1, 10}, // s
      {0, 3, 5},
      {1, 2, 1}, // t
      {1, 3, 2},
      {2, 4, 4}, // x
      {3, 1, 3}, // y
      {3, 2, 9},
      {3, 4, 2},
      {4, 0, 7}, // z
      {4, 2, 6}
    };

    return adjList2dag(V, edgeList);
  }

  public static Graph mstExample() {

    //        kruskal page 632 CLRS
    Map<Integer, Character> int2char =
        new HashMap<Integer, Character>() {
          {
            put(0, 'a');
            put(1, 'b');
            put(2, 'c');
            put(3, 'd');
            put(4, 'e');
            put(5, 'f');
            put(6, 'g');
            put(7, 'h');
            put(8, 'i');
          }
        };
    int V = int2char.size();
    //        edgeList notation for undirected_graph: src_id < dst_id and no symmetric edge
    int[][] edgeList = {
      {0, 1, 4}, // a
      {0, 7, 8},
      {1, 2, 8}, // b
      {1, 7, 11}, // b
      {2, 3, 7}, // c
      {2, 5, 4},
      {2, 8, 2},
      {3, 4, 9}, // d
      {3, 5, 14}, // d
      {4, 5, 10}, // e
      {5, 6, 2}, // f
      {6, 7, 1}, // g
      {6, 8, 6}, // g
      {7, 8, 7}, // h
    };
    int E = edgeList.length;

    Vertex vertices[] = new Vertex[V];
    for (int i = 0; i < V; i++) {
      vertices[i] = Vertex.builder().id(i).outNeighbors(new LinkedList<>()).build();
    }

    Map<Integer, Map<Integer, Integer>> weights = new HashMap<>();
    Edge[] edge = new Edge[E];
    for (int i = 0; i < edge.length; i += 1) {
      int u = edgeList[i][0];
      int v = edgeList[i][1];
      int w = edgeList[i][2];

      edge[i] = Edge.builder().src(vertices[u]).dst(vertices[v]).weight(w).build();

      //            edge[i + 1] = Edge.builder()
      //                    .src(vertices[v])
      //                    .dst(vertices[u])
      //                    .weight(w)
      //                    .build();

      vertices[u].outNeighbors.add(vertices[v]);
      vertices[v].outNeighbors.add(vertices[u]);

      if (!weights.containsKey(u)) {
        weights.put(u, new HashMap<>());
      }
      weights.get(u).put(v, w);

      if (!weights.containsKey(v)) {
        weights.put(v, new HashMap<>());
      }
      weights.get(v).put(u, w);
    }

    for (int i = 0; i < V; i++) {
      vertices[i].degree = vertices[i].outNeighbors.size();
    }

    Graph graph = new Graph();

    graph.vertices = vertices;
    graph.edges = edge;
    graph.weight = weights;
    graph.V = V;
    graph.E = E;

    return graph;
  }

  public static Graph geek4geekConnectedComponentExample() {
    //        https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/

    int V = 5, E = 3;
    Vertex vertices[] = new Vertex[V];
    for (int i = 0; i < V; i++) {
      vertices[i] = Vertex.builder().id(i).build();
    }

    vertices[0].degree = (2);
    vertices[1].degree = (2);
    vertices[2].degree = (2);

    vertices[0].outNeighbors = (new LinkedList<>(Arrays.asList(vertices[1], vertices[2])));
    vertices[1].outNeighbors = (new LinkedList<>(Arrays.asList(vertices[0], vertices[2])));
    vertices[2].outNeighbors = (new LinkedList<>(Arrays.asList(vertices[0], vertices[1])));

    vertices[3].degree = (1);
    vertices[4].degree = (1);
    vertices[3].outNeighbors = (new LinkedList<>(Arrays.asList(vertices[4])));
    vertices[4].outNeighbors = (new LinkedList<>(Arrays.asList(vertices[3])));

    Graph graph = new Graph();

    graph.vertices = vertices;
    graph.V = V;
    graph.E = E;

    return graph;
  }

  public static Graph leetcode323Example(int V, int[][] edges) {
    //        https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/
    //        int[][] edges = new int[][]{{0, 1}, {1, 2}, {2, 3}, {3, 4}};
    //        int V =5;

    LinkedList<Vertex> l = new LinkedList<>();

    Vertex vertices[] = new Vertex[V];
    for (int i = 0; i < V; i++) {
      vertices[i] = Vertex.builder().id(i).outNeighbors(new LinkedList<>()).build();
    }

    for (int i = 0; i < edges.length; i++) {
      int u = edges[i][0];
      int v = edges[i][1];
      l.get(u).outNeighbors.add(l.get(v));
      l.get(v).outNeighbors.add(l.get(u));
    }

    Graph graph = new Graph();

    graph.vertices = vertices;
    graph.V = V;
    //        graph.setNumEdges(3);

    return graph;
  }
}
