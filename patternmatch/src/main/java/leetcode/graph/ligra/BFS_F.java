package leetcode.graph.ligra;

// @lombok.Data
public class BFS_F {
  int[] parents;

  BFS_F(int[] parents) {
    this.parents = parents;
  }

  boolean cond(int d) {
    return parents[d] == Integer.MAX_VALUE;
  }

  boolean update(int s, int d) {
    if (parents[d] == Integer.MAX_VALUE) {
      parents[d] = s;
      return true;
    } else return false;
  }

  boolean updateAtomic(int s, int d) {
    //            todo:
    //            __sync_bool_compare_and_swap
    //
    // https://stackoverflow.com/questions/12799397/java-cas-operation-performs-faster-than-c-equivalent-why
    return true;
  }
}
