package leetcode.graph.ligra;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class BFSLigraV0 {

  public static void main(String[] args) {

    Map<Integer, List<Integer>> graph = new HashMap<>();
    graph.put(0, new LinkedList<>(Arrays.asList(1, 2, 3)));
    graph.put(1, new LinkedList<>(Arrays.asList(4)));
    graph.put(2, new LinkedList<>(Arrays.asList(4, 5, 6)));
    graph.put(3, new LinkedList<>(Arrays.asList(6, 7)));

    //        graph.put(0, new LinkedList<>(Arrays.asList(1, 2)));
    //        graph.put(3, new LinkedList<>(Arrays.asList(4)));

    BFSLigraV0 sol = new BFSLigraV0();
    sol.bfs(graph, 0);
  }

  void bfs(Map<Integer, List<Integer>> graph, int r) {

    List<Integer> parents = new LinkedList<>();
    for (int i = 0; i < 8; i++) {
      parents.add(-1);
    }
    parents.set(r, r);

    Queue<Integer> frontier = new LinkedList<>();
    frontier.add(r);

    while (!frontier.isEmpty()) {

      for (int f : frontier) System.out.println(f);

      Queue<Integer> output = edgeMap(graph, frontier, parents);

      frontier.clear();

      frontier = output;
    }
  }

  Queue<Integer> edgeMap(
      Map<Integer, List<Integer>> graph, Queue<Integer> frontier, List<Integer> parents) {

    Queue<Integer> out = new LinkedList<>();

    for (int front : frontier)
      for (int v : graph.getOrDefault(front, new LinkedList<>())) {
        if (parents.get(v) == -1) {
          out.add(v);
          parents.set(v, v);
        }
      }

    return out;
  }
}
