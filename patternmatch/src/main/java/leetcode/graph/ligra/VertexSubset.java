package leetcode.graph.ligra;

import java.util.List;

class VertexSubset {
  int n;
  int m;
  boolean isDense;

  //    using S = tuple<uintE, data>;
  List<Integer> s;

  long numNonzeros() {
    return m;
  }

  Integer vtx(int i) {
    return s.get(i);
  }
}
