package leetcode.graph.ligra.updateFunction;

abstract class F {

  public abstract boolean update(int src, int dst);

  public abstract boolean updateAtomic();

  public abstract boolean cond(int i);
}
