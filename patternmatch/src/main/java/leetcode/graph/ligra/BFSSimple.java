package leetcode.graph.ligra;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class BFSSimple {

  public static void main(String[] args) {

    Map<Integer, List<Integer>> graph = new HashMap<>();
    //        graph.put(0, new LinkedList<>(Arrays.asList(1, 2, 3)));
    //        graph.put(1, new LinkedList<>(Arrays.asList(4)));
    //        graph.put(2, new LinkedList<>(Arrays.asList(4, 5, 6)));
    //        graph.put(3, new LinkedList<>(Arrays.asList(6, 7)));

    graph.put(0, new LinkedList<>(Arrays.asList(1, 2)));
    graph.put(3, new LinkedList<>(Arrays.asList(4)));

    BFSSimple sol = new BFSSimple();

    int n = 5;
    List<Integer> parents = new LinkedList<>();
    for (int i = 0; i < n; i++) {
      parents.add(-1);
    }

    //        sol.bfs(graph, 0, parents);
    sol.cc(graph);
  }

  void bfs(Map<Integer, List<Integer>> graph, int r, List<Integer> parents) {

    parents.set(r, r);

    Queue<Integer> frontier = new LinkedList<>();
    frontier.add(r);

    while (!frontier.isEmpty()) {
      int front = frontier.remove();

      System.out.print(front);
      System.out.print(", ");

      for (int v : graph.getOrDefault(front, new LinkedList<>())) {
        if (parents.get(v) == -1) {
          frontier.add(v);
          parents.set(v, v);
        }
      }
    }
  }

  void cc(Map<Integer, List<Integer>> graph) {
    int n = 5;
    List<Integer> parents = new LinkedList<>();
    for (int i = 0; i < n; i++) {
      parents.add(-1);
    }

    for (int i = 0; i < n; i++) {
      if (parents.get(i) == -1) {
        bfs(graph, i, parents);
        System.out.println();
      }
    }
  }
}
