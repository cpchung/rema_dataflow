package leetcode.graph.ligra;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import leetcode.graph.Graph;
import leetcode.graph.ligra.graphgen.GraphGen;

public class BFSLigra {

  public static void main(String[] args) {

    //        Graph graph = GraphGen.geek4geekConnectedComponentExample();
    int[][] edges = new int[][] {{0, 1}, {1, 2}, {2, 3}, {3, 4}};

    Graph graph = GraphGen.leetcode323Example(5, edges);

    BFSLigra.bfs(graph);

    List<Integer> res = BFSLigra.connectedComponents(graph);

    Set<Integer> foo = new HashSet<Integer>(res);
    foo.size();
  }

  static void bfs(Graph graph) {
    List<Integer> parents = new LinkedList<>();
    for (int i = 0; i < graph.V; i++) {
      parents.add(-1);
    }
    int start1 = 0;
    int start2 = 3;
    parents.set(start1, start1);
    parents.set(start2, start2);

    Queue<Integer> frontier = new LinkedList<>();
    frontier.add(start1);
    frontier.add(start2);

    while (!frontier.isEmpty()) {

      for (int f : frontier) System.out.println(f);

      Queue<Integer> output = edgeMapBFS(graph, frontier, parents);

      frontier.clear();

      frontier = output;
    }
  }

  static List<Integer> connectedComponents(Graph graph) {
    List<Integer> IDs = new LinkedList<>();
    List<Integer> prevIDs = new LinkedList<>();
    Queue<Integer> frontier = new LinkedList<>();

    for (int i = 0; i < graph.V; i++) {
      IDs.add(i);
      prevIDs.add(i);
      frontier.add(i);
    }

    while (!frontier.isEmpty()) {

      for (int f : frontier) {
        prevIDs.set(f, IDs.get(f));
      }

      Queue<Integer> out = new LinkedList<>();

      for (int front : frontier)

        //            for (Vertex v : graph.getVertices().get(front)) {}
        // todo:
        //  need to implement iterator
        //
        // https://stackoverflow.com/questions/975383/how-can-i-use-the-java-for-each-loop-with-custom-classes

        for (int i = 0; i < graph.vertices[front].outNeighbors.size(); i++) {
          int v = graph.vertices[front].outNeighbors.get(i).id;
          if (prevIDs.get(v) < prevIDs.get(front)) {
            out.add(front);
            IDs.set(front, prevIDs.get(v));
          }
        }
      //            Queue<Integer> out = edgeMapCC(graph, frontier, IDs);
      frontier.clear();
      frontier = out;
    }

    return IDs;
  }

  Queue<Integer> edgeMapCC(Graph graph, Queue<Integer> frontier, List<Integer> parents) {

    Queue<Integer> out = new LinkedList<>();

    for (int front : frontier)

      //            for (Vertex v : graph.getVertices().get(front)) {}
      // todo:
      //  need to implement iterator
      //
      // https://stackoverflow.com/questions/975383/how-can-i-use-the-java-for-each-loop-with-custom-classes

      for (int i = 0; i < graph.vertices[front].outNeighbors.size(); i++) {
        int v = graph.vertices[front].outNeighbors.get(i).id;
        if (parents.get(v) == -1) {
          out.add(v);
          parents.set(v, v);
        }
      }

    return out;
  }

  static Queue<Integer> edgeMapBFS(Graph graph, Queue<Integer> frontier, List<Integer> parents) {

    Queue<Integer> out = new LinkedList<>();

    for (int front : frontier)

      //            for (Vertex v : graph.getVertices().get(front)) {}
      // todo:
      //  need to implement iterator
      //
      // https://stackoverflow.com/questions/975383/how-can-i-use-the-java-for-each-loop-with-custom-classes

      for (int i = 0; i < graph.vertices[front].outNeighbors.size(); i++) {
        int v = graph.vertices[front].outNeighbors.get(i).id;
        if (parents.get(v) == -1) {
          out.add(v);
          parents.set(v, v);
        }
      }

    return out;
  }
}
