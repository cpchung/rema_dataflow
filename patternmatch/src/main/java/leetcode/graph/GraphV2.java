package leetcode.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GraphV2 {

  public Map<Integer, Vertex> id2Vertex;
  public Map<Integer, Edge> id2Edge; // edgeID and nodeID can be different
  private Map<Integer, Set<Integer>> src2dst;

  public void construct() {

    if (this.id2Edge != null) {
      //      construct src2dst from id2Edge
      Map<Integer, Set<Integer>> src2dst = new HashMap<>();
      for (Edge edge : this.id2Edge.values()) {
        int src = edge.getSrcID();
        int dst = edge.getDstID();

        if (!src2dst.containsKey(src)) {
          src2dst.put(src, new HashSet<>());
        }
        if (!src2dst.containsKey(dst)) {
          src2dst.put(dst, new HashSet<>());
        }
        src2dst.get(src).add(dst);
        src2dst.get(dst).add(src);
      }
      this.setSrc2dst(src2dst);
    } else {
      //      construct id2Edge from src2dst
      assert this.getSrc2dst() != null;
      this.id2Edge = new HashMap<>();
      int edgeID = 0;
      for (Entry<Integer, Set<Integer>> entry : this.getSrc2dst().entrySet()) {
        Integer src = entry.getKey();
        for (Integer dst : entry.getValue()) {
          this.getId2Edge().put(edgeID, Edge.builder().id(edgeID).srcID(src).dstID(dst).build());
          edgeID++;
        }
      }
    }
  }
}
