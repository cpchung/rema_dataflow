package leetcode.graph.crawler;

import java.util.List;

/**
 * // This is the HtmlParser's API interface. // You should not implement it, or speculate about its
 * implementation
 */
public interface HtmlParser {
  List<String> getUrls(String url);
}
