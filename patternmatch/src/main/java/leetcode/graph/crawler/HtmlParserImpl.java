package leetcode.graph.crawler;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import lombok.Data;

@Data
public class HtmlParserImpl implements HtmlParser {

  public Map<String, List<String>> url2urls;
  public Example example;

  public HtmlParserImpl(String path) {
    this.url2urls = new HashMap<>();

    Gson gson = new Gson();

    Reader reader;

    {
      try {
        reader = Files.newBufferedReader(Paths.get(path));

        example = gson.fromJson(reader, Example.class);

        List<String> urls = example.getUrls();

        for (List<Integer> list : example.getLinks()) {

          String src = urls.get(list.get(0));

          String dst = urls.get(list.get(1));

          if (!url2urls.containsKey(src)) {
            url2urls.put(src, new LinkedList<>());
          }

          url2urls.get(src).add(dst);
        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public List<String> getUrls(String url) {

    if (url.isEmpty()) {
      System.out.println("isEmptyu()");
    }

    if (!url2urls.containsKey(url)) {
      System.out.println("map does not have key: " + url);
      return Collections.emptyList();
    }
    return url2urls.get(url);
  }
}
