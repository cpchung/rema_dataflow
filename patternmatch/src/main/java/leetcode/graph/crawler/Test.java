package leetcode.graph.crawler;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;

public class Test {

  public static void main(String[] args) throws IOException {

    Gson gson = new Gson();

    String path =
        "/home/cpchung/Dropbox/leetcode/java/src/main/java/leetcode/graph/crawler/testLarge.json";

    //        path =
    // "/home/cpchung/Dropbox/leetcode/java/src/main/java/leetcode/graph/crawler/test1.json";

    Reader reader = Files.newBufferedReader(Paths.get(path));

    Example example = gson.fromJson(reader, Example.class);
    HtmlParser htmlParser = new HtmlParserImpl(path);

    Solution sol = new Solution();
    //        srfg sol = new srfg();
    List<String> res = sol.crawl(example.getStart(), htmlParser);

    System.out.println(res);

    System.out.println(new HashSet<>(res).equals(new HashSet<>(example.getExpected())));
  }
}
