package leetcode.graph.crawler;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.processing.Generated;
import lombok.Data;

@Generated("jsonschema2pojo")
@Data
class Example {
  @SerializedName("start")
  @Expose
  public String start;

  @SerializedName("expected")
  @Expose
  public List<String> expected = null;

  @SerializedName("links")
  @Expose
  public List<List<Integer>> links = null;

  @SerializedName("urls")
  @Expose
  public List<String> urls = null;
}
