package leetcode.graph.crawler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Solution {
  private String getHostname(String url) {
    int idx = url.indexOf('/', 7);
    return (idx != -1) ? url.substring(0, idx) : url;
  }

  private boolean isSameHostname(String url, String hostname) {
    if (!url.startsWith(hostname)) {
      return false;
    }
    return url.length() == hostname.length() || url.charAt(hostname.length()) == '/';
  }

  //
  // https://leetcode.com/problems/web-crawler-multithreaded/discuss/419264/Java-Streams-%20-ConcurrentHashMap-(60-ms)
  List<String> edgeMap(
      List<String> frontier, Set<String> visited, HtmlParser htmlParser, String hostname) {
    //        frontier.parallelStream()
    //                .forEach(
    //                        front -> {
    //                            List<String> neighbor = htmlParser.getUrls(front);
    ////                            for (String v : neighbor) {
    //                            Collection<String> neighborCollection =
    // Optional.ofNullable(neighbor).orElse
    //                            (Collections.emptyList());
    //                            for (String v : neighborCollection) {
    //                                if (!visited.contains(v) && isSameHostname(v, hostname)) {
    //                                    out.add(v);
    //                                    visited.add(v);
    //                                }
    //                            }
    //                        }
    //                );

    List<String> out = new LinkedList<>();
    frontier.parallelStream()
        .filter(e -> !e.isEmpty())
        .flatMap(url -> htmlParser.getUrls(url).parallelStream())
        .filter(url -> isSameHostname(url, hostname))
        .filter(url -> visited.add(url))
        .forEach(
            url -> {

              //                    System.out.println("forEach: " + url);
              //                    System.out.println("sameHost:: " + isSameHostname(url,
              // hostname));
              //                    System.out.println("visited:: " + visited.add(url));
              out.add(url);
            });
    //                .map(url -> out.add(url));

    //                .collect(Collectors.toList());
    return out;
  }

  public List<String> crawl(String startUrl, HtmlParser htmlParser) {
    List<String> frontier = new LinkedList<>();

    frontier.add(startUrl);

    String hostname = getHostname(startUrl);
    //        Set<String> visited = new ConcurrentSkipListSet<>();
    Set<String> visited = ConcurrentHashMap.newKeySet();
    visited.add(startUrl);
    List<String> out = new LinkedList<>();
    while (!frontier.isEmpty()) {
      out = edgeMap(frontier, visited, htmlParser, hostname);
      //            frontier.clear();
      frontier = out;
    }

    return new ArrayList<>(visited);
  }
}
