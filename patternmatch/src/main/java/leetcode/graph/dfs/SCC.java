package leetcode.graph.dfs;

public class SCC {

  //    page 617: 22.5 using DFS
  //    S TRONGLY -C ONNECTED -C OMPONENTS .G/
  //            1 call DFS.G/ to compute finishing times u:f for each vertex u
  // 2 compute G T
  // 3 call DFS.G T /, but in the main loop of DFS, consider the vertices
  //    in order of decreasing u:f (as computed in line 1)
  // 4 output the vertices of each tree in the depth-first forest formed in line 3 as a
  //    separate strongly connected component
}
