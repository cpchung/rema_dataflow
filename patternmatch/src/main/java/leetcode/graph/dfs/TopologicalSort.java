package leetcode.graph.dfs;

import leetcode.graph.Graph;

public class TopologicalSort {

  static int[] topologicalSort(Graph g) {

    int[] topologicalOrder = new DFS().dfs(g);

    return topologicalOrder;
  }
}
