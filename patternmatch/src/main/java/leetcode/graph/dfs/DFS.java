package leetcode.graph.dfs;

import leetcode.graph.Graph;
import leetcode.graph.Vertex;

public class DFS {

  int topologicalOrder[];
  int count;

  public int[] dfs(Graph g) {

    this.topologicalOrder = new int[g.vertices.length];
    this.count = g.vertices.length - 1;

    for (Vertex u : g.vertices) {
      u.color = Vertex.Color.WHITE;
      u.predecessor = null;
    }
    Integer time = 0;
    for (Vertex u : g.vertices) {
      if (u.color == Vertex.Color.WHITE) visit(g, u, time);
    }

    return topologicalOrder;
  }

  void visit(Graph g, Vertex u, Integer time) {
    time++;
    u.pre = time;
    u.color = Vertex.Color.GRAY;

    for (Vertex v : u.outNeighbors) {
      if (v.color == Vertex.Color.WHITE) {
        v.predecessor = u;
        visit(g, v, time);
      }
    }
    u.color = Vertex.Color.BLACK;
    time++;
    u.post = time;

    this.topologicalOrder[this.count--] = u.id;
  }
}
