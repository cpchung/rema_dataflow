package leetcode.graph.topologicalSort;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class CourseScheduleII {
  public static void main(String[] args) {

    CourseScheduleII sol = new CourseScheduleII();

    int[] res;
    res = sol.findOrder(2, new int[][] {{1, 0}});

    //        Input: 4, [[1,0],[2,0],[3,1],[3,2]]
    //        Output: [0,1,2,3] or [0,2,1,3]
    //        Explanation: There are a total of 4 courses to take. To take course 3 you should have
    // finished both
    //        courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
    //        So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3] .
    res = sol.findOrder(4, new int[][] {{1, 0}, {2, 0}, {3, 1}, {3, 2}});

    System.out.println(Arrays.toString(res));
    int[] res2 = sol.findOrderBFS(4, new int[][] {{1, 0}, {2, 0}, {3, 1}, {3, 2}});
    System.out.println(Arrays.equals(res, res2));
  }

  public int[] findOrderBFS(int numCourses, int[][] prerequisites) {
    if (numCourses == 0) return null;
    // Convert graph presentation from edges to indegree of adjacent list.
    int indegree[] = new int[numCourses], order[] = new int[numCourses], index = 0;
    for (int i = 0; i < prerequisites.length; i++) // Indegree - how many prerequisites are needed.
    indegree[prerequisites[i][0]]++;

    Queue<Integer> queue = new LinkedList<Integer>();
    for (int i = 0; i < numCourses; i++)
      if (indegree[i] == 0) {
        // Add the course to the order because it has no prerequisites.
        order[index++] = i;
        queue.offer(i);
      }

    // How many courses don't need prerequisites.
    while (!queue.isEmpty()) {
      int prerequisite = queue.poll(); // Already finished this prerequisite course.
      for (int i = 0; i < prerequisites.length; i++) {
        if (prerequisites[i][1] == prerequisite) {
          indegree[prerequisites[i][0]]--;
          if (indegree[prerequisites[i][0]] == 0) {
            // If indegree is zero, then add the course to the order.
            order[index++] = prerequisites[i][0];
            queue.offer(prerequisites[i][0]);
          }
        }
      }
    }

    return (index == numCourses) ? order : new int[0];
  }

  public int[] findOrder(int numCourses, int[][] prerequisites) {
    int[] incLinkCounts = new int[numCourses];
    List<List<Integer>> adjs = new ArrayList<>(numCourses);
    initialiseGraph(incLinkCounts, adjs, prerequisites);
    // return solveByBFS(incLinkCounts, adjs);
    return solveByDFS(adjs);
  }

  private void initialiseGraph(
      int[] incLinkCounts, List<List<Integer>> adjs, int[][] prerequisites) {
    int n = incLinkCounts.length;
    while (n-- > 0) adjs.add(new ArrayList<>());
    for (int[] edge : prerequisites) {
      incLinkCounts[edge[0]]++;
      adjs.get(edge[1]).add(edge[0]);
    }
  }

  private int[] solveByBFS(int[] incLinkCounts, List<List<Integer>> adjs) {
    int[] order = new int[incLinkCounts.length];
    Queue<Integer> toVisit = new ArrayDeque<>();
    for (int i = 0; i < incLinkCounts.length; i++) {
      if (incLinkCounts[i] == 0) toVisit.offer(i);
    }
    int visited = 0;
    while (!toVisit.isEmpty()) {
      int from = toVisit.poll();
      order[visited++] = from;
      for (int to : adjs.get(from)) {
        incLinkCounts[to]--;
        if (incLinkCounts[to] == 0) toVisit.offer(to);
      }
    }
    return visited == incLinkCounts.length ? order : new int[0];
  }

  private int[] solveByDFS(List<List<Integer>> adjs) {
    BitSet hasCycle = new BitSet(1);
    BitSet visited = new BitSet(adjs.size());
    BitSet onStack = new BitSet(adjs.size());
    Deque<Integer> order = new ArrayDeque<>();
    for (int i = adjs.size() - 1; i >= 0; i--) {
      if (visited.get(i) == false && hasOrder(i, adjs, visited, onStack, order) == false)
        return new int[0];
    }
    int[] orderArray = new int[adjs.size()];
    for (int i = 0; !order.isEmpty(); i++) orderArray[i] = order.pop();
    return orderArray;
  }

  private boolean hasOrder(
      int from, List<List<Integer>> adjs, BitSet visited, BitSet onStack, Deque<Integer> order) {
    visited.set(from);
    onStack.set(from);
    for (int to : adjs.get(from)) {
      if (visited.get(to) == false) {
        if (hasOrder(to, adjs, visited, onStack, order) == false) return false;
      } else if (onStack.get(to) == true) {
        return false;
      }
    }
    onStack.clear(from);
    order.push(from);
    return true;
  }
}
