/* (C)2023 */
package leetcode.graph.topologicalSort;

import java.util.LinkedList;
import java.util.Queue;

public class CourseSchedule {
  public static void main(String[] args) {
    //        int numCourses = 2;
    //        int[][] prerequisites = new int[][]{{1, 0}, {0, 1}};

    int numCourses = 2;
    int[][] prerequisites = new int[][] {{1, 0}};

    //        CourseSchedule courseSchedule = new CourseSchedule();
    CourseScheduleSCC courseSchedule = new CourseScheduleSCC();

    boolean res = courseSchedule.canFinish(numCourses, prerequisites);

    System.out.println(res);
  }

  // Topological Sort
  public boolean canFinish(int numCourses, int[][] prerequisites) {
    int n = numCourses;
    int[] inDegree = new int[n];
    int[][] matrix = new int[n][n];

    for (int i = 0; i < prerequisites.length; i++) {
      int r = prerequisites[i][1];
      int c = prerequisites[i][0];

      if (matrix[r][c] == 0) {
        matrix[r][c] = 1;
        inDegree[c]++;
      }
    }

    Queue<Integer> q = new LinkedList<>();
    for (int i = 0; i < n; i++) {
      if (inDegree[i] == 0) q.add(i);
    }
    int count = 0;
    while (q.size() != 0) {
      // var temp = q.poll();
      int temp = q.element();
      q.remove();
      count++;

      for (int i = 0; i < n; i++) {
        if (matrix[temp][i] == 1) {
          if (--inDegree[i] == 0) q.add(i);
        }
      }
    }

    return count == n;
  }
}
