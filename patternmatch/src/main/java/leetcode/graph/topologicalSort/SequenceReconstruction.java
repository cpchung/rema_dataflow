package leetcode.graph.topologicalSort;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

// https://leetcode.com/problems/sequence-reconstruction/discuss/92580/Java-Solution-using-BFS-Topological-Sort
public class SequenceReconstruction {

  public static void main(String[] args) {

    //        int[] org = {1, 2, 3};
    //        List<List<Integer>>  seqs = Arrays.asList(
    //                Arrays.asList(1, 2),
    //                Arrays.asList(1, 3)
    //        );

    int[] org = {4, 1, 5, 2, 6, 3};
    List<List<Integer>> seqs = Arrays.asList(Arrays.asList(5, 2, 6, 3), Arrays.asList(4, 1, 5, 2));

    //        SequenceReconstruction.sequenceReconstruction(org, seqs);
    SequenceReconstruction.sequenceReconstructionWiki(org, seqs);
  }

  //    public boolean sequenceReconstruction(int[] org, int[][] seqs) {
  public static boolean sequenceReconstruction(int[] org, List<List<Integer>> seqs_) {

    int[][] seqs =
        seqs_.stream()
            .map(l -> l.stream().mapToInt(Integer::intValue).toArray())
            .toArray(int[][]::new);

    Map<Integer, Set<Integer>> adj = new HashMap<>();
    Map<Integer, Integer> inDegree = new HashMap<>();

    for (int[] seq : seqs) {
      if (seq.length == 1) {
        if (!adj.containsKey(seq[0])) {
          adj.put(seq[0], new HashSet<>());
          inDegree.put(seq[0], 0);
        }
      } else {
        for (int i = 0; i < seq.length - 1; i++) {
          if (!adj.containsKey(seq[i])) {
            adj.put(seq[i], new HashSet<>());
            inDegree.put(seq[i], 0);
          }

          if (!adj.containsKey(seq[i + 1])) {
            adj.put(seq[i + 1], new HashSet<>());
            inDegree.put(seq[i + 1], 0);
          }

          if (adj.get(seq[i]).add(seq[i + 1])) {
            inDegree.put(seq[i + 1], inDegree.get(seq[i + 1]) + 1);
          }
        }
      }
    }

    Queue<Integer> queue = new LinkedList<>();
    for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
      if (entry.getValue() == 0) queue.offer(entry.getKey());
    }

    int index = 0;
    while (!queue.isEmpty()) {
      int size = queue.size();
      if (size > 1) return false;
      int curr = queue.poll();
      if (index == org.length || curr != org[index]) return false;
      index++;
      for (int next : adj.get(curr)) {
        inDegree.put(next, inDegree.get(next) - 1);
        if (inDegree.get(next) == 0) queue.offer(next);
      }
    }
    return index == org.length && index == adj.size();
  }

  //
  // https://leetcode.com/problems/sequence-reconstruction/discuss/92572/Simple-Solution-%3A-one-pass-using-only-array-(C%2B%2B-92ms-Java-16ms)
  public static boolean sequenceReconstructionWiki(int[] org, List<List<Integer>> seqs_) {

    int[][] seqs =
        seqs_.stream()
            .map(l -> l.stream().mapToInt(Integer::intValue).toArray())
            .toArray(int[][]::new);

    if (seqs.length == 0) return false;
    int[] pos = new int[org.length + 1];
    for (int i = 0; i < org.length; ++i) pos[org[i]] = i;

    boolean[] flags = new boolean[org.length + 1];
    int toMatch = org.length - 1;
    for (int[] v : seqs) {
      for (int i = 0; i < v.length; ++i) {
        if (v[i] <= 0 || v[i] > org.length) return false;
        if (i == 0) continue;
        int x = v[i - 1], y = v[i];
        if (pos[x] >= pos[y]) return false;
        if (flags[x] == false && pos[x] + 1 == pos[y]) {
          flags[x] = true;
          --toMatch;
        }
      }
    }
    return toMatch == 0;
  }
}
