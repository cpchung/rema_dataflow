package leetcode.graph.topologicalSort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

// https://leetcode.com/problems/course-schedule/discuss/380289/Solution-by-Finding-Strongly-Connected-Components
public class CourseScheduleSCC {

  public boolean canFinish(int numCourses, int[][] prerequisites) {
    Set<Integer> set = new HashSet<>();
    List<List<Integer>> res = getAllSCC(prerequisites, set);
    return set.size() == res.size();
  }

  public List<List<Integer>> getAllSCC(int[][] edges, Set<Integer> s) {
    Map<Integer, List<Integer>> graph = new HashMap<>();
    Map<Integer, List<Integer>> transposeGraph = new HashMap<>();
    for (int[] edge : edges) {
      s.add(edge[0]);
      s.add(edge[1]);
      if (!graph.containsKey(edge[0])) graph.put(edge[0], new ArrayList<>());
      if (!transposeGraph.containsKey(edge[1])) transposeGraph.put(edge[1], new ArrayList<>());
      graph.get(edge[0]).add(edge[1]);
      transposeGraph.get(edge[1]).add(edge[0]);
    }
    Stack<Integer> stack = new Stack<>();
    Set<Integer> set = new HashSet<>();
    for (int node : graph.keySet()) {
      dfs(node, graph, set, stack);
    }
    List<List<Integer>> res = new ArrayList<>();
    set.clear();
    while (!stack.isEmpty()) {
      List<Integer> list = bfs(stack.pop(), transposeGraph, set);
      if (list.size() > 0) res.add(list);
    }
    return res;
  }

  private void dfs(
      int node, Map<Integer, List<Integer>> graph, Set<Integer> set, Stack<Integer> stack) {
    if (set.contains(node)) return;
    set.add(node);
    if (graph.containsKey(node)) {
      for (int child : graph.get(node)) {
        dfs(child, graph, set, stack);
      }
    }
    stack.push(node);
  }

  private List<Integer> bfs(int node, Map<Integer, List<Integer>> graph, Set<Integer> set) {
    List<Integer> res = new ArrayList<>();
    if (set.contains(node)) return res;
    Queue<Integer> queue = new LinkedList<>();
    queue.offer(node);
    set.add(node);
    while (!queue.isEmpty()) {
      int size = queue.size();
      while (size-- > 0) {
        int n = queue.poll();
        res.add(n);
        if (!graph.containsKey(n)) continue;
        for (int child : graph.get(n)) {
          if (set.contains(child)) continue;
          queue.offer(child);
          set.add(child);
        }
      }
    }
    return res;
  }
}
