package leetcode.graph;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

// @NoArgsConstructor //cannot use with @Builder  at the same time
// @Data
// @ToString  //cannot use this when there is recursive def of member variable, like List<Vertex>
// below
// @EqualsAndHashCode
@Getter
@Setter
// @RequiredArgsConstructor
@Builder
public class Vertex implements Comparable<Vertex> {

  public int id;
  //    TODO: Vertex should not contain neighbour info.
  //     Neighbour info should be stored in Graph, not on edges
  public List<Vertex> outNeighbors; // undirected graph
  public int degree;

  public int distance = 0;
  public Vertex predecessor; // pi, directed graph

  public String label;

  public int pre;
  public int post;

  public Color color;

  @Override
  public int compareTo(Vertex vertex) {
    //        I want vertices to be in descending order by id so that
    //        they can be added to stack in descending order and
    //        then popped out in ascending order
    if (id > vertex.id) {
      return -1;
    } else if (id < vertex.id) {
      return 1;
    } else {
      return 0;
    }
  }

  public String toString() {
    return "Vertex(id= " + id + ") ";
  }

  public enum Color {
    WHITE,
    GRAY,
    BLACK
  }
}
