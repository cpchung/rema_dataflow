/* (C)2023 */
package loader;

public class MyLibrary {
  public void sayHello() {
    System.out.println("Hello from My Library!");
  }
}
