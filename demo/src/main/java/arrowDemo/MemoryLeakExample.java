/* (C)2023 */
package arrowDemo;

import static java.util.Arrays.asList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.arrow.algorithm.search.VectorSearcher;
import org.apache.arrow.algorithm.sort.DefaultVectorComparators;
import org.apache.arrow.algorithm.sort.VectorValueComparator;
import org.apache.arrow.dataset.file.FileFormat;
import org.apache.arrow.dataset.file.FileSystemDatasetFactory;
import org.apache.arrow.dataset.jni.NativeMemoryPool;
import org.apache.arrow.dataset.scanner.ScanOptions;
import org.apache.arrow.dataset.scanner.Scanner;
import org.apache.arrow.dataset.source.Dataset;
import org.apache.arrow.dataset.source.DatasetFactory;
import org.apache.arrow.memory.ArrowBuf;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.util.AutoCloseables;
import org.apache.arrow.vector.BigIntVector;
import org.apache.arrow.vector.BitVector;
import org.apache.arrow.vector.FieldVector;
import org.apache.arrow.vector.IntVector;
import org.apache.arrow.vector.VarCharVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.ipc.ArrowFileWriter;
import org.apache.arrow.vector.ipc.ArrowReader;
import org.apache.arrow.vector.types.Types;
import org.apache.arrow.vector.types.pojo.ArrowType;
import org.apache.arrow.vector.types.pojo.Field;
import org.apache.arrow.vector.types.pojo.FieldType;
import org.apache.arrow.vector.types.pojo.Schema;

public class MemoryLeakExample {

  static void readFromParquetFile() {
//    String filePath = "file:/Users/chak-pong.chung/dev/scripts/File1.parquet";
    String filePath = "file:/home/cpchung/code/rema_dataflow/demo/src/main/resources/File1.parquet";

    String uri = filePath;
    ScanOptions options = new ScanOptions(/*batchSize*/ 1200000);
    try (BufferAllocator allocator = new RootAllocator(); DatasetFactory datasetFactory = new FileSystemDatasetFactory(
        allocator, NativeMemoryPool.getDefault(), FileFormat.PARQUET, uri);
//        Dataset dataset = datasetFactory.finish();

        Dataset dataset = datasetFactory.finish(
            datasetFactory.inspect()); Scanner scanner = dataset.newScan(
        options); ArrowReader arrowReader = scanner.scanBatches()) {

      Schema schema = datasetFactory.inspect();
      System.out.println(schema);

      int batch = 0;
      while (arrowReader.loadNextBatch()) {
        batch++;
        try (VectorSchemaRoot root = arrowReader.getVectorSchemaRoot()) {
          FieldVector fieldVector = root.getVector("Age");
          System.out.println(fieldVector);

          BigIntVector linearSearchVector = (BigIntVector) fieldVector;

          String o1 = String.format("linearSearchVector.get(2): %d", linearSearchVector.get(2));

          System.out.println(o1);
          VectorValueComparator<BigIntVector> comparatorInt = DefaultVectorComparators.createDefaultComparator(
              linearSearchVector);
          int result = VectorSearcher.linearSearch(linearSearchVector, comparatorInt,
              linearSearchVector, 2);

          System.out.println("result");
          System.out.println(result);

//    https://arrow.apache.org/docs/java/index.html
          fieldVector.getTransferPair(allocator);
          System.out.print(root.contentToTSVString());
          String summary = String.format("Batch: %d, rowCount: %d", batch,
              root.getRowCount());//returns 12 char fractional part filling with 0
          System.out.println(summary);

        }
      }

//      System.out.println(StreamSupport.stream(scanner.scan().spliterator(), false).count());
    } catch (Exception e) {
      e.printStackTrace();
    }


  }


  static void basicExample() {
    try (RootAllocator root = new RootAllocator(8 * 1024)) {
      BufferAllocator child = root.newChildAllocator("c1", 100, 4 * 1024);
      ArrowBuf arrowBuf = child.buffer(4 * 1024);
      System.out.println(arrowBuf);
      arrowBuf.close();
      child.close();
    }
  }


  static void constructVectorInMemory() {
    try (RootAllocator allocator = new RootAllocator(Long.MAX_VALUE)) {
//      AllocationListener allocationListener = new AllocationListener() {
//      }

      // Define the schema for our vector
      Field age = Field.nullable("Age", Types.MinorType.BIGINT.getType());
      Field my_bool = Field.nullable("my_bool", Types.MinorType.BIT.getType());
      Field name = new Field("name", FieldType.nullable(new ArrowType.Utf8()), null);

//       Create a schema and vector root from the field
      Schema schema = new Schema(asList(age, name, my_bool));
      VectorSchemaRoot root = VectorSchemaRoot.create(schema, allocator);

      root.allocateNew();

      // Get the bit vector from the vector root
      BitVector bitVector = (BitVector) root.getVector("my_bool");

      // Create a null mask and set one value as null
      bitVector.setSafe(0, 0);  // set the first value to null
      // Set the remaining values of the vector
      bitVector.set(1, 1);  // set the second value to true
      bitVector.set(2, 0);  // set the third value to false

      BigIntVector bigIntVector = (BigIntVector) root.getVector("Age");
      bigIntVector.set(0, 0);  // set the first value to 0
      // Set the remaining values of the vector
      bigIntVector.set(1, 1);  // set the second value to 1
      bigIntVector.set(2, 0);  // set the third value to 0
//       Set the record count of the vector root
      root.setRowCount(3);

      // Do something with the vector data...
      System.out.println(root);
      System.out.print(root.contentToTSVString());

//       Clean up resources
      root.close();//unsafe
      try {
        AutoCloseables.close(root);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }


  static void  writeToParquet(){
    try (BufferAllocator allocator = new RootAllocator()) {
      Field name = new Field("name", FieldType.nullable(new ArrowType.Utf8()), null);
      Field age = new Field("age", FieldType.nullable(new ArrowType.Int(32, true)), null);
      Schema schemaPerson = new Schema(asList(name, age));
      try(
          VectorSchemaRoot vectorSchemaRoot = VectorSchemaRoot.create(schemaPerson, allocator)
      ){
        VarCharVector nameVector = (VarCharVector) vectorSchemaRoot.getVector("name");
        nameVector.allocateNew(3);
        nameVector.set(0, "David".getBytes());
        nameVector.set(1, "Gladis".getBytes());
        nameVector.set(2, "Juan".getBytes());
        IntVector ageVector = (IntVector) vectorSchemaRoot.getVector("age");
        ageVector.allocateNew(3);
        ageVector.set(0, 10);
        ageVector.set(1, 20);
        ageVector.set(2, 30);
        vectorSchemaRoot.setRowCount(3);
        File file = new File("randon_access_to_file.arrow");
//        File file = new File("randon_access_to_file.parquet");
        try (
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ArrowFileWriter writer = new ArrowFileWriter(vectorSchemaRoot, null, fileOutputStream.getChannel())) {
          writer.start();
          writer.writeBatch();
          writer.end();
          System.out.println("Record batches written: " + writer.getRecordBlocks().size() + ". Number of rows written: " + vectorSchemaRoot.getRowCount());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
  public static void main(String[] args) {

//    basicExample();
//    readFromParquetFile();
//    constructVectorInMemory();

//    can writing to parquet create leak?
    writeToParquet();

  }
}


