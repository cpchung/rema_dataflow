package arrowDemo;

import java.util.stream.StreamSupport;
import org.apache.arrow.algorithm.search.VectorSearcher;
import org.apache.arrow.algorithm.sort.DefaultVectorComparators;
import org.apache.arrow.algorithm.sort.VectorValueComparator;
import org.apache.arrow.dataset.file.FileFormat;
import org.apache.arrow.dataset.file.FileSystemDatasetFactory;
import org.apache.arrow.dataset.jni.NativeMemoryPool;
import org.apache.arrow.dataset.scanner.ScanOptions;
import org.apache.arrow.dataset.scanner.Scanner;
import org.apache.arrow.dataset.source.Dataset;
import org.apache.arrow.dataset.source.DatasetFactory;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.vector.IntVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.ipc.ArrowReader;
public class Main {

  public static void main(String[] args) {
    /// home/cpchung/code/python/PatternMatch/data/social-network-sf0
    // .003-bi-composite-merged-fk/graphs/parquet/raw/composite-merged-fk/static/Place/part_0_0.snappy.parquet
    System.out.println(System.getProperty("user.dir"));
    String filePath =
        "file:/home/cpchung/pokeai/graph/data/social-network-sf0.003-bi-parquet/graphs/parquet/bi/composite-merged-fk/initial_snapshot/static/Organisation/part-00000-9353b37a-a560-4975-8bb4-f8475a470d82-c000.snappy.parquet";
//        "file:/home/cpchung/pokeai/graph/data/social-network-sf0.003-bi-parquet/graphs/parquet/bi/composite-merged-fk/";
    //        String uri = "file:" + System.getProperty("user.dir") +
    // "/thirdpartydeps/parquetfiles/data1.parquet";
//    String uri = "file:" + System.getProperty("user.dir") + "/../.." + filePath;
//    String uri = "file:" + filePath;
    String uri = filePath;
    ScanOptions options = new ScanOptions(/*batchSize*/ 1200000);
    try (
        BufferAllocator allocator = new RootAllocator();
        DatasetFactory datasetFactory = new FileSystemDatasetFactory(allocator, NativeMemoryPool.getDefault(), FileFormat.PARQUET, uri);
//        Dataset dataset = datasetFactory.finish();

        Dataset dataset = datasetFactory.finish(datasetFactory.inspect());
        Scanner scanner = dataset.newScan(options);
        ArrowReader reader = scanner.scanBatches()

    ) {

//      Schema schema = datasetFactory.inspect();
//      System.out.println(schema);
      int count = 1;

      while (reader.loadNextBatch()) {
        try (VectorSchemaRoot root = reader.getVectorSchemaRoot()) {
          System.out.println(root.getVector("LocationPlaceId"));


          IntVector linearSearchVector = (IntVector) root.getVector("LocationPlaceId");

          System.out.println(linearSearchVector.get(10));
          VectorValueComparator<IntVector> comparatorInt = DefaultVectorComparators.createDefaultComparator(linearSearchVector);
          int result = VectorSearcher.linearSearch(linearSearchVector, comparatorInt, linearSearchVector, 59);


          System.out.println(result);
//    https://arrow.apache.org/docs/java/index.html
//          root.getVector("LocationPlaceId").getTransferPair()

//          System.out.print(root.contentToTSVString());

//          Schema schema= root.getSchema();
//          System.out.println(schema);

//          System.out.println("Batch: " + count++ + ", RowCount: " + root.getRowCount());

        }
      }

      System.out.println(StreamSupport.stream(scanner.scan().spliterator(), false).count());
    } catch (Exception e) {
      e.printStackTrace();
    }


//    try(BufferAllocator bufferAllocator = new RootAllocator(8 * 1024)){
//      ArrowBuf arrowBuf = bufferAllocator.buffer(4 * 1024);
//      System.out.println(arrowBuf);
//      arrowBuf.close();
//    }
  }
}
