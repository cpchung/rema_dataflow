/* (C)2023 */
package arrowDemo;

import java.util.Arrays;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.vector.BitVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.types.Types;
import org.apache.arrow.vector.types.pojo.ArrowType;
import org.apache.arrow.vector.types.pojo.Field;
import org.apache.arrow.vector.types.pojo.FieldType;
import org.apache.arrow.vector.types.pojo.Schema;

public class ArrowExample {

  public static void main(String[] args) {
    try (RootAllocator allocator = new RootAllocator(Long.MAX_VALUE)) {
//      AllocationListener allocationListener = new AllocationListener() {
//      }
       BufferAllocator bufferAllocator= allocator.newChildAllocator("c1",100,1000);

      // Define the schema for our vector
      Field field = Field.nullable("my_bool", Types.MinorType.BIT.getType());

//       Create a schema and vector root from the field
      Schema schema =
          new Schema(
              Arrays.asList(new Field("name", FieldType.nullable(new ArrowType.Utf8()), null)));
      VectorSchemaRoot root = VectorSchemaRoot.create(schema, allocator);

      // Get the bit vector from the vector root
      BitVector bitVector = (BitVector) root.getVector("my_bool");

      // Create a null mask and set one value as null
//      NullableBitVector.Mutator mutator = (NullableBitVector.Mutator)
//          bitVector.getMutator();
//      bitVector.set(0, 1);
//      bitVector.set(1, 1);
//      bitVector.set(2, 1);
//      NullableBitVector nulls = mutator.getValidityBuffer();
//      nulls.set(0, 0);  // set the first value to null
//
//      // Set the remaining values of the vector
//      mutator.set(1, 1);  // set the second value to true
//      mutator.set(2, 0);  // set the third value to false

//       Set the record count of the vector root
      root.setRowCount(3);

      // Do something with the vector data...
      System.out.println(root);

//       Clean up resources
      root.close();
    }
  }
}
