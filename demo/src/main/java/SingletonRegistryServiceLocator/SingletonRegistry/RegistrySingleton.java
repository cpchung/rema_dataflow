/* (C)2023 */
package SingletonRegistryServiceLocator.SingletonRegistry;

import java.util.HashMap;
import java.util.Map;

public class RegistrySingleton {

  private static Map<String, RegistrySingleton> registry = new HashMap<String, RegistrySingleton>();
  private String moduleName = null;

  /**
   * Make sure to have private default constructor. This avoids direct instantiation of class using
   * new keyword/Class.newInstance() method
   */
  private RegistrySingleton() {}

  public static synchronized RegistrySingleton getInstance(String moduleName) {

    RegistrySingleton instance = null;

    if (registry.containsKey(moduleName)) {
      instance = registry.get(moduleName);
    } else {
      instance = new RegistrySingleton();
      instance.moduleName = moduleName;
      registry.put(moduleName, instance);
    }

    return instance;
  }

  public String getModuleName() {
    return moduleName;
  }

  public String toString() {
    return "RegistrySingletonn : " + getModuleName();
  }
}
