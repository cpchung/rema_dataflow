/* (C)2023 */
package SingletonRegistryServiceLocator.SingletonRegistry;

public class Main {

  public static void main(String[] args) {

    System.out.println(RegistrySingleton.getInstance("UI"));
    System.out.println(RegistrySingleton.getInstance("Server"));
    System.out.println(RegistrySingleton.getInstance("UI"));
    System.out.println(RegistrySingleton.getInstance("Admin"));
    System.out.println(RegistrySingleton.getInstance("Server"));
  }
}
