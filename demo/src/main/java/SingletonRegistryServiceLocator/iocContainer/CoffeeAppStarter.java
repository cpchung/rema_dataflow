/* (C)2023 */
package SingletonRegistryServiceLocator.iocContainer;

import java.util.HashMap;
import java.util.Map;

class CoffeeMachine {

  public Coffee brewFilterCoffee() {
    return null;
  }
}

class PremiumCoffeeMachine extends CoffeeMachine {

  public PremiumCoffeeMachine(Map<CoffeeSelection, CoffeeBean> m) {}
}

class CoffeeBean {

  public CoffeeBean(String a, int b) {}

  String a;
  int b;
}

class Coffee {}

enum CoffeeSelection {
  ESPRESSO,
  FILTER_COFFEE
}

class CoffeeException extends RuntimeException {}

public class CoffeeAppStarter {

  public static void main(String[] args) {
    // create a Map of available coffee beans
    Map<CoffeeSelection, CoffeeBean> beans = new HashMap<CoffeeSelection, CoffeeBean>();
    beans.put(CoffeeSelection.ESPRESSO, new CoffeeBean("My favorite espresso bean", 1000));
    beans.put(
        CoffeeSelection.FILTER_COFFEE, new CoffeeBean("My favorite filter coffee bean", 1000));

    // get a new CoffeeMachine object
    PremiumCoffeeMachine machine = new PremiumCoffeeMachine(beans);

    // Instantiate CoffeeApp
    CoffeeApp app = new CoffeeApp(machine);

    // brew a fresh coffee
    try {
      app.prepareCoffee(CoffeeSelection.ESPRESSO);
    } catch (CoffeeException e) {
      e.printStackTrace();
    }
  }
}
