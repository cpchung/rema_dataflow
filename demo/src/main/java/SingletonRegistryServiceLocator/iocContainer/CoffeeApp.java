/* (C)2023 */
package SingletonRegistryServiceLocator.iocContainer;

class CoffeeApp {

  private CoffeeMachine coffeeMachine;

  public CoffeeApp(CoffeeMachine coffeeMachine) {
    this.coffeeMachine = coffeeMachine;
  }

  //  public Coffee prepareCoffee(CoffeeSelection selection) {
  //    Coffee coffee = this.coffeeMachine.brewFilterCoffee();
  //    System.out.println("Coffee is ready!");
  //    return coffee;
  //  }

  public Coffee prepareCoffee(CoffeeSelection selection) throws CoffeeException {
    CoffeeMachine coffeeMachine = CoffeeServiceLocator.getInstance().coffeeMachine();
    Coffee coffee = coffeeMachine.brewFilterCoffee();
    System.out.println("Coffee is ready!");
    return coffee;
  }
}
