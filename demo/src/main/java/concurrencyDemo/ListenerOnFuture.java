package concurrencyDemo;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ListenerOnFuture {

  // new api
  public ListenableFutureTask<String> fetchConfigListenableTask(String configKey) {
    return ListenableFutureTask.create(() -> {
      TimeUnit.MILLISECONDS.sleep(500);
      return String.format("%s.%d", configKey, new Random().nextInt(Integer.MAX_VALUE));
    });
  }

  public static void main(String[] args){
    ExecutorService service = Executors.newSingleThreadExecutor();
    ListeningExecutorService lExecService = MoreExecutors.listeningDecorator(service);

    ListenableFuture<Integer> asyncTask = lExecService.submit(() -> {
      TimeUnit.MILLISECONDS.sleep(500); // long running task
      return 5;
    });




  }

}
