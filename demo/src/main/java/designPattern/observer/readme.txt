Observer design pattern is also called as publish-subscribe pattern.
https://www.digitalocean.com/community/tutorials/observer-design-pattern-in-java

https://refactoring.guru/design-patterns/observer/java/example

https://howtodoinjava.com/design-patterns/behavioral/observer-design-pattern/


key ideas:
different implementation
1.
register listener and observer to monitor state of an object

object update states

object notify state update

2.
register listener or observer to event source

on event published, notify state update (we can introduce cyle here)

3.







