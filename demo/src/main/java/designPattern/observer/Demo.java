package designPattern.observer;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

public class Demo {

  public static void main(String[] args) {
    PCLNewsAgency pclNewsAgency = new PCLNewsAgency();
    PCLNewsChannel pclNewsChannel = new PCLNewsChannel();
    pclNewsAgency.addPropertyChangeListener(pclNewsChannel);


    pclNewsAgency.setNews("111");


    ONewsAgency oNewsAgency = new ONewsAgency();
    ONewsChannel oNewsChannel= new ONewsChannel();
    oNewsAgency.addObserver(oNewsChannel);

    oNewsAgency.setNews("add news");


    Object referent = new Object();
    ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();

    WeakReference weakReference1 = new WeakReference<>(referent);
    WeakReference weakReference2 = new WeakReference<>(referent, referenceQueue);

  }

}
