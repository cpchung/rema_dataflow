package designPattern.example;

public class MyListenerImpl implements MyListener {

  private String msg;
  private String data;

  public MyListenerImpl(String data) {
    this.data = data;
  }

  @Override
  public void onEvent() {
    System.out.println(msg);
  }
}