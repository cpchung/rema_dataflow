package designPattern.example;

import java.util.Collection;
import java.util.HashSet;

public class ToBeListened {

  private Collection<MyListener> listeners = new HashSet<>();

  public void register(MyListener listener) {
    listeners.add(listener);
  }

  public void eventHappens() {
    for (MyListener listener : listeners) {
      listener.onEvent();
    }
  }


}
