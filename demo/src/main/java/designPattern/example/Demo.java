package designPattern.example;

public class Demo {
  public static void main(String[] args) {
    ToBeListened obj = new ToBeListened();
    obj.register(new MyListenerImpl("listener 1 called"));
    obj.register(new MyListenerImpl("listener 2 called"));
    System.out.println("calling event");
    obj.eventHappens();

  }
}
