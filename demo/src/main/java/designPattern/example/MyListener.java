package designPattern.example;

import java.util.EventListener;

public interface MyListener extends EventListener {
  void onEvent();
}