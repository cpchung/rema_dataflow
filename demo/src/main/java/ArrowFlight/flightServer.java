/* (C)2023 */
package ArrowFlight;

import java.io.IOException;
import org.apache.arrow.flight.FlightServer;
import org.apache.arrow.flight.Location;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;

public class flightServer {

  public static void main(String[] args) {
    Location location = Location.forGrpcInsecure("0.0.0.0", 33333);
    BufferAllocator allocator = new RootAllocator();

    final CookbookProducer producer = new CookbookProducer(allocator, location);
    final FlightServer flightServer = FlightServer.builder(allocator, location, producer).build();

    // Server
    try {
      flightServer.start();
      System.out.println("S1: Server (Location): Listening on port " + flightServer.getPort());

      System.out.println("Server started!");
      flightServer.awaitTermination();

    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    // Server shut down
    //    flightServer.shutdown();
    //    System.out.println("C8: Server shut down successfully");

  }
}
