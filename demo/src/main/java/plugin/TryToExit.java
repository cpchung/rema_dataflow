/* (C)2023 */
package plugin;

/** This plugin tries to call System.exit(), which the SecurityManager doesn't allow. */
public class TryToExit implements PluginFunction {

  public void setParameter(int param) {
    // this function doesn't care about its parameter,
    // so it doesn't even store it for later use
  }

  public int getResult() {
    // The next line will be caught by the SecurityManagers 'checkExit' method.
    System.exit(0);
    return 42;
  }

  public String getPluginName() {
    return "plugin.TryToExit";
  }

  // yes, this operation can fail, but we are going to ignore this here
  public boolean hasError() {
    return false;
  }
}
