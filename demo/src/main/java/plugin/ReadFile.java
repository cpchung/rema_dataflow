/* (C)2023 */
package plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * This plugin read two numbers from a file and adds them. For this to work, the SecurityManager
 * needs to allow file read access in 'checkRead' and access to the system property 'user.dir' in
 * 'checkPropertyAccess'.
 */
public class ReadFile implements PluginFunction {

  static String filename = "plugins" + File.separator + "input.txt";

  boolean hasError = false;

  public void setParameter(int param) {
    // we're ignoring any parameters
  }

  public int getResult() {
    String line1 = null, line2 = null;
    hasError = false;
    try {
      BufferedReader br = new BufferedReader(new FileReader(filename));
      if (br.ready()) line1 = br.readLine();
      if (br.ready()) line2 = br.readLine();
      br.close();
      return Integer.valueOf(line1).intValue() + Integer.valueOf(line2).intValue();
    } catch (IOException ioex) {
      hasError = true;
    }
    return 0;
  }

  public String getPluginName() {
    return "plugin.ReadFile";
  }

  public boolean hasError() {
    return hasError;
  }
}
