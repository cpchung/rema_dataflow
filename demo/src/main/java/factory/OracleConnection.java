/* (C)2023 */
package factory;

class OracleConnection implements IConnection {

  @Override
  public String getVendorName() {
    return "Oracle 11g";
  }
}
