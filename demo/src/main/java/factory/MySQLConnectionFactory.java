/* (C)2023 */
package factory;

class MySQLConnectionFactory implements IConnectionFactory {

  @Override
  public IConnection getConnection() {
    return new MySQLConnection();
  }
}
