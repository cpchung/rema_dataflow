/* (C)2023 */
package factory;

interface IConnection {

  /**
   * Gets the connection DB vendor name.
   *
   * @return
   */
  public String getVendorName();
}
