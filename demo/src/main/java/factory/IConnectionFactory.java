/* (C)2023 */
package factory;

interface IConnectionFactory {

  /**
   * Get the connection associated with this DB factory.
   *
   * @return
   */
  public IConnection getConnection();
}
