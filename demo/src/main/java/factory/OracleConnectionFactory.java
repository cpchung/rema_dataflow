/* (C)2023 */
package factory;

class OracleConnectionFactory implements IConnectionFactory {

  @Override
  public IConnection getConnection() {
    return new OracleConnection();
  }
}
