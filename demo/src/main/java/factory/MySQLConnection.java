/* (C)2023 */
package factory;

class MySQLConnection implements IConnection {

  @Override
  public String getVendorName() {
    return "MySQL 6.0";
  }
}
