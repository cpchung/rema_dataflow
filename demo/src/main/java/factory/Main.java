/* (C)2023 */
package factory;

public class Main {
  /**
   * @param args
   */
  public static void main(String[] args) {

    /** Get Connection from OracleConnectionFactory */
    System.setProperty("DB_CONNECTION_FACTORY", "factory.OracleConnectionFactory");

    IConnectionFactory factory = DBConnectionFactory.getConnectionFactory();
    IConnection connection = factory.getConnection();
    System.out.println("Obtained connection for DB Vendor : " + connection.getVendorName());

    /** Get Connection from MySQLConnectionFactory */
    System.setProperty("DB_CONNECTION_FACTORY", "factory.MySQLConnectionFactory");

    factory = DBConnectionFactory.getConnectionFactory();
    connection = factory.getConnection();
    System.out.println("Obtained connection for DB Vendor : " + connection.getVendorName());
  }
}
