package arrowResultCache;

/* (C)2023 */

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.arrow.flight.Action;
import org.apache.arrow.flight.CallStatus;
import org.apache.arrow.flight.Criteria;
import org.apache.arrow.flight.FlightDescriptor;
import org.apache.arrow.flight.FlightEndpoint;
import org.apache.arrow.flight.FlightInfo;
import org.apache.arrow.flight.FlightStream;
import org.apache.arrow.flight.Location;
import org.apache.arrow.flight.NoOpFlightProducer;
import org.apache.arrow.flight.PutResult;
import org.apache.arrow.flight.Result;
import org.apache.arrow.flight.Ticket;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.util.AutoCloseables;
import org.apache.arrow.vector.VectorLoader;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.VectorUnloader;
import org.apache.arrow.vector.ipc.message.ArrowRecordBatch;

public class ResultCacheProducer extends NoOpFlightProducer implements AutoCloseable {

  private final BufferAllocator allocator;
  private final Location location;
  private final ConcurrentMap<FlightDescriptor, ResultCacheDataset> datasets;

  public ResultCacheProducer(BufferAllocator allocator, Location location) {
    this.allocator = allocator;
    this.location = location;
    this.datasets = new ConcurrentHashMap<>();
  }

  @Override
  public Runnable acceptPut(
      CallContext context, FlightStream flightStream, StreamListener<PutResult> ackStream) {
    List<ArrowRecordBatch> batches = new ArrayList<>();
    return () -> {
      long rows = 0;
      VectorUnloader unloader;
      while (flightStream.next()) {
        unloader = new VectorUnloader(flightStream.getRoot());
        final ArrowRecordBatch arb = unloader.getRecordBatch();
        batches.add(arb);
        rows += flightStream.getRoot().getRowCount();
      }
      ResultCacheDataset resultCacheDataset = new ResultCacheDataset(batches,
          flightStream.getSchema(), rows);
      datasets.put(flightStream.getDescriptor(), resultCacheDataset);
      ackStream.onCompleted();
    };
  }

  @Override
  public void getStream(CallContext context, Ticket ticket, ServerStreamListener listener) {
    FlightDescriptor flightDescriptor =
        FlightDescriptor.path(new String(ticket.getBytes(), StandardCharsets.UTF_8));
    ResultCacheDataset resultCacheDataset = this.datasets.get(flightDescriptor);
    if (resultCacheDataset == null) {
      throw CallStatus.NOT_FOUND.withDescription("Unknown descriptor").toRuntimeException();
    }
    try (VectorSchemaRoot root =
        VectorSchemaRoot.create(this.datasets.get(flightDescriptor).getSchema(), allocator)) {
      VectorLoader loader = new VectorLoader(root);
      listener.start(root);
      for (ArrowRecordBatch arrowRecordBatch : this.datasets.get(flightDescriptor).getBatches()) {
        loader.load(arrowRecordBatch);
        listener.putNext();
      }
      listener.completed();
    }
  }

  @Override
  public void doAction(CallContext context, Action action, StreamListener<Result> listener) {
    FlightDescriptor flightDescriptor =
        FlightDescriptor.path(new String(action.getBody(), StandardCharsets.UTF_8));
    switch (action.getType()) {
      case "DELETE": {
        ResultCacheDataset removed = datasets.remove(flightDescriptor);
        if (removed != null) {
          try {
            removed.close();
          } catch (Exception e) {
            listener.onError(
                CallStatus.INTERNAL.withDescription(e.toString()).toRuntimeException());
            return;
          }
          Result result = new Result("Delete completed".getBytes(StandardCharsets.UTF_8));
          listener.onNext(result);
        } else {
          Result result =
              new Result(
                  "Delete not completed. Reason: Key did not exist."
                      .getBytes(StandardCharsets.UTF_8));
          listener.onNext(result);
        }
        listener.onCompleted();
      }
    }
  }

  @Override
  public FlightInfo getFlightInfo(CallContext context, FlightDescriptor descriptor) {
    FlightEndpoint flightEndpoint =
        new FlightEndpoint(
            new Ticket(descriptor.getPath().get(0).getBytes(StandardCharsets.UTF_8)), location);
    return new FlightInfo(
        datasets.get(descriptor).getSchema(),
        descriptor,
        Collections.singletonList(flightEndpoint),
        /*bytes=*/ -1,
        datasets.get(descriptor).getRows());
  }

  @Override
  public void listFlights(
      CallContext context, Criteria criteria, StreamListener<FlightInfo> listener) {
    datasets.forEach(
        (k, v) -> {
          listener.onNext(getFlightInfo(null, k));
        });
    listener.onCompleted();
  }

  @Override
  public void close() throws Exception {
    AutoCloseables.close(datasets.values());
  }
}
