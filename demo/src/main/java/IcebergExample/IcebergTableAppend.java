/* (C)2023 */
package IcebergExample;

import static org.apache.iceberg.Files.localInput;
import static org.apache.iceberg.types.Types.NestedField.optional;
import static org.apache.iceberg.types.Types.NestedField.required;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.iceberg.CatalogProperties;
import org.apache.iceberg.CombinedScanTask;
import org.apache.iceberg.DataFile;
import org.apache.iceberg.DataFiles;
import org.apache.iceberg.FileFormat;
import org.apache.iceberg.Files;
import org.apache.iceberg.PartitionKey;
import org.apache.iceberg.PartitionSpec;
import org.apache.iceberg.Schema;
import org.apache.iceberg.Table;
import org.apache.iceberg.TableScan;
import org.apache.iceberg.Transaction;
import org.apache.iceberg.catalog.Catalog;
import org.apache.iceberg.catalog.Namespace;
import org.apache.iceberg.catalog.TableIdentifier;
import org.apache.iceberg.data.GenericRecord;
import org.apache.iceberg.data.parquet.GenericParquetWriter;
import org.apache.iceberg.expressions.Expressions;
import org.apache.iceberg.expressions.UnboundPredicate;
import org.apache.iceberg.hadoop.HadoopCatalog;
import org.apache.iceberg.io.FileAppender;
import org.apache.iceberg.parquet.Parquet;
import org.apache.iceberg.rest.RESTCatalog;
import org.apache.iceberg.types.Types;

public class IcebergTableAppend {

  static String lakehouse = "/tmp/iceberg-test";
  static int NUM_MONTH = 12;
  static int NUM_ROWS_PER_MONTH = 2;
  Configuration conf;

  String col1 = "hotel_id";
  String col2 = "hotel_name";
  Schema schema;

  PartitionSpec spec;

  public IcebergTableAppend() {
    try {
      FileUtils.deleteDirectory(new File(lakehouse));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    conf = new Configuration();
    conf.set(CatalogProperties.WAREHOUSE_LOCATION, lakehouse);

    schema =
        new Schema(
            required(1, col1, Types.LongType.get())
//          ,
//          required(2, col2, Types.StringType.get()),
//          required(3, "customer_id", Types.LongType.get()),
//          required(4, "arrival_date", Types.DateType.get()),
//          required(5, "departure_date", Types.DateType.get()),
//          required(6, "value", Types.DoubleType.get())

        );

//    spec = PartitionSpec.builderFor(schema).month("arrival_date").build();
    spec = PartitionSpec.builderFor(schema).identity(col1).build();
  }

  /*
   * add a new column
   * add a new row
   *
   * update
   *
   * delete
   *
   *
   * */


  void scanning() {
    TableIdentifier id = TableIdentifier.parse("bookings.rome_hotels");
    String warehousePath = "file://" + lakehouse;
    Catalog catalog = new HadoopCatalog(conf, warehousePath);
    Table table = catalog.createTable(id, schema, spec);

    TableScan scan = table.newScan()
        .filter(Expressions.equal("id", 5))
        .select("id", "data");

    Schema projection = scan.schema();
    Iterable<CombinedScanTask> tasks = scan.planTasks();


  }

  public void write() {

    TableIdentifier id = TableIdentifier.parse("bookings.rome_hotels");
    String warehousePath = "file://" + lakehouse;
    Catalog catalog = new HadoopCatalog(conf, warehousePath);
    Table table = catalog.createTable(id, schema, spec);
    //    List<GenericRecord> records = Lists.newArrayList();
    List<GenericRecord> records = new ArrayList<>();
    // generating a bunch of records

//    for (int j = 1; j <= NUM_MONTH; j++) {
//      for (int i = 0; i < NUM_ROWS_PER_MONTH; i++) {
//        GenericRecord rec = GenericRecord.create(schema);
////        rec.setField("hotel_id", (long) (i));
//        rec.setField(col1, col1);
////        rec.setField("customer_id", (long) (i));
////        rec.setField("arrival_date", LocalDate.of(2022, j, (i % 23) + 1).plus(1, ChronoUnit.DAYS));
////        rec.setField("departure_date", LocalDate.of(2022, j, (i % 23) + 5));
////        rec.setField("value", (double) i);
//        records.add(rec);
//      }
//    }

    GenericRecord rec = GenericRecord.create(schema);
    rec.setField(col1, 5L);
    records.add(rec);

    File parquetFile = new File(lakehouse + "/bookings/rome_hotels/arq_001.parquet");
    FileAppender<GenericRecord> appender;
    try {
      appender =
          Parquet.write(Files.localOutput(parquetFile))
              .schema(table.schema())
              .createWriterFunc(GenericParquetWriter::buildWriter)
              .build();

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    try {
      appender.addAll(records);
    } finally {
      try {
        appender.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    PartitionKey partitionKey = new PartitionKey(table.spec(), table.schema());
    DataFile dataFile =
        DataFiles.builder(table.spec())
            .withPartition(partitionKey)
            .withInputFile(localInput(parquetFile))
            //          .withMetrics(appender.metrics())
            .withFormat(FileFormat.PARQUET)
            .withRecordCount(NUM_MONTH * NUM_ROWS_PER_MONTH)
            .build();

    Transaction t = table.newTransaction();
    t.newAppend().appendFile(dataFile).commit();
    // commit all changes to the table
    t.commitTransaction();
  }

  // read entire column by column name

  //    read entire row by row id

  //    read selected part of a column by a filter

  //    read selected rows by a filter

  public void read() {

    TableIdentifier id = TableIdentifier.parse("bookings.rome_hotels");
    String warehousePath = "file://" + lakehouse; // we dont need the file:// part
    Catalog catalog = new HadoopCatalog(conf, warehousePath);

    Table table = catalog.loadTable(id);

    UnboundPredicate<Integer> expressions = Expressions.equal(col1, 5);
//    UnboundPredicate<String> expressions1 = Expressions.equal(col2, col2);

    TableScan scan = table.newScan().filter(expressions).select(col1, col2);
    //        TableScan scan = table.newScan().filter(expressions1).select(col1, col2);

    //        Schema projection = scan.schema().;
    Iterable<CombinedScanTask> tasks = scan.planTasks();

    System.out.println("!");
    //    Update operations
    table.updateSchema().addColumn("count", Types.LongType.get()).commit(); // ??? no effect??

    //    Transactions
    Transaction t = table.newTransaction();

    // commit operations to the transaction
    //        t.newDelete().deleteFromRowFilter(expressions).commit(); //Cannot delete file
    // where
    // some, but not all, rows match filter ref(name="hotel_id") == 5
    t.newDelete()
        .deleteFromRowFilter(expressions)
        .commit(); // Cannot delete file where some, but not all, rows match filter
    // ref(name="hotel_id") == 5
    //            t.newAppend().appendFile(data).commit();

    // commit all the changes to the table
    t.commitTransaction();

    System.out.println("!");
  }

  static void test() {

    Map<String, String> properties = new HashMap<>();
    properties.put(CatalogProperties.CATALOG_IMPL, "org.apache.iceberg.rest.RESTCatalog");
    properties.put(CatalogProperties.URI, "http://rest:8181");
    properties.put(CatalogProperties.WAREHOUSE_LOCATION, "s3a://warehouse/wh");
    properties.put(CatalogProperties.FILE_IO_IMPL, "org.apache.iceberg.aws.s3.S3FileIO");
    //     properties.put(AwsProperties.S3FILEIO_ENDPOINT, "http://minio:9000");

    RESTCatalog catalog = new RESTCatalog();
    Configuration conf = new Configuration();
    catalog.setConf(conf);
    catalog.initialize("demo", properties);

    Schema schema =
        new Schema(
            required(1, "level", Types.StringType.get()),
            required(2, "event_time", Types.TimestampType.withZone()),
            required(3, "message", Types.StringType.get()),
            optional(
                4, "call_stack", Types.ListType.ofRequired(5, Types.StringType.get())));

    PartitionSpec spec = PartitionSpec.builderFor(schema).hour("event_time").build();

    Namespace namespace = Namespace.of("webapp");
    TableIdentifier name = TableIdentifier.of(namespace, "logs");

    catalog.createTable(name, schema, spec);

    List<TableIdentifier> tables = catalog.listTables(namespace);
    System.out.println(tables);

    catalog.dropTable(name);
  }

  public static void main(String[] args) {
    //    test();

    IcebergTableAppend icebergTableAppend = new IcebergTableAppend();
    icebergTableAppend.write();
    icebergTableAppend.read();
  }
}
