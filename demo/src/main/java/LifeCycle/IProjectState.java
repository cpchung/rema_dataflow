/* (C)2023 */
package LifeCycle;

interface IProjectState {

  /**
   * Move the state of project life cycle to next state.
   *
   * @param projectLifeCycle
   */
  public void nextPhase(ProjectLifeCycle projectLifeCycle);
}
