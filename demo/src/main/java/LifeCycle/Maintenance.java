/* (C)2023 */
package LifeCycle;

class Maintenance implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project maintenance is in progress ...");
    projectLifeCycle.setCurProjectState(new Maintenance());
  }
}
