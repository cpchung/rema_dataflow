/* (C)2023 */
package LifeCycle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test {

  /**
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {

    BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
    String line;

    ProjectLifeCycle projectLifeCycle = new ProjectLifeCycle();
    System.out.println("----Project Life Cycle Management----");

    while (true) {
      System.out.println("Current Phase : " + projectLifeCycle.getCurrentPhase());

      System.out.println("enter command:");
      line = console.readLine();
      if ("exit".equalsIgnoreCase(line.trim())) {
        break;
      }

      projectLifeCycle.nextPhase();
    }
  }
}
