/* (C)2023 */
package LifeCycle;

class Implementation implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project implementation has been completed");
    projectLifeCycle.setCurProjectState(new Testing());
  }
}
