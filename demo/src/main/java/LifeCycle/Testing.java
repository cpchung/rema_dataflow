/* (C)2023 */
package LifeCycle;

class Testing implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project testing has been completed");
    projectLifeCycle.setCurProjectState(new Deployment());
  }
}
