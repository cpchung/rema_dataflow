/* (C)2023 */
package LifeCycle;

class Design implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project design has been completed");
    projectLifeCycle.setCurProjectState(new Implementation());
  }
}
