/* (C)2023 */
package LifeCycle;

class Deployment implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project deployment has been completed");
    projectLifeCycle.setCurProjectState(new Maintenance());
  }
}
