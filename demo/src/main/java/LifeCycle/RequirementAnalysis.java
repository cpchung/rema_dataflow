/* (C)2023 */
package LifeCycle;

class RequirementAnalysis implements IProjectState {

  @Override
  public void nextPhase(ProjectLifeCycle projectLifeCycle) {
    System.out.println("Project requirement analysis has been completed");
    projectLifeCycle.setCurProjectState(new Design());
  }
}
