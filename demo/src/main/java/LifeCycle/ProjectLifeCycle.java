/* (C)2023 */
package LifeCycle;

class ProjectLifeCycle {

  private IProjectState curProjectState = null;

  public ProjectLifeCycle() {
    curProjectState = new RequirementAnalysis();
  }

  public IProjectState getCurProjectState() {
    return curProjectState;
  }

  public void setCurProjectState(IProjectState curProjectState) {
    this.curProjectState = curProjectState;
  }

  public void nextPhase() {
    curProjectState.nextPhase(this);
  }

  public String getCurrentPhase() {
    return curProjectState.getClass().getSimpleName();
  }
}
