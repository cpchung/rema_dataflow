#https://kubernetes.io/docs/tasks/tools/install-minikube/
#https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux
#minikube start --kubernetes-version v1.12.0 --cpus 4 --memory 8192

#eval $(minikube docker-env)
docker build . -t cpchung/rema

#kubectl create deployment rema --image=cpchung/rema --dry-run=client -o=yaml > deployment.yaml
#echo --- >> deployment.yaml
#kubectl create service clusterip rema --tcp=8080:8080 --dry-run=client -o=yaml >> deployment.yaml
#imagePullPolicy:Never

kubectl delete pod,service rema
kubectl delete deployment rema
kubectl get all
#kubectl apply -f deployment.yaml

#kubectl port-forward svc/rema 8080:8080


#debugging:
#kubectl describe pod/rema-5cd47fb54c-qvqm6
