/* (C)2023 */
package jetty.websocketExample.client;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestApp {

  public void test() {
    try {
      // open websocket
      //      final WebsocketClientEndpoint clientEndPoint = new WebsocketClientEndpoint(new
      // URI("wss://real.okcoin.cn:10440/websocket/okcoinapi"));
      final WebsocketClientEndpoint clientEndPoint =
          new WebsocketClientEndpoint(new URI("wss://localhost:9000"));

      // add listener
      clientEndPoint.addMessageHandler(
          new WebsocketClientEndpoint.MessageHandler() {
            public void handleMessage(String message) {
              System.out.println(message);
            }
          });

      // send message to websocket
      clientEndPoint.sendMessage("{'event':'addChannel','channel':'ok_btccny_ticker'}");

      // wait 5 seconds for messages from websocket
      Thread.sleep(5000);

    } catch (InterruptedException ex) {
      System.err.println("InterruptedException exception: " + ex.getMessage());
    } catch (URISyntaxException ex) {
      System.err.println("URISyntaxException exception: " + ex.getMessage());
    }
  }

  public static void tryWebSocket() throws Exception {
    final String url = "ws://localhost:8182/websocket";
    final WebSocketClient client = new WebSocketClient(url);
    client.open();
    final String fatty =
        IntStream.range(0, 1024).mapToObj(String::valueOf).collect(Collectors.joining());
    //    client.<String>eval(UUID.randomUUID().toString() + ":" + fatty);
    client.<String>eval("1111:2222");

    client.close();
  }

  public static void main(String[] args) {
    try {
      tryWebSocket();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
