/* (C)2023 */
package netty_pair.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import java.util.List;
import netty_pair.ResponseData;

public class ResponseDataDecoder extends ReplayingDecoder<ResponseData> {

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    System.out.println("ReplayingDecoder");
    ResponseData data = new ResponseData();
    data.setIntValue(in.readInt());
    out.add(data);
  }
}
