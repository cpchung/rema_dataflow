/* (C)2023 */
package netty_pair.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import java.nio.charset.Charset;
import netty_pair.RequestData;

public class RequestDataEncoder extends MessageToByteEncoder<RequestData> {

  private final Charset charset = Charset.forName("UTF-8");

  @Override
  protected void encode(ChannelHandlerContext ctx, RequestData msg, ByteBuf out) throws Exception {

    System.out.println("MessageToByteEncoder");
    out.writeInt(msg.getIntValue());
    out.writeInt(msg.getStringValue().length());
    out.writeCharSequence(msg.getStringValue(), charset);
  }
}
