/* (C)2023 */
package netty_pair.client;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import netty_pair.RequestData;
import netty_pair.ResponseData;

public class ClientHandler extends ChannelInboundHandlerAdapter {

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {

    RequestData msg = new RequestData();
    msg.setIntValue(123);
    msg.setStringValue("all work and no play makes jack a dull boy");
    //        msg.setStringValue("/ws1");
    ChannelFuture future = ctx.writeAndFlush(msg);
    future.sync();
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    System.out.println((ResponseData) msg);
    ctx.close();
  }
}
