package netty_pair;

import lombok.Data;

@Data
public class RequestData {
    private int intValue;
    private String stringValue;

    // standard getters and setters
}