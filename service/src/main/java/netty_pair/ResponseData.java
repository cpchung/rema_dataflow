package netty_pair;

import lombok.Data;

@Data
public class ResponseData {
    private int intValue;

    // standard getters and setters
}