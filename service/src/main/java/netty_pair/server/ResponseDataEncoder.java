/* (C)2023 */
package netty_pair.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import netty_pair.ResponseData;

public class ResponseDataEncoder extends MessageToByteEncoder<ResponseData> {

  @Override
  protected void encode(ChannelHandlerContext ctx, ResponseData msg, ByteBuf out) throws Exception {
    System.out.println("MessageToByteEncoder");
    out.writeInt(msg.getIntValue());
  }
}
