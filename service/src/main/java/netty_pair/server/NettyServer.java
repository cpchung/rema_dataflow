/* (C)2023 */
package netty_pair.server;

import netty_pair.chapter8.BootstrapWithInitializer;

public class NettyServer {

  private int port;

  private NettyServer(int port) {
    this.port = port;
  }

  public static void main(String[] args) throws Exception {
    int port;
    if (args.length > 0) {
      port = Integer.parseInt(args[0]);
    } else {
      port = 8080;
    }
    new NettyServer(port).run();
  }

  public void run() throws Exception {
    //        EventLoopGroup bossGroup = new NioEventLoopGroup();
    //        EventLoopGroup workerGroup = new NioEventLoopGroup();
    //        try {
    //            ServerBootstrap bootstrap = new ServerBootstrap();
    //               bootstrap.group(bossGroup, workerGroup)
    //                .channel(NioServerSocketChannel.class)
    //                .childHandler(new ChannelInitializer<SocketChannel>() {
    //                    @Override
    //                    public void initChannel(SocketChannel ch)
    //                            throws Exception {
    //                        ch.pipeline().addLast(
    //                                new RequestDecoder(),
    //                                new ResponseDataEncoder(),
    //                                new ProcessingHandler());
    //                    }
    //                })
    //                .option(ChannelOption.SO_BACKLOG, 128)
    //                .childOption(ChannelOption.SO_KEEPALIVE, true);
    //
    //            ChannelFuture future = bootstrap.bind(port).sync();
    //            future.channel().closeFuture().sync();
    //        } finally {
    //            workerGroup.shutdownGracefully();
    //            bossGroup.shutdownGracefully();
    //        }

    BootstrapWithInitializer bootstrapWithInitializer = new BootstrapWithInitializer();
    bootstrapWithInitializer.bootstrap();
  }
}
