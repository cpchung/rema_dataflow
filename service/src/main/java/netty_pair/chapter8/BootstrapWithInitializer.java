/* (C)2023 */
package netty_pair.chapter8;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.net.InetSocketAddress;
import netty_pair.server.ProcessingHandler;
import netty_pair.server.RequestDecoder;
import netty_pair.server.ResponseDataEncoder;

/**
 * Listing 8.6 Bootstrapping and using ChannelInitializer
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
public class BootstrapWithInitializer {

  /** Listing 8.6 Bootstrapping and using ChannelInitializer */
  public void bootstrap() throws InterruptedException {
    ServerBootstrap bootstrap = new ServerBootstrap();
    bootstrap
        .group(new NioEventLoopGroup(), new NioEventLoopGroup())
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializerImpl());
    ChannelFuture future = bootstrap.bind(new InetSocketAddress(8080));
    future.sync();

    //            ChannelFuture future = bootstrap.bind(port).sync();
    //            future.channel().closeFuture().sync();
  }

  final class ChannelInitializerImpl extends ChannelInitializer<Channel> {
    @Override
    protected void initChannel(Channel ch) throws Exception {
      ChannelPipeline pipeline = ch.pipeline();
      pipeline.addLast(
          //                    new HttpClientCodec(),
          //                    new HttpObjectAggregator(Integer.MAX_VALUE),
          new RequestDecoder(), new ResponseDataEncoder(), new ProcessingHandler());

      // HTTP编解码支持
      //            pipeline.addLast(new HttpServerCodec());
      //
      //            //HTTP大文件编写
      //            pipeline.addLast(new ChunkedWriteHandler());
      //            //HTTP Message聚合
      //            pipeline.addLast("HttpObjectAggregator",new
      // HttpObjectAggregator(Integer.MAX_VALUE));
      //
      //            pipeline.addLast(new WsSwitchHandler());
    }
  }
}
