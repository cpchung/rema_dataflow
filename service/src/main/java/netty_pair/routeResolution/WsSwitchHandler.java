/* (C)2023 */
package netty_pair.routeResolution;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import netty_pair.RequestData;
import netty_pair.ResponseData;

public class WsSwitchHandler extends ChannelInboundHandlerAdapter {

  class ProcessingHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
      System.out.println("ChannelInboundHandlerAdapter");
      RequestData requestData = (RequestData) msg;
      ResponseData responseData = new ResponseData();
      responseData.setIntValue(requestData.getIntValue() * 2);
      ChannelFuture future = ctx.writeAndFlush(responseData);
      future.addListener(ChannelFutureListener.CLOSE);
      System.out.println(requestData);
    }
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if (msg instanceof FullHttpRequest) {
      String uri = ((FullHttpRequest) msg).getUri();
      ctx.pipeline().remove(this);

      if ("/ws1".equalsIgnoreCase(uri)) {
        ctx.pipeline()
            .addAfter(
                "HttpObjectAggregator",
                WebSocketServerProtocolHandler.class.getName(),
                new WebSocketServerProtocolHandler(uri));
        ctx.pipeline()
            .addAfter(
                WebSocketServerProtocolHandler.class.getName(),
                //                        WebsocketHandler.class.getName(),
                //                        new WebsocketHandler()

                ProcessingHandler.class.getName(),
                new ProcessingHandler());
      } else if ("/ws2".equalsIgnoreCase(uri)) {
        ctx.pipeline()
            .addAfter(
                "HttpObjectAggregator",
                WebSocketServerProtocolHandler.class.getName(),
                new WebSocketServerProtocolHandler(uri));
        ctx.pipeline()
            .addAfter(
                WebSocketServerProtocolHandler.class.getName(),
                //                        WebsocketBHandler.class.getName(),
                //                        new WebsocketBHandler()
                ProcessingHandler.class.getName(),
                new ProcessingHandler());
      } else {
        ctx.channel().close();
        return;
      }

      ctx.pipeline().fireChannelRead(msg);
    }
  }
}
// https://git.eclipse.org/c/jetty/org.eclipse.jetty.project.git/tree/examples/embedded/src/main/java/org/eclipse/jetty/embedded/ManyHandlers.java
// https://github.com/netty/netty/issues/7239
