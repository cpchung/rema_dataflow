package ProviderExample;

import com.google.inject.Inject;
import javax.ejb.Stateless;
import javax.inject.Provider;

@Stateless
class Boundary {

  public String message() {
    return "Good morning";
  }
}

class Index {

  @Inject Provider<Boundary> boundary;

  public String getMessage() {
    return boundary.get().message();
  }
}

public class Main {

  public static void main(String... args) {

    Index index = new Index();

    System.out.println(index.getMessage());
  }
}
// https://www.knowledgewalls.com/j2ee/books/spring-30-examples/how-to-use-javaxinjectprovider-in-java-di-with-spring-example
// https://stackoverflow.com/questions/13487987/where-to-use-ejb-3-1-and-cdi/13504763#13504763
