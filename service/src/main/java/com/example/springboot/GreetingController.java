/* (C)2023 */
package com.example.springboot;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

  private static final String template = "Hello, %s!";
  private final AtomicLong counter = new AtomicLong();

  @RequestMapping("/greeting")
  public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
    return new Greeting(counter.incrementAndGet(), String.format(template, name));
  }

  @GetMapping(path = "/hi", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> sayHi(@RequestBody Greeting/*or Object here*/ newGreeting)
      throws IOException, SQLException {

//        To invoke this one:
//    curl --location --request GET 'localhost:8080/hi' \
//    --header 'Content-Type: application/json; charset=utf-8' \
//    --data-raw '{
//    "email": "email@email.com",
//        "password": "tuffCookie"
//  }'



    // Get data from service layer into entityList.

    System.out.println("hitting hi endpoint!");
    System.out.println(newGreeting.toString());

    Map<String, Greeting> entities = new HashMap<>();

    entities.put("1", new Greeting(111, "world"));
    entities.put("2", new Greeting(222, "hi"));
    entities.put("3", new Greeting(33, "hi"));

    return new ResponseEntity<>(entities, HttpStatus.OK);
  }
}
