/* (C)2023 */
package com.example.springboot;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import robinhood.Rema;
// import tasks.DataQualityReport;

@RestController
@RequestMapping("/rema")
public class RemaController {


  @PostMapping(path = "/submitJobs")
  public ResponseEntity<Object> submitJobs(@RequestBody Object newObject)
      throws IOException, SQLException {
    // Get data from service layer into entityList.

    System.out.println("hitting submitJobs endpoint!");

    /*curl --location 'localhost:8080/rema/submitJobs' \
    --header 'Content-Type: application/json; charset=utf-8' \
    --data '{
    "id": [1,2],
    "content": [1,2,3]
  }'*/

    Map<String, List<Integer>> map = (Map<String, List<Integer>>) newObject;

    List<Integer> value;

    for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
      String key = entry.getKey();
      value = entry.getValue();
      System.out.println(value);
    }

    //    input: List/set , output: list/set
    return new ResponseEntity<>(map, HttpStatus.OK);
  }

  @RequestMapping("/submitJobs2")
  public String home() {

    Set<Integer> input = new HashSet<>(Arrays.asList(2));

    Map<Integer, Set<Integer>> graph = new HashMap<>() {
      {
        put(7, new HashSet<>(Arrays.asList(6)));
        put(6, new HashSet<>(Arrays.asList(3, 4, 5)));
        put(5, new HashSet<>(Arrays.asList(3, 4)));
        put(4, new HashSet<>(Arrays.asList(2)));
        put(3, new HashSet<>(Arrays.asList(1)));
        put(2, new HashSet<>(Arrays.asList(0)));
        put(1, new HashSet<>(Arrays.asList(2)));
        put(0, new HashSet<>(Arrays.asList()));
      }
    };
    new Rema().delayedExecution(graph, input);
    return "Hello Docker World";
  }


}
