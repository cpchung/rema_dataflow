/* (C)2023 */
package materialized_view;

import static optimizer.Util.print;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import optimizer.Optimizer;
import optimizer.SimpleSchema;
import optimizer.SimpleTable;
import org.apache.calcite.adapter.enumerable.EnumerableRules;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.externalize.RelDotWriter;
import org.apache.calcite.rel.rules.CoreRules;
import org.apache.calcite.sql.SqlExplainLevel;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.type.SqlTypeName;
import org.apache.calcite.tools.RuleSet;
import org.apache.calcite.tools.RuleSets;

public class Rewrite {

  static RelNode sqlToRel(Optimizer optimizer, String query) {
    /// parse sql string to sql tree
    SqlNode queryTree;
    try {
      queryTree = optimizer.parse(query);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    /// validate sql tree
    SqlNode validatedQueryTree = optimizer.validate(queryTree);
    RelNode queryRelTree = optimizer.convert(validatedQueryTree);

    return queryRelTree;

  }

  static void printDAG(RelNode relNode) {
    //    String sql = "select 1 + 2, 3 from (values (true))";
//    final RelNode rel = tester.convertSqlToRel(sql).rel;
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    RelDotWriter planWriter =
        new RelDotWriter(pw, SqlExplainLevel.EXPPLAN_ATTRIBUTES, false);
    relNode.explain(planWriter);
    System.out.println(sw);
    pw.flush();

  }

  public static void main(String[] args) throws Exception {
    SimpleTable depts =
        SimpleTable.newBuilder("depts")
            .addField("deptno", SqlTypeName.INTEGER)
            .addField("deptname", SqlTypeName.VARCHAR)
            .withRowCount(60_000L)
            .build();

    SimpleTable locations =
        SimpleTable.newBuilder("locations")
            .addField("locationid", SqlTypeName.INTEGER)
            .addField("state", SqlTypeName.CHAR)
            .withRowCount(60_000L)
            .build();

    SimpleTable emps =
        SimpleTable.newBuilder("emps")
            .addField("empid", SqlTypeName.INTEGER)
            .addField("deptno", SqlTypeName.INTEGER)
            .addField("locationid", SqlTypeName.INTEGER)
            .addField("empname", SqlTypeName.VARCHAR)
            .addField("salary", SqlTypeName.DECIMAL)
            .addField("deptname", SqlTypeName.VARCHAR)
            .withRowCount(60_000L)
            .build();

    SimpleTable mvTable =
        SimpleTable.newBuilder("mv")
            .addField("empid", SqlTypeName.INTEGER)  //for first rewrite
            .addField("deptno", SqlTypeName.INTEGER) // for second rewrite
//            .addField("deptname", SqlTypeName.VARCHAR) // Union rewriting
            .withRowCount(60_000L)
            .build();

    SimpleSchema schema =
        SimpleSchema.newBuilder("calcite")
            .addTable(depts)
            .addTable(locations)
            .addTable(emps)
            .addTable(mvTable)
            .build();

    Optimizer optimizer = Optimizer.create(schema);

    List<List<String>> queryGroup = List.of(
//        Join rewriting
//        List.of("SELECT empid\n"
//            + "FROM depts\n"
//            + "JOIN (\n"
//            + "  SELECT empid, deptno\n"
//            + "  FROM emps\n"
//            + "  WHERE empid = 1) AS subq\n"
//            + "ON depts.deptno = subq.deptno",
//
//            "SELECT empid\n" + "FROM emps\n" + "JOIN depts USING (deptno)",
//
//            "SELECT empid\n"
//                + "FROM mv\n"
//                + "WHERE empid = 1")

//        Aggregate rewriting
//        ,
//        List.of("SELECT deptno\n"
//                + "FROM emps\n"
//                + "WHERE deptno > 10\n"
//                + "GROUP BY deptno",
//
//            "SELECT empid, deptno\n"
//                + "FROM emps\n"
//                + "WHERE deptno > 5\n"
//                + "GROUP BY empid, deptno",
//
//            "SELECT deptno\n"
//                + "FROM mv\n"
//                + "WHERE deptno > 10\n"
//                + "GROUP BY deptno")
//        ,

//        Aggregate rewriting (with aggregation rollup)
//       ,
//       List.of(
//           "SELECT deptno, COUNT(*) AS c, SUM(salary) AS s\n"
//           + "FROM emps\n"
//           + "GROUP BY deptno",
//
//
//           "SELECT empid, deptno, COUNT(*) AS c, SUM(salary) AS s\n"
//               + "FROM emps\n"
//               + "GROUP BY empid, deptno"
//           ,
//           "SELECT deptno, SUM(c), SUM(s)\n"
//               + "FROM mv\n"
//               + "GROUP BY deptno" ) //  SUM(c), SUM(s) ?????

//        Query partial rewriting
//        List.of(
//            "SELECT deptno, COUNT(*)\n"
//                + "FROM emps\n"
//                + "GROUP BY deptno",
//
//            "SELECT empid, depts.deptno, COUNT(*) AS c, SUM(salary) AS s\n"
//                + "FROM emps\n"
//                + "JOIN depts USING (deptno)\n"
//                + "GROUP BY empid, depts.deptno"
//
//            ,
//            "SELECT deptno, SUM(c)\n"
//                + "FROM mv\n"
//                + "GROUP BY deptno"

//            Union rewriting
        List.of(

            "SELECT empid, deptname\n"
                + "FROM emps\n"
                + "JOIN depts ON emps.deptno = depts.deptno\n"
                + "WHERE salary > 10000"
            ,
            "SELECT empid, deptname\n"
                + "FROM emps\n"
                + "JOIN depts ON emps.deptno = depts.deptno\n"
                + "WHERE salary > 12000"
            ,
            "SELECT empid, deptname\n"
                + "FROM mv\n"
                + "UNION ALL\n"
                + "SELECT empid, deptname\n"
                + "FROM emps\n"
                + "JOIN depts ON emps.deptno = depts.deptno\n"
                + "WHERE salary > 10000 AND salary <= 12000"
        )

//        Union rewriting with aggregate

//        List.of(
//            "SELECT empid, deptname, SUM(salary) AS s\n"
//                + "FROM emps\n"
//                + "JOIN depts ON emps.deptno = depts.deptno\n"
//                + "WHERE salary > 10000\n"
//                + "GROUP BY empid, deptname",
//
//            "SELECT empid, deptname, SUM(salary) AS s\n"
//                + "FROM emps\n"
//                + "JOIN depts ON emps.deptno = depts.deptno\n"
//                + "WHERE salary > 12000\n"
//                + "GROUP BY empid, deptname",
//
//            "SELECT empid, deptname, SUM(s)\n"
//                + "FROM (\n"
//                + "  SELECT empid, deptname, s\n"
//                + "  FROM mv\n"
//                + "  UNION ALL\n"
//                + "  SELECT empid, deptname, SUM(salary) AS s\n"
//                + "  FROM emps\n"
//                + "  JOIN depts ON emps.deptno = depts.deptno\n"
//                + "  WHERE salary > 10000 AND salary <= 12000\n"
//                + "  GROUP BY empid, deptname) AS subq\n"
//                + "GROUP BY empid, deptname"
//        )
    );

    RuleSet rules =
        RuleSets.ofList(
            CoreRules.PROJECT_FILTER_TRANSPOSE,
            CoreRules.FILTER_PROJECT_TRANSPOSE,
            CoreRules.FILTER_TO_CALC,
            CoreRules.PROJECT_TO_CALC,
            CoreRules.FILTER_CALC_MERGE,
            CoreRules.PROJECT_CALC_MERGE,
            EnumerableRules.ENUMERABLE_TABLE_SCAN_RULE,
            EnumerableRules.ENUMERABLE_JOIN_RULE, //was missing
            EnumerableRules.ENUMERABLE_PROJECT_RULE,
            EnumerableRules.ENUMERABLE_FILTER_RULE,
            EnumerableRules.ENUMERABLE_CALC_RULE,
            EnumerableRules.ENUMERABLE_AGGREGATE_RULE);

    for (List<String> queries : queryGroup) {

      for (String query : queries) {
        RelNode mvRelNode = sqlToRel(optimizer, query);
        print("after CONVERSION", mvRelNode);
        printDAG(mvRelNode);

//        RelNode optimizedUserQueryTree = optimizer.optimize(
//            mvRelNode,
//            mvRelNode.getTraitSet().plus(EnumerableConvention.INSTANCE),
//            rules
//        );
//
//        print("AFTER OPTIMIZATION", optimizedUserQueryTree);
//        printDAG(optimizedUserQueryTree);
      }
      System.out.println("!!");

    }


  }
}
