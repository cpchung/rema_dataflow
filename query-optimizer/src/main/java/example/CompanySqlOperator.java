/* (C)2023 */
package example;

import org.apache.calcite.sql.SqlAggFunction;
import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.util.ReflectiveSqlOperatorTable;

public class CompanySqlOperator extends ReflectiveSqlOperatorTable {

  private static CompanySqlOperator instance;

  public static final SqlAggFunction APPROX_PERCENTILE =
      new TDigest.SqlApproximatePercentileFunction();
  public static final SqlAggFunction TDIGEST = new TDigest.SqlTDigestAggFunction();

  public static final SqlFunction TDIGEST_QUANTILE = new TDigest.SqlTDigestQuantileFunction();

  private CompanySqlOperator() {}

  public static synchronized CompanySqlOperator instance() {
    if (instance == null) {
      instance = new CompanySqlOperator();
      instance.init();
    }

    return instance;
  }
}
