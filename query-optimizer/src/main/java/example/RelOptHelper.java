/* (C)2023 */
package example;

import org.apache.calcite.plan.RelOptRule;
import org.apache.calcite.plan.RelOptRuleOperand;
import org.apache.calcite.plan.RelTrait;
import org.apache.calcite.rel.RelNode;

public class RelOptHelper {

  public static RelOptRuleOperand any(Class<? extends RelNode> first) {
    return RelOptRule.operand(first, RelOptRule.any());
  }

  public static RelOptRuleOperand some(
      Class<? extends RelNode> rel,
      RelTrait trait,
      RelOptRuleOperand first,
      RelOptRuleOperand... rest) {
    return RelOptRule.operand(rel, trait, RelOptRule.some(first, rest));
  }
}
