/* (C)2023 */
package example;

import static org.apache.calcite.sql.type.OperandTypes.family;

import org.apache.calcite.sql.SqlAggFunction;
import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.SqlFunctionCategory;
import org.apache.calcite.sql.SqlKind;
import org.apache.calcite.sql.type.InferTypes;
import org.apache.calcite.sql.type.OperandTypes;
import org.apache.calcite.sql.type.ReturnTypes;
import org.apache.calcite.sql.type.SqlTypeFamily;

public class TDigest {

  public static final class SqlTDigestAggFunction extends SqlAggFunction {

    public SqlTDigestAggFunction() {
      super(
          "TDIGEST",
          null,
          SqlKind.OTHER_FUNCTION,
          ReturnTypes.DOUBLE,
          null,
          OperandTypes.family(SqlTypeFamily.NUMERIC),
          SqlFunctionCategory.USER_DEFINED_FUNCTION,
          false,
          false);
    }
  }

  public static final class SqlApproximatePercentileFunction extends SqlAggFunction {
    public SqlApproximatePercentileFunction() {
      super(
          "APPROX_PERCENTILE",
          null,
          SqlKind.OTHER_FUNCTION,
          ReturnTypes.DOUBLE,
          //          null,
          InferTypes.FIRST_KNOWN,
          family(SqlTypeFamily.NUMERIC, SqlTypeFamily.DECIMAL),
          SqlFunctionCategory.USER_DEFINED_FUNCTION,
          false,
          false);
    }
  }

  public static final class SqlTDigestQuantileFunction extends SqlFunction {
    public SqlTDigestQuantileFunction() {
      super(
          "TDIGEST_QUANTILE",
          SqlKind.OTHER_FUNCTION,
          ReturnTypes.DOUBLE,
          null,
          family(SqlTypeFamily.NUMERIC),
          SqlFunctionCategory.USER_DEFINED_FUNCTION);
    }
  }
}
