/* (C)2023 */
package example;

import static optimizer.Util.print;

import optimizer.Optimizer;
import optimizer.SimpleSchema;
import optimizer.SimpleTable;
import org.apache.calcite.plan.RelOptRule;
import org.apache.calcite.plan.hep.HepPlanner;
import org.apache.calcite.plan.hep.HepProgram;
import org.apache.calcite.plan.hep.HepProgramBuilder;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.rules.CoreRules;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.type.SqlTypeName;

public class Main {

  private static HepPlanner hepPlanner(RelOptRule... relOptRules) {
    HepProgramBuilder hepProgramBuilder = HepProgram.builder();

    for (RelOptRule relOptRule : relOptRules) {
      hepProgramBuilder.addRuleInstance(relOptRule);
    }
    return new HepPlanner(hepProgramBuilder.build());
  }

  public static void example() throws Exception {

    SimpleTable emp =
        SimpleTable.newBuilder("emp")
            .addField("sal", SqlTypeName.DECIMAL)
            .withRowCount(60_000L)
            .build();

    SimpleTable lineitem =
        SimpleTable.newBuilder("lineitem")
            .addField("l_quantity", SqlTypeName.DECIMAL)
            .addField("price", SqlTypeName.DECIMAL)
            //            .addField("price", SqlTypeName.DATE)
            //            .addField("price", SqlTypeName.VARCHAR)
            .addField("discount", SqlTypeName.DECIMAL)
            .addField("l_shipdate", SqlTypeName.DATE)
            .withRowCount(60_000L)
            .build();

    SimpleSchema schema = SimpleSchema.newBuilder("tpch").addTable(lineitem).addTable(emp).build();

    Optimizer optimizer = Optimizer.create(schema);

    String sql =
        "select sum (l.price *l.discount) as revenue\n" + "from lineitem l where l.price < 10";

    sql =
        "select sal\n"
            + "from emp\n"
            + "where case when (sal = 1000) then\n"
            + "(case when sal = 1000 then null else 1 end is null) else\n"
            + "(case when sal = 2000 then null else 1 end is null) end is true";

    sql = "select approx_percentile(l.price, 0.6) from lineitem l";
    sql =
        "select approx_percentile(l.price, 0.0),approx_percentile(l.price,"
            + " 0.5),approx_percentile(l.price, 1.0) from lineitem l";

    //        sql= "select MIN(price), MAX(discount) from lineitem";
    //    sql = "select approx_percentile(0.6, l.price) from lineitem l";

    /// parse sql string to sql tree
    SqlNode sqlTree = optimizer.parse(sql);
    /// validate sql tree
    SqlNode validatedSqlTree = optimizer.validate(sqlTree);
    RelNode relTree = optimizer.convert(validatedSqlTree);

    print("before CONVERSION", relTree);

    HepPlanner hepPlanner =
        hepPlanner(
            CoreRules.PROJECT_FILTER_TRANSPOSE,
            CoreRules.FILTER_REDUCE_EXPRESSIONS,
            ApproxPercentileRewriteRule.INSTANCE);

    hepPlanner.setRoot(relTree);

    RelNode actual = hepPlanner.findBestExp();

    print("AFTER OPTIMIZATION", actual);
  }

  public static void main(String[] args) throws Exception {
    example();

    String sql =
        "SELECT emp1.employee_id AS id_one, emp1.rating AS rating, count(*) AS the_count\n"
            + "FROM cp.\"data/employees.json\" AS emp1\n"
            + "INNER JOIN cp.\"data/employees.json\" AS emp2\n"
            + "  ON emp1.position_id = emp2.position_id\n"
            + "  AND emp1.employee_id != emp2.employee_id\n"
            + "  AND emp1.rating > (\n"
            + "    SELECT AVG(emp3.rating)\n"
            + "    FROM cp.\"data/employees.json\" AS emp3\n"
            + "    WHERE emp3.position_id = emp1.position_id\n"
            + ") GROUP BY emp1.employee_id, emp1.rating\n"
            + "ORDER BY emp1.employee_id";
  }
}
