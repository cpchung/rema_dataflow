package example;

import java.util.Collections;
import java.util.Properties;
import org.apache.calcite.avatica.util.Casing;
import org.apache.calcite.config.CalciteConnectionConfig;
import org.apache.calcite.config.CalciteConnectionConfigImpl;
import org.apache.calcite.config.CalciteConnectionProperty;
import org.apache.calcite.jdbc.CalciteSchema;
import org.apache.calcite.jdbc.JavaTypeFactoryImpl;
import org.apache.calcite.prepare.CalciteCatalogReader;
import org.apache.calcite.prepare.Prepare;
import org.apache.calcite.rel.type.RelDataTypeFactory;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.schema.impl.ScalarFunctionImpl;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;


public class UdfTest {
    private static final String SQL = "select SQUARE_FUNC(1)";
    private static final String FUN_NAME = "SQUARE_FUNC";

    public static void main(String[] args) throws Exception {
        useFramworksExec();
    }

    private static void useFramworksExec() throws Exception {
        CalciteSchema rootSchema = CalciteSchema.createRootSchema(false, false);
        SchemaPlus schema = rootSchema.plus();
        schema.add(FUN_NAME, ScalarFunctionImpl.create(SquareFunction.class, "eval"));


        Properties configProperties = new Properties();
        configProperties.put(CalciteConnectionProperty.CASE_SENSITIVE.camelName(), Boolean.TRUE.toString());
        configProperties.put(CalciteConnectionProperty.UNQUOTED_CASING.camelName(), Casing.UNCHANGED.toString());
        configProperties.put(CalciteConnectionProperty.QUOTED_CASING.camelName(), Casing.UNCHANGED.toString());
        CalciteConnectionConfig configs = new CalciteConnectionConfigImpl(configProperties);

        RelDataTypeFactory typeFactory = new JavaTypeFactoryImpl();
        Prepare.CatalogReader reader = new CalciteCatalogReader(
                rootSchema,
                Collections.singletonList(schema.getName()),
                typeFactory,
                configs
        );

        FrameworkConfig config = Frameworks.newConfigBuilder().operatorTable(reader).defaultSchema(schema).build();
        Planner planner = Frameworks.getPlanner(config);
        SqlNode ast = planner.parse(SQL);
        SqlNode validatedAst = planner.validate(ast);
        System.out.println(validatedAst.toString());
    }
}

//public class UdfTest {
//    private static final String SQL = "select SQUARE_FUNC(1)";
//    private static final String FUN_NAME = "SQUARE_FUNC";
//
//    public static void main(String[] args) throws Exception {
//        useFramworksExec();
//    }
//
//    private static void useFramworksExec() throws Exception {
//        CalciteSchema rootSchema = CalciteSchema.createRootSchema(false, false);
//        SchemaPlus schema = rootSchema.plus();
//        schema.add(FUN_NAME, ScalarFunctionImpl.create(SquareFunction.class, "eval"));
////        CalciteCatalogReader reader = new CalciteCatalogReader(rootSchema,
////                SqlParser.Config.DEFAULT.caseSensitive(),
////                rootSchema.path(null),
////                new JavaTypeFactoryImpl());
//
//
//        Properties configProperties = new Properties();
//        configProperties.put(CalciteConnectionProperty.CASE_SENSITIVE.camelName(), Boolean.TRUE.toString());
//        configProperties.put(CalciteConnectionProperty.UNQUOTED_CASING.camelName(), Casing.UNCHANGED.toString());
//        configProperties.put(CalciteConnectionProperty.QUOTED_CASING.camelName(), Casing.UNCHANGED.toString());
//        CalciteConnectionConfig configs = new CalciteConnectionConfigImpl(configProperties);
//
//        RelDataTypeFactory typeFactory = new JavaTypeFactoryImpl();
//        Prepare.CatalogReader reader = new CalciteCatalogReader(
//                rootSchema,
//                Collections.singletonList(schema.getName()),
//                typeFactory,
//                configs
//        );
//
//        FrameworkConfig config = Frameworks.newConfigBuilder().operatorTable(reader).defaultSchema(schema).build();
////        Planner planner = Frameworks.getPlanner(config);
////        SqlNode ast = planner.parse(SQL);
//        SqlParser.ConfigBuilder parserConfig = SqlParser.configBuilder();
//
//
//        SqlParser parser = SqlParser.create(SQL, parserConfig.build());
//
//        SqlNode ast = parser.parseStmt();
//
//        SqlNode validatedAst = ast;
////        SqlNode validatedAst = planner.validate(ast);
////        System.out.println(validatedAst.toString());
//
//
//        SqlOperatorTable operatorTable = ChainedSqlOperatorTable.of(SqlStdOperatorTable.instance());
//
//        SqlValidator.Config validatorConfig = SqlValidator.Config.DEFAULT
////                .withLenientOperatorLookup(config.lenientOperatorLookup())
////                .withSqlConformance(config.conformance())
////                .withDefaultNullCollation(config.defaultNullCollation())
//                .withIdentifierExpansion(true);
//
//        SqlValidator validator = SqlValidatorUtil.newValidator(operatorTable, reader, typeFactory, validatorConfig);
//
//        VolcanoPlanner planner = new VolcanoPlanner(RelOptCostImpl.FACTORY, Contexts.of(config));
//        planner.addRelTraitDef(ConventionTraitDef.INSTANCE);
//
//        RelOptCluster cluster = RelOptCluster.create(planner, new RexBuilder(typeFactory));
//        SqlToRelConverter.Config converterConfig = SqlToRelConverter.configBuilder()
//                .withTrimUnusedFields(true)
//                .withExpand(false) // https://issues.apache.org/jira/browse/CALCITE-1045
//                .build();
//
//
//
//
//        SqlToRelConverter converter = new SqlToRelConverter(
//                null,
//                validator,
//                reader,
//                cluster,
//                StandardConvertletTable.INSTANCE,
//                converterConfig
//        );
//
//        RelRoot root = converter.convertQuery(validatedAst, false, true);
//
//        RelNode relNode = root.rel;
//
//        System.out.println(RelOptUtil.toString(relNode));
//
//
//    }
//}