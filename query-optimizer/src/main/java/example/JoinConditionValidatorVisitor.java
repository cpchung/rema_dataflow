package example;

import org.apache.calcite.rex.RexCall;
import org.apache.calcite.rex.RexInputRef;
import org.apache.calcite.rex.RexVisitorImpl;
import org.apache.calcite.sql.SqlKind;


public final class JoinConditionValidatorVisitor extends RexVisitorImpl<Void> {


  private final int left;
  private final int right;

  private boolean isValid;

  protected JoinConditionValidatorVisitor(int left, int right) {
    super(true);

    this.left = left;
    this.right = right;
    this.isValid = true;
  }


  @Override
  public Void visitCall(RexCall call) {
    if (call.op.kind == SqlKind.EQUALS
        || call.op.kind == SqlKind.NOT_EQUALS
        || call.op.kind == SqlKind.LESS_THAN
    ) {
      if (call.operands.get(0) instanceof RexInputRef && call.operands.get(
          1) instanceof RexInputRef) {

        int in1 = ((RexInputRef) call.operands.get(0)).getIndex();
        int in2 = ((RexInputRef) call.operands.get(1)).getIndex();

        if ((in1 < left && in2 < left)
            || (in1 >= left && in2 >= left)
            || (in1 >= left + right || in2 >= left + right)) {

          isValid = false;
        }
      }
    }

    return super.visitCall(call);
  }
}
