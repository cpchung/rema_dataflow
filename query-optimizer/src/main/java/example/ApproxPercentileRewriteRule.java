/* (C)2023 */
package example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.calcite.plan.Contexts;
import org.apache.calcite.plan.Convention;
import org.apache.calcite.plan.RelOptRule;
import org.apache.calcite.plan.RelOptRuleCall;
import org.apache.calcite.plan.hep.HepRelVertex;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.core.Aggregate;
import org.apache.calcite.rel.core.AggregateCall;
import org.apache.calcite.rel.core.RelFactories;
import org.apache.calcite.rel.logical.LogicalAggregate;
import org.apache.calcite.rel.logical.LogicalProject;
import org.apache.calcite.rex.RexBuilder;
import org.apache.calcite.rex.RexNode;
import org.apache.calcite.tools.RelBuilder;
import org.apache.calcite.tools.RelBuilderFactory;

public final class ApproxPercentileRewriteRule extends RelOptRule {

  public static final ApproxPercentileRewriteRule INSTANCE =
      new ApproxPercentileRewriteRule(
          RelBuilder.proto(
              Contexts.of(
                  RelFactories.DEFAULT_PROJECT_FACTORY, RelFactories.DEFAULT_FILTER_FACTORY)));

  //  public ApproxPercentileRewriteRule(RelBuilderFactory factory) {
  //
  ////    public static RelOptRuleOperand any(Class<? extends RelNode> first) {
  ////      return RelOptRule.operand(first, RelOptRule.any());
  ////    }
  ////
  ////    public static RelOptRuleOperand some(
  ////        Class<? extends RelNode> rel,
  ////        RelTrait trait,
  ////        RelOptRuleOperand first,
  ////        RelOptRuleOperand... rest) {
  ////      return RelOptRule.operand(rel, trait, RelOptRule.some(first, rest));
  ////    }
  //    super(
  //          RelOptHelper.some(LogicalAggregate.class, Convention.NONE,
  // RelOptHelper.any(RelNode.class)),
  //        factory,
  //        "ApproxPercentileRewriteRule");
  //
  ////    super(
  ////        RelOptRule.operand(),
  ////        factory,
  ////        "ApproxPercentileRewriteRule");
  //  }

  public ApproxPercentileRewriteRule(RelBuilderFactory relBuilderFactory) {
    super(
        RelOptRule.operand(
            LogicalAggregate.class,
            Convention.NONE,
            RelOptRule.some(RelOptRule.operand(RelNode.class, RelOptRule.any()))),
        relBuilderFactory,
        "ApproxPercentileRewriteRule");
  }

  @Override
  public boolean matches(RelOptRuleCall relOptRuleCall) {

    final Aggregate aggregate = relOptRuleCall.rel(0);

    for (AggregateCall aggregateCall : aggregate.getAggCallList()) {
      if (aggregateCall.getAggregation() == CompanySqlOperator.APPROX_PERCENTILE) {
        if (!aggregateCall.hasFilter()) {
          return true;
        }
      }
    }
    return false;
  }

  public void onMatch(RelOptRuleCall call) {

    final LogicalAggregate agg = call.rel(0);

    List<AggregateCall> calls = new ArrayList();

    for (AggregateCall aggregateCall : agg.getAggCallList()) {
      if (aggregateCall.getAggregation() != CompanySqlOperator.APPROX_PERCENTILE) {
        calls.add(aggregateCall);
        continue;
      }

      calls.add(
          AggregateCall.create(
              CompanySqlOperator.TDIGEST,
              aggregateCall.isDistinct(),
              true,
              Arrays.asList(aggregateCall.getArgList().get(0)),
              -1,
              aggregateCall.getType(),
              aggregateCall.getName()));
    }

    RelNode aggInput = agg.getInput();

    final RexBuilder rexBuilder = agg.getCluster().getRexBuilder();
    final RelBuilder builder = call.builder();

    LogicalProject currentRel = (LogicalProject) ((HepRelVertex) aggInput).getCurrentRel();

    RexNode columnName = currentRel.getProjects().get(0);

    //    RexNode columnName =
    //        rexBuilder.makeInputRef(rexBuilder.getTypeFactory().createSqlType(SqlTypeName.DOUBLE),
    // 0);

    RexNode percentile = currentRel.getProjects().get(1);
    //    RexNode percentile =
    //    rexBuilder.makeInputRef(rexBuilder.getTypeFactory().createSqlType(SqlTypeName.DOUBLE), 1);

    System.out.println(columnName.getType().getSqlTypeName());
    System.out.println(percentile.getType().getSqlTypeName());

    if (columnName.getType().getSqlTypeName().getName().equals("")) {
      throw new IllegalArgumentException("");
    }

    final RelNode rewrittenQuery =
        builder
            .push(aggInput)
            .aggregate(builder.groupKey(agg.getGroupSet()), calls)
            .project(builder.call(CompanySqlOperator.TDIGEST_QUANTILE, percentile, columnName))
            .build();

    call.transformTo(rewrittenQuery);
  }
}
