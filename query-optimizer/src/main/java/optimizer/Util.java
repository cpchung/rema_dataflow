package optimizer;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.externalize.RelWriterImpl;
import org.apache.calcite.sql.SqlExplainLevel;

public class Util {
  public static void print(String header, RelNode relTree) {
    StringWriter sw = new StringWriter();

    sw.append(header).append(":").append("\n");

    RelWriterImpl relWriter = new RelWriterImpl(new PrintWriter(sw), SqlExplainLevel.ALL_ATTRIBUTES, true);

    relTree.explain(relWriter);

    System.out.println(sw.toString());
  }
}
