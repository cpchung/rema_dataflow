package CommonSubexpressionElimination;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommonSubexpressionElimination {
  public static void main(String args[]) throws IOException {
    String s, temp;
    String arr[][] = new String[10][2]; // assuming 10 unique operations with LHS and RHS stored
    int flag = 0, index = 0;

    String root = "/home/cpchung/code/CypherANTLR/src/main/java/";
    BufferedReader br =
        new BufferedReader(
            new InputStreamReader(
                new FileInputStream(root + "CommonSubexpressionElimination" + "/input.txt")));
    File op = new File(root + "CommonSubexpressionElimination/output.txt");
    if (!op.exists()) op.createNewFile();
    BufferedWriter output = new BufferedWriter(new FileWriter(op.getAbsoluteFile()));
    for (; (s = br.readLine()) != null; flag = 0) {
      temp = s.substring(s.indexOf("=") + 1);
      for (int i = 0; i < index; i++) {
        if (temp.equals(arr[i][1])) {
          flag = 1;
          break;
        } else if (temp.contains(arr[i][1])) s = s.replaceAll(arr[i][1], arr[i][0]);
      }
      if (flag == 0) {
        arr[index][0] = s.substring(0, s.indexOf("="));
        arr[index][1] = temp;
        index++;
        output.write(s);
        output.newLine();
      }
    }
    output.close();
  }
}
