/* (C)2023 */
package calcite;

import org.apache.calcite.config.Lex;
import org.apache.calcite.rel.RelRoot;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.Planner;
import org.apache.calcite.tools.RelConversionException;
import org.apache.calcite.tools.ValidationException;

public class SqlParserSample {

  static void test(String query) {
    Planner planner =
        Frameworks.getPlanner(
            Frameworks.newConfigBuilder()
                .defaultSchema(Frameworks.createRootSchema(false))
                .build());

    SqlNode parsed = null;

    try {
      parsed = planner.parse(query);
    } catch (SqlParseException e) {
      e.printStackTrace();
    }

    try {
      planner.validate(parsed);
    } catch (ValidationException e) {
      e.printStackTrace();
    }

    try {
      RelRoot relRoot = planner.rel(parsed);
    } catch (RelConversionException e) {
      e.printStackTrace();
    }
  }

  void queryToRelationalAlgebraRoot() {
    //
    // https://stackoverflow.com/questions/55367966/converting-sql-query-with-aggregate-function-to-relational-algebra-expression-in
  }

  public static void main(String[] args) throws SqlParseException {
    // Sql statement
    String query = "SELECT * from emps where id = 1";
    //        query = "SELECT * from emps where id = 1";

    query = "SELECT pn FROM p WHERE pId IN (SELECT pId FROM orders WHERE Quantity > 100)";
    //         query = "SELECT s.dnasamplename, e.Total_expression_level,
    // e.Soluble_expression_level," +
    //                " s.id " +
    //                "FROM table1 e" +
    //                "JOIN table2 s on s.constructname = e.Clone_name" +
    //                "WHERE e.Total_expression_level like '0:%'";

    // Analyze the configuration
    SqlParser.Config mysqlConfig = SqlParser.configBuilder().setLex(Lex.MYSQL).build();
    // Create a parser
    SqlParser sqlParseTree = SqlParser.create(query, mysqlConfig);
    // parse sql
    SqlNode sqlNode = sqlParseTree.parseQuery();



    test(query);
  }
}
