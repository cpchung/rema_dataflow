/* (C)2023 */
package optimizer;

import static optimizer.Util.print;

import org.apache.calcite.adapter.enumerable.EnumerableConvention;
import org.apache.calcite.adapter.enumerable.EnumerableRules;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.rules.CoreRules;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.type.SqlTypeName;
import org.apache.calcite.tools.RuleSet;
import org.apache.calcite.tools.RuleSets;
import org.junit.Test;

public class OptimizerTest {
  @Test
  public void test_tpch_q6() throws Exception {
    SimpleTable lineitem =
        SimpleTable.newBuilder("lineitem")
            .addField("l_quantity", SqlTypeName.DECIMAL)
            .addField("l_extendedprice", SqlTypeName.DECIMAL)
            .addField("l_discount", SqlTypeName.DECIMAL)
            .addField("l_shipdate", SqlTypeName.DATE)
            .withRowCount(60_000L)
            .build();

    SimpleSchema schema = SimpleSchema.newBuilder("tpch").addTable(lineitem).build();

    Optimizer optimizer = Optimizer.create(schema);
    //        select sum(l.l_extendedprice * l.l_discount) as revenue
    //        from lineitem l
    //        where l.l_shipdate >= ?
    //            and l.l_shipdate < ?
    //            and l.l_discount between (? - 0.01) AND (? + 0.01)
    //        and l.l_quantity < ?
    String sql =
        "select\n"
            + "    sum(l.l_extendedprice * l.l_discount) as revenue\n"
            + "from\n"
            + "    lineitem l\n"
            + "where\n"
            + "    l.l_shipdate >= ?\n"
            + "    and l.l_shipdate < ?\n"
            + "    and l.l_discount between (? - 0.01) AND (? + 0.01)\n"
            + "    and l.l_quantity < ?";

    /// parse sql string to sql tree
    SqlNode sqlTree = optimizer.parse(sql);
    /// validate sql tree
    SqlNode validatedSqlTree = optimizer.validate(sqlTree);
    RelNode relTree = optimizer.convert(validatedSqlTree);

    print("AFTER CONVERSION", relTree);

    RuleSet rules =
        RuleSets.ofList(
            CoreRules.FILTER_TO_CALC,
            CoreRules.PROJECT_TO_CALC,
            CoreRules.FILTER_CALC_MERGE,
            CoreRules.PROJECT_CALC_MERGE,
            EnumerableRules.ENUMERABLE_TABLE_SCAN_RULE,
            EnumerableRules.ENUMERABLE_PROJECT_RULE,
            EnumerableRules.ENUMERABLE_FILTER_RULE,
            EnumerableRules.ENUMERABLE_CALC_RULE,
            EnumerableRules.ENUMERABLE_AGGREGATE_RULE);

    RelNode optimizerRelTree =
        optimizer.optimize(
            relTree, relTree.getTraitSet().plus(EnumerableConvention.INSTANCE), rules);

    print("AFTER OPTIMIZATION", optimizerRelTree);
    System.out.println("!!");
  }

  @Test
  public void testJoinCondition() throws Exception {

    //        SELECT *
    //            FROM A AS "o"
    //        INNER JOIN (SELECT *
    //            FROM B
    //        ) AS "l" ON "o"."o_orderkey" = "l"."l_orderkey"
    SimpleTable lineitem =
        SimpleTable.newBuilder("lineitem")
            .addField("a", SqlTypeName.DECIMAL)
            .addField("b", SqlTypeName.DECIMAL)
            .addField("c", SqlTypeName.DECIMAL)
            .addField("d", SqlTypeName.DATE)
            .withRowCount(10L)
            .build();

    SimpleSchema schema = SimpleSchema.newBuilder("tpch").addTable(lineitem).build();

    Optimizer optimizer = Optimizer.create(schema);
    //        select sum(l.l_extendedprice * l.l_discount) as revenue
    //        from lineitem l
    //        where l.l_shipdate >= ?
    //            and l.l_shipdate < ?
    //            and l.l_discount between (? - 0.01) AND (? + 0.01)
    //        and l.l_quantity < ?
    String sql =
        "select\n"
            + "    sum(l.l_extendedprice * l.l_discount) as revenue\n"
            + "from\n"
            + "    lineitem l\n"
            + "where\n"
            + "    l.l_shipdate >= ?\n"
            + "    and l.l_shipdate < ?\n"
            + "    and l.l_discount between (? - 0.01) AND (? + 0.01)\n"
            + "    and l.l_quantity < ?";

    /// parse sql string to sql tree
    SqlNode sqlTree = optimizer.parse(sql);
    /// validate sql tree
    //        SqlNode validatedSqlTree = optimizer.validate(sqlTree);
    //        RelNode relTree = optimizer.convert(validatedSqlTree);
    //
    //        print("AFTER CONVERSION", relTree);
    //
    //        RuleSet rules = RuleSets.ofList(
    //            CoreRules.FILTER_TO_CALC,
    //            CoreRules.PROJECT_TO_CALC,
    //            CoreRules.FILTER_CALC_MERGE,
    //            CoreRules.PROJECT_CALC_MERGE,
    //            EnumerableRules.ENUMERABLE_TABLE_SCAN_RULE,
    //            EnumerableRules.ENUMERABLE_PROJECT_RULE,
    //            EnumerableRules.ENUMERABLE_FILTER_RULE,
    //            EnumerableRules.ENUMERABLE_CALC_RULE,
    //            EnumerableRules.ENUMERABLE_AGGREGATE_RULE
    //        );
    //
    //        RelNode optimizerRelTree = optimizer.optimize(
    //            relTree,
    //            relTree.getTraitSet().plus(EnumerableConvention.INSTANCE),
    //            rules
    //        );
    //
    //        print("AFTER OPTIMIZATION", optimizerRelTree);
  }
}
