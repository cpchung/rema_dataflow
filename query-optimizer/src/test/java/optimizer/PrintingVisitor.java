/* (C)2023 */
package optimizer;

import org.apache.calcite.sql.SqlIdentifier;
import org.apache.calcite.sql.SqlLiteral;
import org.apache.calcite.sql.util.SqlBasicVisitor;

class PrintingVisitor extends SqlBasicVisitor<Void> {

  public Void visit(SqlLiteral literal) {
    System.out.println("Literal: " + literal);
    return null;
  }

  public Void visit(SqlIdentifier id) {
    System.out.println("Identifier: " + id);
    return null;
  }
}
