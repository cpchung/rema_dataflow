/* (C)2023 */
package optimizer;

import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.SqlSelect;
import org.apache.calcite.sql.parser.SqlParser;
import org.junit.Test;

public class Rewrite {

  @Test
  public void testParsing() throws Exception {

    SqlParser parser = SqlParser.create("SELECT * FROM \"foo\" WHERE \"bar\"=3 GROUP BY \"baz\"");
    SqlNode query = parser.parseQuery();

    System.out.println(query.toString());
    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!");

    SqlSelect select = (SqlSelect) query;

    System.out.println(select.getSelectList());
    System.out.println(select.getWhere());
    System.out.println(select.getGroup());
    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!");

    query.accept(new PrintingVisitor());
  }
}
