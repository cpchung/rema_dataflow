/* (C)2023 */
package optimizer;

import static optimizer.Util.print;

import org.apache.calcite.adapter.enumerable.EnumerableRules;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.rules.CoreRules;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.type.SqlTypeName;
import org.apache.calcite.tools.RuleSet;
import org.apache.calcite.tools.RuleSets;
import org.junit.Test;

public class RewriteOnMVTest {

  @Test
  public void testJoinRewriting() throws Exception {
    SimpleTable depts =
        SimpleTable.newBuilder("depts")
            .addField("deptno", SqlTypeName.INTEGER)
            .addField("deptname", SqlTypeName.VARCHAR)
            .withRowCount(60_000L)
            .build();

    SimpleTable locations =
        SimpleTable.newBuilder("locations")
            .addField("locationid", SqlTypeName.INTEGER)
            .addField("state", SqlTypeName.CHAR)
            .withRowCount(60_000L)
            .build();

    SimpleTable emps =
        SimpleTable.newBuilder("emps")
            .addField("empid", SqlTypeName.INTEGER)
            .addField("deptno", SqlTypeName.INTEGER)
            .addField("locationid", SqlTypeName.INTEGER)
            .addField("empname", SqlTypeName.VARCHAR)
            .addField("salary", SqlTypeName.DECIMAL)
            .withRowCount(60_000L)
            .build();
    SimpleSchema schema =
        SimpleSchema.newBuilder("depts").addTable(depts).addTable(locations).addTable(emps).build();

    Optimizer optimizer = Optimizer.create(schema);

    String query =
        "SELECT empid\n"
            + "FROM depts\n"
            + "JOIN (\n"
            + "  SELECT empid, deptno\n"
            + "  FROM emps\n"
            + "  WHERE empid = 1) AS subq\n"
            + "ON depts.deptno = subq.deptno";

    String mv = "SELECT empid\n" + "FROM emps\n" + "JOIN depts USING (deptno)";

    /// parse sql string to sql tree
    SqlNode queryTree = optimizer.parse(query);
    /// validate sql tree
    SqlNode validatedQueryTree = optimizer.validate(queryTree);
    RelNode queryRelTree = optimizer.convert(validatedQueryTree);

    /// parse sql string to sql tree
    SqlNode mvTree = optimizer.parse(mv);
    /// validate sql tree
    SqlNode validatedMvTree = optimizer.validate(mvTree);
    RelNode mvRelTree = optimizer.convert(validatedMvTree);

    print("AFTER CONVERSION", queryRelTree);

    RuleSet rules =
        RuleSets.ofList(
            CoreRules.FILTER_TO_CALC,
            CoreRules.PROJECT_TO_CALC,
            CoreRules.FILTER_CALC_MERGE,
            CoreRules.PROJECT_CALC_MERGE,
            EnumerableRules.ENUMERABLE_TABLE_SCAN_RULE,
            EnumerableRules.ENUMERABLE_PROJECT_RULE,
            EnumerableRules.ENUMERABLE_FILTER_RULE,
            EnumerableRules.ENUMERABLE_CALC_RULE,
            EnumerableRules.ENUMERABLE_AGGREGATE_RULE);

    //    RelNode optimizerRelTree = optimizer.optimize(
    //            queryRelTree,
    //            queryRelTree.getTraitSet().plus(EnumerableConvention.INSTANCE),
    //        rules
    //    );
    //
    //    print("AFTER OPTIMIZATION", optimizerRelTree);
    System.out.println("!!");
  }
}
