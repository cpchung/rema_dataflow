/* (C)2023 */
package optimizer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;
import javax.sql.DataSource;
import org.apache.calcite.adapter.jdbc.JdbcSchema;
import org.apache.calcite.jdbc.CalciteConnection;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.apache.calcite.tools.RelBuilder;
import org.apache.calcite.tools.RelRunner;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CalciteTests {

  private String jdbcUrl;
  private Connection connection;
  private DataSource mockDataSource;

  @Before
  public void setupTests() throws SQLException {
    jdbcUrl = MockDb.INSTANCE.getUrl();
    connection = DriverManager.getConnection(jdbcUrl, "", "");

    // You can populate test data into the database like so:
    try (Statement s = connection.createStatement()) {
      s.execute(
          "create table mytemptable("
              + "id integer not null primary key,"
              + "exampleFoo varchar(25),"
              + "exampleBar varchar(25))");

      s.execute("insert into mytemptable values(1, 'test', '1234')");
      s.execute("insert into mytemptable values(2, 'test2', 'xyz')");
    }

    mockDataSource = JdbcSchema.dataSource(jdbcUrl, "org.hsqldb.jdbcDriver", "", "");
  }

  @Ignore
  @Test
  public void calciteUnitTestExample() throws SQLException {

    // Build our connection
    Connection connection = DriverManager.getConnection("jdbc:calcite:");

    // Unwrap our connection using the CalciteConnection
    CalciteConnection calciteConnection = connection.unwrap(CalciteConnection.class);

    // Get a pointer to our root schema for our Calcite Connection
    SchemaPlus rootSchema = calciteConnection.getRootSchema();

    // Attach our Postgres Jdbc Datasource to our Root Schema
    rootSchema.add(
        "exampleSchema",
        JdbcSchema.create(rootSchema, "exampleSchema", mockDataSource, null, null));

    FrameworkConfig config = Frameworks.newConfigBuilder().defaultSchema(rootSchema).build();

    RelBuilder r = RelBuilder.create(config);

    RelNode node =
        r
            // First parameter is the Schema, the second is the table name
            .scan("exampleSchema", "MYTEMPTABLE")
            // If you want to select from more than one table, you can do so by adding a second scan
            // parameter
            .filter(r.equals(r.field("ID"), r.literal(1)))
            // These are the fields you want to return from your query
            .project(r.field("ID"), r.field("EXAMPLEFOO"), r.field("EXAMPLEBAR"))
            .build();

    RelRunner runner = connection.unwrap(RelRunner.class);
    PreparedStatement ps = runner.prepare(node);

    ps.execute();

    ResultSet resultSet = ps.getResultSet();
    //        DBTablePrinter.printResultSet(resultSet);
  }

  static class MockDb {
    MockDb() {}

    static final MockDb INSTANCE = new MockDb();
    private final AtomicInteger id = new AtomicInteger(1);

    public String getUrl() {
      return "jdbc:hsqldb:mem:db" + id.getAndIncrement();
    }
  }
}
