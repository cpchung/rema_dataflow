package sql;

import java.util.Arrays;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class SQLDemo {

    static class Test<I extends Integer> {

        <L extends Long> void x(I i, L l) {

            System.out.println(i + ", " + l);

            System.out.println(i.intValue() + ", " + l.longValue());
        }
    }

    public static void main(String[] args) {

        new Test().x(1, 2L);

        CharStream inputStream =
                CharStreams.fromString(
                        "UNWIND [1.0, NULL, 3.0] as v1 RETURN v1, coalesce(v1, gds.util.NaN()) as v2;");


        String sql= "select sal\n"
                + "from emp\n"
                + "where case when (sal = 1000) then\n"
                + "(case when sal = 1000 then null else 1 end is null) else\n"
                + "(case when sal = 2000 then null else 1 end is null) end is true";

         sql=  "SELECT emp1.employee_id AS id_one, emp1.rating AS rating, count(*) AS the_count\n"
            + "FROM cp.\"data/employees.json\" AS emp1\n"
            + "INNER JOIN cp.\"data/employees.json\" AS emp2\n"
            + "  ON emp1.position_id = emp2.position_id\n"
            + "  AND emp1.employee_id != emp2.employee_id\n"
            + "  AND emp1.rating > (\n"
            + "    SELECT AVG(emp3.rating)\n"
            + "    FROM cp.\"data/employees.json\" AS emp3\n"
            + "    WHERE emp3.position_id = emp1.position_id\n"
            + ") GROUP BY emp1.employee_id, emp1.rating\n"
            + "ORDER BY emp1.employee_id;";




        inputStream =
                CharStreams.fromString(sql);

//        String path = "/home/cpchung/code/rema_dataflow/parser/src/main/java/sql/test.sql";
//        inputStream = CharStreams.fromString(readFromFile(path));

        TrinoLexer markupLexer = new TrinoLexer(inputStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(markupLexer);
        TrinoParser parser = new TrinoParser(commonTokenStream);

        ParseTree tree = parser.parse();
        System.out.println(tree);

        // show AST in GUI
        TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
        viewr.open();
    }
}
