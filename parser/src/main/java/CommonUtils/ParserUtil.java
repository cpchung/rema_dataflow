package CommonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class ParserUtil {
  public static String execCmd(String cmd) {
    String result = null;
    try (InputStream inputStream = Runtime.getRuntime().exec(cmd).getInputStream();
        Scanner s = new Scanner(inputStream).useDelimiter("\\A")) {
      result = s.hasNext() ? s.next() : null;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }

  public static String readFromFile(String path) {
    try {
      //            ProcessBuilder builder = new ProcessBuilder();
      //            String[] command = {"bash", "cypher-lint", "-a", path};
      //            builder.command(command);
      //            Process shellP = builder.start();
      System.out.println(execCmd("bash cypher-lint -a " + path));
      byte[] encoded = Files.readAllBytes(Paths.get(path));
      String s = new String(encoded, StandardCharsets.US_ASCII);
      System.out.println(s);
      return s;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
