/* (C)2023 */
package cypher;

import static CommonUtils.ParserUtil.readFromFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Main {
  public static void dfs(ParseTree root) {
    if (root.equals(null)) return;
    if (root.getChildCount() == 1) {
      System.out.print("> ");
      System.out.print(root.getChild(0));
      dfs(root.getChild(0));
      System.out.println();
    } else {
      for (int i = 0; i < root.getChildCount(); i++) {
        System.out.print("> ");
        System.out.print(root.getChild(0).getText());
        dfs(root.getChild(i));
        System.out.println();
      }
    }
  }

  static void cypher(String path) {

    CharStream inputStream = CharStreams.fromString(readFromFile(path));
    CypherLexer markupLexer = new CypherLexer(inputStream);
    CommonTokenStream commonTokenStream = new CommonTokenStream(markupLexer);
    CypherParser parser = new CypherParser(commonTokenStream);
    ParseTree tree = parser.oC_Cypher();
    //        dfs(tree);

    System.out.println("!!!" + parser.getNumberOfSyntaxErrors() + "!!!");
    CypherBaseVisitorImpl cypherVisitor = new CypherBaseVisitorImpl();
    //        CypherBaseVisitor cypherVisitor = new CypherBaseVisitor();
    cypherVisitor.visit(tree);
    System.out.println(tree.toStringTree(parser));

    //        ParseTreeWalker.DEFAULT.walk(new CypherListenerBaseImpl(), tree);
    //        ParseTreeWalker.DEFAULT.walk(new CypherListnerImpl(), tree);
    // show AST in GUI
    TreeViewer viewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
    viewer.open();
  }

  public static void main(String[] args) {
    Path currentRelativePath = Paths.get("");
    String s = currentRelativePath.toAbsolutePath().toString();
    System.out.println("Current absolute path is: " + s);

    String path = s + "/test.cyp";

    cypher(path);
  }
}
