package cypher;

import java.util.Arrays;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class ANTLRDemo {

  static class Test<I extends Integer> {

    <L extends Long> void x(I i, L l) {

      System.out.println(i + ", " + l);

      System.out.println(i.intValue() + ", " + l.longValue());
    }
  }

  public static void main(String[] args) {

    new Test().x(1, 2L);

    CharStream inputStream =
        CharStreams.fromString(
            "UNWIND [1.0, NULL, 3.0] as v1 RETURN v1, coalesce(v1, gds.util.NaN()) as v2;");

    inputStream =
        CharStreams.fromString("MATCH (a:University) RETURN a.url ORDER BY a.url LIMIT 100");

    CypherLexer markupLexer = new CypherLexer(inputStream);
    CommonTokenStream commonTokenStream = new CommonTokenStream(markupLexer);
    CypherParser parser = new CypherParser(commonTokenStream);

    ParseTree tree = parser.oC_Cypher();
    System.out.println(tree);

    // show AST in GUI
    TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
    viewr.open();
  }
}
