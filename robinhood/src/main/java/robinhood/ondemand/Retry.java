package robinhood.ondemand;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


public class Retry {
    // assumes the current class is called MyLogger
    private static final  Logger LOGGER = Logger.getLogger(Retry.class.getName());
    //  1. get the nodes in set   t1 ={  l,m,i,o}
    private Set<Node> search(Node root, Set<Integer> input) {

        Set<Node> output = new HashSet<>();
        Stack<Node> stack = new Stack<>();
//    Deque<Node> stack = new ArrayDeque<>();
        stack.push(root);

//    while (!stack.empty()) {
        while (!stack.isEmpty()) {

            Node node = stack.pop();

            if (input.contains(node.getVal())) {
                output.add(node);
            }

            for (Node n : node.getChildren()) {
                stack.push(n);
            }
        }

        return output;
    }

    //  2.  traverse up through paren
    private Set<Node> getParents(Set<Node> input) {

        Set<Node> impacted = new HashSet<>();
        for (Node node : input) {

            // Queue<Node> queue = new PriorityQueue<Node>(5, (a,b)-> a.getVal() -b.getVal());
            Deque<Node> queue = new ArrayDeque<>() {};

            queue.add(node);

            while (!queue.isEmpty()) {
                Node start = queue.remove();

                for (Node front : start.getParents()) {

                    if (!impacted.contains(front) && !input.contains(front)) {
                        impacted.add(front);
                        queue.add(front);
                    }
                }
            }
        }

        return impacted;
    }

    private List<Integer> sort(Set<Node> input) {

        List<List<Integer>> dependencies = new ArrayList<>();

        //    for (Node node : input) {
        //
        //      if (node.getParent() != null) {
        //        dependencies.add(Arrays.asList(node.getParent().getVal(), node.getVal()));
        //      }
        //    }
        //
        // https://stackoverflow.com/questions/10043209/convert-arraylist-into-2d-array-containing-varying-lengths-of-arrays
        int[][] intArray =
                dependencies.stream().map(u -> u.stream().mapToInt(i -> i).toArray()).toArray(int[][]::new);

//    System.out.println(Arrays.toString(intArray));
        LOGGER.info(Arrays.toString(intArray));
//    Scheduler sol = new Scheduler();
        //    int[] order = sol.seekOrder(input.size(), intArray);

        List<Node> ls = new ArrayList(input);
        Collections.sort(
                ls,
                new Comparator<Node>() {
                    @Override
                    public int compare(final Node lhs, Node rhs) {
                        // TODO return 1 if rhs should be before lhs
                        //     return -1 if lhs should be before rhs
                        //     return 0 otherwise (meaning the order stays the same)

                        return lhs.getVal() - rhs.getVal();
                    }
                });

        List<Integer> res = new ArrayList<>();

        for (Node node : ls) {
            res.add(node.getVal());
        }

        //    System.out.println(res);
        //    return new ArrayList<>();
        return res;
    }
    //  3. getImpacted
    public Map<Integer, Set<Integer>> getImpactedAndFailedJobs(
            Map<Integer, Set<Integer>> graph, Set<Integer> input) {

        Node root = map2tree(graph);
        System.out.println(root.getVal());

        Set<Node> updatedNodes = search(root, input);
        updatedNodes.stream().forEach(x -> System.out.println(x.getVal()));

        Set<Node> impacted = getParents(updatedNodes);

        impacted = Stream.concat(impacted.stream(), updatedNodes.stream()).collect(Collectors.toSet());

        System.out.println("!!!!!!!!!!!!!!!");
        impacted.stream().forEach(x -> System.out.println(x.getVal()));

        //    considering applying tc to recover the dependencies using augmented parents
        //    ms.sort(impacted);

        System.out.println();

        Map<Integer, Set<Integer>> impactedMap = set2Map(impacted);
        System.out.println(set2Map(impacted));

        //    augment with leave nodes.
        for (Node node : impacted) {
            if (!impactedMap.containsKey(node.getVal())) {
                impactedMap.put(node.getVal(), new HashSet<>());
            }
        }

        return impactedMap;
    }

    private Map<Integer, Set<Integer>> set2Map(Set<Node> input) {

        Map<Integer, Set<Integer>> dependencies = new HashMap<>() {};

        for (Node node : input) {
            System.out.println(node.getVal());
            for (Node parent : node.getParents()) {
                if (!dependencies.containsKey(parent.getVal())) {
                    dependencies.put(parent.getVal(), new HashSet<>());
                }
                dependencies.get(parent.getVal()).add(node.getVal());
            }
        }

        return dependencies;
    }

    public Node map2tree(Map<Integer, Set<Integer>> tree) {

        Map<Integer, Node> int2Node = new HashMap<>();

        for (Entry kv : tree.entrySet()) {

            Node node;
            if (int2Node.containsKey(kv.getKey())) {
                node = int2Node.get(kv.getKey());
            } else {
                node = new Node((Integer) kv.getKey(), new HashSet<>(), new HashSet<>());
            }

            for (Integer childVal : (Set<Integer>) kv.getValue()) {
                Node childNode;
                if (int2Node.containsKey(childVal)) {
                    childNode = int2Node.get(childVal);
                } else {
                    childNode = new Node(childVal, new HashSet<>(), new HashSet<>());
                    int2Node.put(childVal, childNode);
                }
                node.getChildren().add(childNode);
                childNode.getParents().add(node);
            }

            int2Node.put((Integer) kv.getKey(), node);
        }

//    Node root =
//        int2Node.values().stream().filter(x -> x.getParents().size() == 0).findFirst().orElse(null);
        //        .get();

        return int2Node.values().stream().filter(x -> x.getParents().size() == 0).findFirst().orElse(null);
    }


    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @Setter
    @Getter
    // @Data
//  public static class Node {
    private class Node {

        private int val;
        //  @ToString.Exclude
        private Set<Node> children;
        //  @ToString.Exclude
        //  public Node parent = null;
        private Set<Node> parents = null;
    }
}
