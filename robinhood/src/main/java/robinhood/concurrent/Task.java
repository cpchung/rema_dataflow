package robinhood.concurrent;


import java.util.function.Supplier;

public class Task implements Supplier<Boolean> {

    String id;

    Task(String id) {
        this.id = id;
    }

    private Boolean checkStatus() {
//            Status status = VALUES.get(RANDOM.nextInt(SIZE));
        switch (id) {
//      case "1":
//      case "12":
//        return false;

//      case "10":
//      case "6":
//        return false;
            default:
                return true;
        }

    }

    @Override
    public Boolean get() {

        String formatter = "Thread  %s --------Task %s ";
        System.out
                .printf(formatter + "started\n", Thread.currentThread().getName(), id);
//            if(id.equals("5")){
//                throw new RuntimeException("5 exit");
//            }
//        pause(getRandom());
        try {
            System.out
                    .printf(formatter + "about to sleep\n", Thread.currentThread().getName(),
                            id);
            Thread.sleep(2 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }

        if (checkStatus()) {
            System.out
                    .printf(formatter + "succeeded\n", Thread.currentThread().getName(), id);
        } else {
            System.out
                    .printf(formatter + "failed\n", Thread.currentThread().getName(), id);
        }

        return checkStatus();
    }
}