package robinhood.concurrent;

import java.util.function.BiFunction;

public class BiTask implements BiFunction<Boolean, Boolean, Boolean> {
    @Override
    public Boolean apply(Boolean left, Boolean right) {
//        System.out.println("Brewing Task with " + left + " and " + right);
//        pause(getRandom());

        return left && right;
    }
}
