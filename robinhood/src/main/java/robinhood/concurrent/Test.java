package robinhood.concurrent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import robinhood.Rema;

public class Test {

    public static void main(String[] args) {

        Map<Integer, Set<Integer>> graph =
                new HashMap<>() {
                    {
                        put(1, new HashSet<>(Arrays.asList(2, 3, 4)));
                        put(2, new HashSet<>(Arrays.asList(5, 6)));
                        put(3, new HashSet<>(Arrays.asList(7)));
                        put(4, new HashSet<>(Arrays.asList(8, 9, 10)));
                        put(5, new HashSet<>(Arrays.asList(11)));
                        put(6, new HashSet<>(Arrays.asList(12, 13)));
                        put(7, new HashSet<>(Arrays.asList()));
                        put(8, new HashSet<>(Arrays.asList()));
                        put(9, new HashSet<>(Arrays.asList(14, 15)));
                        put(10, new HashSet<>(Arrays.asList()));
                        put(11, new HashSet<>(Arrays.asList()));
                        put(12, new HashSet<>(Arrays.asList(16, 17)));
                        put(13, new HashSet<>(Arrays.asList()));
                        put(14, new HashSet<>(Arrays.asList(18)));
                        put(15, new HashSet<>(Arrays.asList(19)));
                        put(16, new HashSet<>(Arrays.asList()));
                        put(17, new HashSet<>(Arrays.asList()));
                        put(18, new HashSet<>(Arrays.asList()));
                        put(19, new HashSet<>(Arrays.asList()));
                    }
                };
        graph =
                new HashMap<>() {
                    {
                        put(7, new HashSet<>(Arrays.asList(6)));
                        put(6, new HashSet<>(Arrays.asList(3, 4, 5)));
                        put(5, new HashSet<>(Arrays.asList(3, 4)));
                        put(4, new HashSet<>(Arrays.asList(2)));
                        put(3, new HashSet<>(Arrays.asList(1)));
                        put(2, new HashSet<>(Arrays.asList(0)));
                        put(1, new HashSet<>(Arrays.asList(2)));
                        put(0, new HashSet<>(Arrays.asList()));
                    }
                };
        HashSet<Integer> input = new HashSet<>(Arrays.asList(3, 4));

        new Rema().triggerOndemand(graph, input);
//        new Rema().delayedExecution(graph, input);


    }
}
