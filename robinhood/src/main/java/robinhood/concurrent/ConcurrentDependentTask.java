package robinhood.concurrent;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
// @Data
public class ConcurrentDependentTask<T> {
    Map<T, Set<T>> dependencies;
    Map<T, CompletableFuture<Boolean>> job2CF;
    ExecutorService executor;

    public CompletableFuture<Boolean> evaluate(T root) {

        if (job2CF.containsKey(root)) {
            return job2CF.get(root);
        }

        if (dependencies.get(root).isEmpty()) {
            CompletableFuture<Boolean> todo =
                    CompletableFuture.supplyAsync(new Task(String.valueOf(root)), executor);
            job2CF.put(root, todo);
            return todo;
        }

        CompletableFuture<Boolean> reducedDependentTodo = CompletableFuture.completedFuture(true);
        for (T child : dependencies.get(root)) {
            reducedDependentTodo =
                    reducedDependentTodo.thenCombineAsync(evaluate(child), new BiTask(), executor);
        }

        job2CF.put(
                root,
                reducedDependentTodo.thenComposeAsync(
                        x -> {
                            if (x) {
                                return CompletableFuture.supplyAsync(new Task(String.valueOf(root)), executor);
                            } else {
                                return CompletableFuture.completedFuture(false);
                            }
                        }));

        return job2CF.get(root);
    }

    enum Status {
        STARTING,
        BOOTSTRAPPING,
        RUNNING,
        WAITING,
        TERMINATING,
        TERMINATED,
        TERMINATED_WITH_ERRORS
    }
}
