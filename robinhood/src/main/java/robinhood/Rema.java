package robinhood;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import robinhood.concurrent.ConcurrentDependentTask;
import robinhood.ondemand.Retry;
import robinhood.threadpool.ScheduledCompletable;
public class Rema {

    private static final int MASTER = -1;

    private Set<Integer> getMasterChildren(Map<Integer, Set<Integer>> graph) {

        Set<Integer> masterChildren = new HashSet<>();
        masterChildren.addAll(graph.keySet());
        masterChildren.removeAll(
                graph.values().stream().flatMap(Collection::stream).collect(Collectors.toSet()));

        return masterChildren;
    }

    public int triggerOndemand(Map<Integer, Set<Integer>> graph, Set<Integer> input) {

        Retry retry = new Retry();
        Map<Integer, Set<Integer>> dependencies = retry.getImpactedAndFailedJobs(graph, input);

        dependencies.put(MASTER, getMasterChildren(graph));
        System.out.println(dependencies);

        // kick off concurrent run
        ConcurrentDependentTask<Integer> concurrentDependentTask =
                new ConcurrentDependentTask<>(
                        dependencies, new HashMap<>(), ScheduledCompletable.getExecutorService());

        try {

            System.out.println(concurrentDependentTask.evaluate(MASTER).get());
            concurrentDependentTask.getExecutor().shutdownNow();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return 2020;
    }

    public void delayedExecution(Map<Integer, Set<Integer>> graph, Set<Integer> input) {

        // (Make the executor as above.)
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(4);

        //    Supplier<CompletableFuture<Integer>> asyncTask =
        //        () -> CompletableFuture.completedFuture(10 + 25);//

        Supplier<CompletableFuture<Integer>> asyncTask =
                () -> CompletableFuture.completedFuture(new Rema().triggerOndemand(graph, input));

        CompletableFuture<Integer> completableFuture =
                ScheduledCompletable.scheduleAsync(executor, asyncTask, 2, TimeUnit.SECONDS);

        completableFuture.thenAccept(
                answer -> {
                    System.out.println(answer);
                });
        System.out.println("Answer coming...");

        //    ScheduledCompletable.shutdownAndAwaitTermination(executor);
        //    executor.shutdownNow();
    }

    public static void main(String[] args) {

        //  a,b,c,d,e,f,g,h,i,j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
        //  1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26
        Map<Integer, Set<Integer>> graph =
                new HashMap<>() {
                    {
                        put(1, new HashSet<>(Arrays.asList(2, 3, 4)));
                        put(2, new HashSet<>(Arrays.asList(5, 6)));
                        put(3, new HashSet<>(Arrays.asList(7)));
                        put(4, new HashSet<>(Arrays.asList(8, 9, 10)));
                        put(5, new HashSet<>(Arrays.asList(11)));
                        put(6, new HashSet<>(Arrays.asList(12, 13)));
                        put(7, new HashSet<>(Arrays.asList()));
                        put(8, new HashSet<>(Arrays.asList()));
                        put(9, new HashSet<>(Arrays.asList(14, 15)));
                        put(10, new HashSet<>(Arrays.asList()));
                        put(11, new HashSet<>(Arrays.asList()));
                        put(12, new HashSet<>(Arrays.asList(16, 17)));
                        put(13, new HashSet<>(Arrays.asList()));
                        put(14, new HashSet<>(Arrays.asList(18)));
                        put(15, new HashSet<>(Arrays.asList(19)));
                        put(16, new HashSet<>(Arrays.asList()));
                        put(17, new HashSet<>(Arrays.asList()));
                        put(18, new HashSet<>(Arrays.asList()));
                        put(19, new HashSet<>(Arrays.asList()));
                    }
                };

        Set<Integer> input = new HashSet<>(Arrays.asList(12, 13, 9, 15));

        graph =
                new HashMap<>() {
                    {
                        put(7, new HashSet<>(Arrays.asList(6)));
                        put(6, new HashSet<>(Arrays.asList(3, 4, 5)));
                        put(5, new HashSet<>(Arrays.asList(3, 4)));
                        put(4, new HashSet<>(Arrays.asList(2)));
                        put(3, new HashSet<>(Arrays.asList(1)));
                        put(2, new HashSet<>(Arrays.asList(0)));
                        put(1, new HashSet<>(Arrays.asList(2)));
                        put(0, new HashSet<>(Arrays.asList()));
                    }
                };
        //    Map<Integer, Set<Integer>> finalGraph = graph;
        //    Set<Integer> roots=
        //        graph.keySet().stream().filter(x->
        // !finalGraph.values().contains(x)).collect(Collectors.toSet());

        input = new HashSet<>(Arrays.asList(3, 4));

        //    new Rema().triggerOndemand(graph, input);

        new Rema().delayedExecution(graph, input);
    }
}
