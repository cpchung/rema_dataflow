package robinhood.threadpool;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class ScheduledCompletable {

    private ScheduledCompletable(){}

    private static final int NUMTHREAD = 2;


//  private  final  ExecutorService executor =

    private static class NameableThreadFactory implements ThreadFactory {

        private final AtomicInteger threadsNum = new AtomicInteger();
        private final String namePattern;

        public NameableThreadFactory(String baseName) {
            namePattern = baseName + "-%d";
        }

        @Override
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format(namePattern, threadsNum.addAndGet(1)));
        }

    }



    public static ExecutorService getExecutorService(){

        return Executors
                .newFixedThreadPool(NUMTHREAD, new NameableThreadFactory("Chak"));
        //        .newWorkStealingPool();
        //  ExecutorService executor = Executors.newFixedThreadPool(NOTHREADS,      new ThreadFactory() {
//    public Thread newThread(Runnable r) {
//      Thread t = Executors.defaultThreadFactory().newThread(r);
//      t.setDaemon(true);
//      return t;
//    }
//  });

        //
//      .setNameFormat("MyThreadPool-Worker-%d")
//      .setDaemon(true)
//      .build();

    }



    public static <T> CompletableFuture<T> schedule(
            ScheduledExecutorService executor,
            Supplier<T> command,
            long delay,
            TimeUnit unit
    ) {
        CompletableFuture<T> completableFuture = new CompletableFuture<>();
        executor.schedule(
                (() -> {
                    try {
                        return completableFuture.complete(command.get());
                    } catch (Exception t) {
                        return completableFuture.completeExceptionally(t);
                    }
                }),
                delay,
                unit
        );
        return completableFuture;
    }


    public static <T> CompletableFuture<T> scheduleAsync(
            ScheduledExecutorService executor,
            Supplier<CompletableFuture<T>> command,
            long delay,
            TimeUnit unit
    ) {
        CompletableFuture<T> completableFuture = new CompletableFuture<>();
        executor.schedule(
                () -> {
                    command.get().thenAccept(
                            t -> completableFuture.complete(t)
                    )
                            .exceptionally(
                                    t -> {completableFuture.completeExceptionally(t);return null;}
                            );
                },
                delay,
                unit
        );
        return completableFuture;
    }

    public static void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(1, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(1, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }


}
